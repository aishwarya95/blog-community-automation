Framework documentation

Base Selenium : This class contains functions that performs action on web object ex: click, type, extracting text, Handling checkbox, radio button select list etc, These are generic functions not application specific. i.e can be reused for automating other UI application as well. Functions are designed to handle different locator type i.e if the locator type changes you don't have to change anything at function level Updating the locator type and locator information in object repository should suffice.

Application Library: It consists of function that maps to business logic, ex: login to application, Fill the signup form, Fill Request batch form, Make payment etc. It's built on top of base selenium functions. Functions available in this class are application specific.

Step Mappers: This basically maps a DSL to specific application library. Step mappers are segregated into multiple operation specific mappers. Ex: all search related operation are defined in one step mapper,request batch related operations in another etc.

Features Files: All operation specific to particular feature are grouped in single feature file.
Scenario maps to test case of that feature.
Data tables are used to pass values to respective step.


Object repository: This File contains the locator information of all web object used in test scenarios.
There are 5 columns 
ObjectName: This is unique key to extract the locator information from object repository, there cannot be duplicate keys in OR.
ObjectType: This column is for information purpose, identifies what type of web object the locator maps to ex : button, textbox,radio button, checkbox,select list, link etc. 
LocatorType: Different types of locator ID/NAME/XPATH/CSS/JAVASCRIPT/LINK etc, should be specified in capital case.
Locator: locator to identify the web object.
PageName: This is information purpose, indicate web object is on which page.

Configuration File: Contains Global configuration information like type of browser, wait times, Sleep Time, Data extraction queries etc.
Browser : Used to indicate on which browser test execution should be carried out.
BaseURL : Application URL, this is URL that would be invoked on launch of browser.
FirefoxDriverPath,ChromeDriverPath,IEDriverPath : This should point to paths where drivers are downloaded and kept, This drivers are responsible for executing the java code inside browser.
Explicit_wait_time,Implicit_wait_time,Max_page_load_time : Contains different type of wait times used for synchronization of test execution.

DbServerName: Name of Database server <IP or FQDN works>
DbUserName=Database username
DbPassword=Database password
DbName=Name of Database
DbPort=Database port
CourseIdQuery=<Queries used for extraction course id with placeholders that gets replaced during runtime>
UserIdQuery=<Queries used for extraction course id with placeholders that gets replaced during runtime>
General_wait_time: This time is used for sleeping on certain scenario's where there are refresh happening after page load.
TimeStampThreshold: Used in google analytics, if test start time and google analytic specific action greater than this threshold test case would be marked as failed.

Google Analytics assertion framework
To extract Har(HTTP Archive) logs for assertion we use browsermob proxy, browser launched would be configured to route all request through browser mob proxy. using built in API's of proxy all http requests would be extracted and lopped over to find whether matching Event Category/Event Label/Event action entries could be found. if there are no traces of specific EA-EC-EL combination testcase would be marked as failed.

Approach for verifying timestamp in Google analytics calls.
Time would be captured at start of test cases, which would be named as test case start time.
Actual time would be extracted from https calls matching the EA-EC-EL criteria.
Time difference between Test case start time and time passed in GA calls of specific test case shouldn't be greater than certain threshold value (Configurable). ex : difference > 300 i.e 5min would mark test case as failed.


Execution details
Test cases could be executed in 2 ways
1) Junit : This approach wouldn't generate the allure report.
2) Maven : This approach would generate allure report.

Junit :
Configuration to be done before execution
Add following changes in run configuration -> Arguments -> Program arguments.
-DConfigFile=Automation.properties

Configure the junit runner class to point to a particular feature file or the parent directory ( which runs all the feature files in that directory )

Execution:
Right click on Junit Runner class -> Run As ->Junit.

Maven: 
Configuration to be made before execution:
Right click on Project -> Run Configuration -> Maven build... -> Add below value in goals field of main tab.
test -DConfigFile=Automation.properties

Execution:
Right click on project -> click on Run As -> Click on Maven build.

Results : It creats a folder allure-results inside project


Steps to generate allure reports from command line utiltiy.

Step 1: Download the allure command line utility from following location
https://github.com/allure-framework/allure2/releases/tag/2.9.0
Step 2: Unzip and place it in some location on local driver say c:\\allure290
Step 3: Open Command prompt and move to the location where allure command line utility copied
Ex : c:\\allure290\bin
Step 4 : Execute the command
allure serve <path of allure-result folder>
Ex: allure serve C:\Users\abc\eclipse-workspace\RestAssured_Cucumber_Allure\allure-results2



Step Mappers
Though the DSL as self explanatory,have tried to explain the operation performed by each statement.

Given user navigates to edureka website : THis would open the browser and navigate to the URL specified in configuration file.

And fills up signup form with below details : Clicks on Signup button on Home Screen and Fill up the form with details provided in datatable.

And set following password on "Ramu@123#" : used to set the password on signup screen.

Then verify  following "Enter a valid Email address" error message is displayed on signup screen related to "email" : Validates email error message on signup screen.

And verify  following "Please enter a valid mobile number." error message is displayed on signup screen related to "phoneno"
Validates Phone error message on signup screen.

Then verify  following "Enter a valid Password" error message is displayed on signup screen related to "password" : 
Validates password error message on signup screen.

And assert following analytics data : verifies whether matching ec-el-ea google analytics call is made or not.

Then signup should be successful and user lands on Home page : used to verify after completion of signup, used lands on home page.

searches for course "<course name>" and navigates to course landing page. : Searches for specific course in search button and navigates to course landing page.

And fills up request a batch form with below details : click on request batch button and fills up the form with details provided in data table.

Then Request batch operation should be successfull and following message displayed to user "<Confirmation Message>" : Verifies user is greeting with successfull message after submitting request batch form with valid details.

verify following "Please enter a valid email address" error message is displayed on request batch screen related to "email": verifies user is prompted with  email error message for invalid mail id's.

Then verify following "Please enter a valid mobile number." error message is displayed on request batch screen related to "phoneno" : verifies user is prompted with  phone error message for invalid phone no provided as input.

And clicks on pay full price link : clicks on pay full price link on course landing page.

And fills up payfull price form with below details : fills up form with details provided in datatable.

Then verify below course price information displayed to user : verifies the amount displayed to user before making payment.

Then verify following "Enter a valid Email ID" error message is displayed on pay full price screen related to "email" : verifies user is prompted with  email error message for invalid mail id's.

Then verify following "Please enter a valid mobile number." error message is displayed on pay full price screen related to "phoneno" : verifies user is prompted with  phone error message for invalid phone no provided as input.
