Feature: CustomLink feature

  @Sanity @ReferralCredits @Customlink
  Scenario Outline: Verify custom link functionality for ccavenue single course payment(TC_RC_001)
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Customer Delight->Customers->User Registration" Page
    And Verify User registration form
      | #Email                    | name | Country |
      | leadforms@tech.edureka.in | test | India   |
    And extract the user id
    And extract the course id for "<Course>" through "Display_title"
    And extract course price information for currency "<Currency>"
    And Navigate to "Sales->Create Custom Link" Page
    Then verify Custom link form
      | Course   | Currency   | Batch   | Amount | giveEducash | provideEMI | gateway  | checkDiscountlimit | MultiplePayment |
      | <Course> | <Currency> | <Batch> |  22000 | TRUE        | FALSE      | CCAvenue | TRUE               | FALSE     |
    And Check Preview and send mail
    And click on pay securely for "CCAvenue"
    Then Verify CCAvenue Payment
   And extract course price information for currency "<Currency>"
    And extract course price in usd and inr
    And Assert following data in "Pre_orders" table
      | select orderid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' and courseid='$COURSE_ID'  order by id desc limit 1 | IS_NOT_NULL_OR_BLANK |
      | select courseid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' and courseid='$COURSE_ID' order by id desc limit 1 | $COURSE_ID           |
    And Assert following data in "post_orders" table
      | Query                                                                                                                                                                         | Expected Result      |
      | Select orderid from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'                | IS_NOT_NULL_OR_BLANK |
      | Select priceinr from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'               | $PRICE_INR           |
      | Select priceusd from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'               | $PRICE_USD           |
      | Select gateway from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'                | invoice-CCAvenue     |
      | Select original_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'         | $ORIGINAL_PRICE      |
      | Select discount_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'         | $DISCOUNT            |
      | Select final_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'            | $FINAL_PRICE         |
      | Select servicetax_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'       | $SERVICETAX          |
      | Select invoice_no from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'             | IS_NOT_NULL_OR_BLANK |
      | Select gateway_transaction_id from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' | IS_NOT_NULL_OR_BLANK |
      | Select payment_type from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'           |                    1 |
    And Assert following data in "user_course" table
      | Query                                                                                  | Expected Result |
      | select course_id from user_courses where user_id='$USER_ID' and course_id='$COURSE_ID' | $COURSE_ID      |
      | select is_paid from user_courses where user_id='$USER_ID'                              |               1 |
    And Assert following data in "users" table
      | Query                                                                                  | Expected Result |
      | select customer_status from users where id='$USER_ID' | 2 |
    
    Examples: 
      | Course                        | Currency | Batch      |
      | DevOps Certification Training | INR      | Open Batch |
     
      
      
  @Sanity @ReferralCredits @Customlink
  Scenario Outline: Verify custom link functionality for ccavenue multiple course payment (TC_RC_001)
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Customer Delight->Customers->User Registration" Page
    And Verify User registration form
      | #Email                    | name | Country |
      | leadforms@tech.edureka.in | test | India   |
    And extract the user id
    And extract the course id for "<Course>" through "Display_title"
    And extract course price information for currency "<Currency>"
    And Navigate to "Sales->Create Custom Link" Page
    Then verify Custom link form
      | Course   | Currency   | Batch   | Amount | giveEducash | provideEMI | gateway  | checkDiscountlimit | MultiplePayment |
      | <Course> | <Currency> | <Batch> |  22000 | TRUE        | FALSE      | CCAvenue | TRUE               | TRUE,Blockchain Certification Training Course     |
    And Check Preview and send mail
    And click on pay securely for "CCAvenue"
    Then Verify CCAvenue Payment
    And extract course price information for currency "<Currency>"
    And extract course price in usd and inr
    And Assert following data in "Pre_orders" table
      | select orderid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' and courseid='$COURSE_ID'  order by id desc limit 1 | IS_NOT_NULL_OR_BLANK |
      | select courseid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' and courseid='$COURSE_ID' order by id desc limit 1 | $COURSE_ID           |
    And Assert following data in "user_course" table
      | Query                                                                                  | Expected Result |
      | select course_id from user_courses where user_id='$USER_ID' and course_id='$COURSE_ID' | $COURSE_ID      |
      | select is_paid from user_courses where user_id='$USER_ID'                              |               1 |
    And Assert following data in "users" table
      | Query                                                                                  | Expected Result |
      | select customer_status from users where id='$USER_ID' | 3 |
    
    Examples: 
      | Course                        | Currency | Batch      |
      | DevOps Certification Training | INR      | Open Batch |
     