Feature: Request batch operations

 Scenario: Verify user able to request a batch for specific course (TC_CLP_RB_001)
    Given user navigates to edureka website
   And login with below credentials
    | Email             | Password    |
    | testforequestbatch1@tech.edureka.in | 123456789 |
    #And click on search bar
    And click on course "DevOps Certification Training" from the search list
    And fills up request a batch form with below details
      | Date | #Email             | PhoneNo    | code|
      | 15   | testforequestbatch1@tech.edureka.in | 9845984590 |91|
    Then Request batch operation should be successfull and following message displayed to user "Your details have been successfully submitted. Our learning consultants will get in touch with you shortly."
    And extract the user id
    And extract the course id for slug "devops-certification-training"
    And Assert following data in "user_events" table
      |Query                                                                                                                   | ExpectedResult  | 
      | select course_id from user_events where user_id='$USER_ID'  and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' and course_id='$COURSE_ID'| $COURSE_ID      | 
      | select event_context from user_events where user_id='$USER_ID'  and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' and course_id='$COURSE_ID'| Batch Requested | 
      | select event_type from user_events where user_id='$USER_ID'  and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' and course_id='$COURSE_ID'| IN              | 

#Scenario: Verify user not able to complete request batch operation with invalid PhoneNo (TC_CLP_RB_002)
    #Given user navigates to edureka website
   #And login with below credentials
    #| Email             | Password    |
    #| testforequestbatch1@tech.edureka.in | 123456789 |
    #And click on search bar
    #And click on course "DevOps Certification Training" from the search list
    #And fills up request a batch form with below details
      #| Date | #Email             | PhoneNo    | code|
      #| 15   | requestbatch2@tech.edureka.in | 98459845901 |91|
    #Then verify following "Please enter a valid mobile number." error message is displayed on request batch screen related to "phoneno"
#	  
#Scenario: Verify user not able to complete request batch operation with invalid email (TC_CLP_RB_003)
   #Given user navigates to edureka website
   #And login with below credentials
    #| Email             | Password    |
    #| testforequestbatch1@tech.edureka.in | 123456789 |
    #And click on search bar
    #And click on course "DevOps Certification Training" from the search list
      #And fills up request a batch form with below details
      #| Date | #Email             | PhoneNo    | code|
      #| 15   | requestbatch3@tech.edureka.in1 | 9845984590 |91|
      #Then verify following "Please enter a valid email address" error message is displayed on request batch screen related to "email"
      #
#Scenario: Verify user not able to complete request batch operation with invalid email and Phone number (TC_CLP_RB_004)
    #Given user navigates to edureka website
   #And login with below credentials
    #| Email             | Password    |
    #| testforequestbatch1@tech.edureka.in | 123456789 |
    #And click on search bar
    #And click on course "DevOps Certification Training" from the search list
    #And fills up request a batch form with below details
     #| Date | #Email             | PhoneNo    | code|
     #| 15   | requestbatch4@tech.edureka.in1 | 98459845901 |91|
    #Then verify following "Please enter a valid email address" error message is displayed on request batch screen related to "email"
    #And verify following "Please enter a valid mobile number." error message is displayed on request batch screen related to "phoneno" 
