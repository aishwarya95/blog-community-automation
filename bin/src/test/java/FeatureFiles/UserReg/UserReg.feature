Feature: UserRegistration form feature

Background:
	And update password of admin
	
@Sanity
Scenario: Veriify user registration form (TC_HP_SP_001)
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Customer Delight->Customers->User Registration" Page
    And Verify User registration form
    | #Email                    | name     | Country     |
    | leadforms@tech.edureka.in | test     | India       |
    And extract the user id
        And Assert following data in "user" table
      | Query                                                         | ExpectedResult       |
      | select first_name from users where email='$EMAIL_ID'          | IS_NOT_Null_or_Blank |
      | select last_name from users where email='$EMAIL_ID'           | IS_NOT_NULL_OR_Blank |
      #| select preferred_timezone from users where email='$EMAIL_ID'  | IST                  |
      #| select preffered_currency from users where email='$EMAIL_ID'  | INR                  |
      #| select preffered_country from users where email='$EMAIL_ID'   |                   12 |
      | select customer_status from users where email='$EMAIL_ID'     |                    1 |
      | select country from users where email='$EMAIL_ID'             | IN                   |
      | select created from users where email='$EMAIL_ID'             | IS_NOT_Null_or_Blank |
      | select modified from users where email='$EMAIL_ID'            | IS_NOT_Null_or_Blank |
    