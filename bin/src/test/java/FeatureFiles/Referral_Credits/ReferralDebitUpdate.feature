Feature: Referral Credits feature

  @Sanity @ReferralCredits @Regression
  Scenario Outline: Verify referral DEBIT functionality in Referral credits form <Currency>(TC_RC_001)
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Customer Delight->Customers->User Registration" Page
    And Verify User registration form
      | #Email                    | name | Country |
      | leadforms@tech.edureka.in | test | India   |
    And Navigate to "Sales->Referral Credits->Manage Referral Credits" Page
    Then verify update referral credits functionality
      | Currency   | #Email    | Action   | Amount |
      | <Currency> | $EMAIL_ID | <Action> |   2000 |
    Then verify update referral credits functionality
      | Currency   | #Email    | Action   | Amount |
      | <Currency> | $EMAIL_ID | <SecondAction> |   1000 |  
    And extract the user id
    And Assert following data in "referral_transactions" table
      | Query                                                        	                                        | ExpectedResult       |
      | select edureka_cash_value from referral_transactions where user_id='$USER_ID' order by id desc limit 1       | $EDUCASH_POINTS      |
      | select transaction_type from referral_transactions where user_id='$USER_ID' order by id desc limit 1         | DEBIT                |
      | select medium from referral_transactions where user_id='$USER_ID'   order by id desc  limit 1                | ADMIN                |
      | select refer_medium from referral_transactions where user_id='$USER_ID' order by id desc  limit 1            | EDUREKA_ADMIN        |
   And Assert following data in "ambassadors" table
      | Query                                                        	                        | ExpectedResult       |
      | select edureka_cash from ambassadors where user_id='$USER_ID'                         | $EDUCASH_POINTS.000      |
      | select edureka_cash_promotional from ambassadors where user_id='$USER_ID'             | $EDUCASH_POINTS    |
     
    Examples: 
      | Currency | Action | SecondAction |
      | USD      | Credit | Debit        |
      