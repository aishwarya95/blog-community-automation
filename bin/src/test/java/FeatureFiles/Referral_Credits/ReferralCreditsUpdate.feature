Feature: Referral Credits feature

  @Sanity @ReferralCredits @Regression
  Scenario Outline: Verify referral credits functionality in Referral credits form <Currency>(TC_RC_001)
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Customer Delight->Customers->User Registration" Page
    And Verify User registration form
      | #Email                    | name | Country |
      | leadforms@tech.edureka.in | test | India   |
    And Navigate to "Sales->Referral Credits->Manage Referral Credits" Page
    Then verify update referral credits functionality
      | Currency   | #Email    | Action   | Amount |
      | <Currency> | $EMAIL_ID | <Action> |   2589 |
    And extract the user id
    And Assert following data in "referral_transactions" table
      | Query                                                        	                        | ExpectedResult       |
      | select edureka_cash_value from referral_transactions where user_id='$USER_ID'         | $EDUCASH_POINTS      |
      | select transaction_type from referral_transactions where user_id='$USER_ID'           | CREDIT               |
      | select medium from referral_transactions where user_id='$USER_ID'                     | ADMIN                |
      | select refer_medium from referral_transactions where user_id='$USER_ID'               | EDUREKA_ADMIN        |
   And Assert following data in "ambassadors" table
      | Query                                                        	                        | ExpectedResult       |
      | select edureka_cash from ambassadors where user_id='$USER_ID'                         | $EDUCASH_POINTS.000      |
      | select edureka_cash_promotional from ambassadors where user_id='$USER_ID'             | $EDUCASH_POINTS    |
     
    Examples: 
      | Currency | Action |
      | INR      | Credit |
      | USD      | Credit |
      | GBP      | Credit |
      | SGD      | Credit |
      | EUR      | Credit |
      | AUD      | Credit |
      | CAD      | Credit |
      
      
      
      
      