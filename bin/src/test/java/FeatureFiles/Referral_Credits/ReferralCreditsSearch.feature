Feature: Referral Credits feature

  @Sanity @ReferralCredits
  Scenario Outline: Verify search functionality in Referral credits form(TC_RC_001)
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Sales->Referral Credits->Manage Referral Credits" Page
    Then verify search functionality in referral credits
     |Case       | #Email                    | ExpectedMessage    |
     |<Casetype> | referralcredittcs@tech.edureka.in | <ExpectedMessage>  |
 
      
Examples:
    | Casetype 							         | ExpectedMessage                                    |
    | Non Registered User            | This User Doesn't Exists              						  |
    | User without Edureka cash      | This User Don't have Edureka Points                |
    
