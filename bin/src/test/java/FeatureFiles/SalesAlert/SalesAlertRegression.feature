Feature: SalesAlert form feature

Background:
	And update password of admin
	
@Sanity @SalesAlert
Scenario Outline: Veriify Sales Alert for live course with EMI(TC_SA_RG_001)
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Customer Delight->Customers->User Registration" Page
    And Verify User registration form
    | #Email                    | name     | Country     |
    | leadforms@tech.edureka.in | test     | India       |
    And extract the user id
    And Navigate to "Sales->Sales Alert" Page
    Then verify Sales alert form
      |Coursetype| Course   | Currency   | Batch   | Amount | gateway              | Message                     |
      |Live      | <Course> | <Currency> | <Batch> |  22000 | Razorpay,CCAvenue    | Data saved successfully!    |   
    And extract the course id for "<Course>" through "Analytics_title"
    And extract course price information for currency "<Currency>"
    And extract course price in usd and inr 
    And extract original price and final price for Sales Alert form with currency conversion     
    And Assert following data in "Pre_orders" table
      | select orderid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' and courseid='$COURSE_ID'  order by id desc limit 1 | IS_NOT_NULL_OR_BLANK |
      | select courseid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' and courseid='$COURSE_ID' order by id desc limit 1 | $COURSE_ID           |
    And Assert following data in "post_orders" table
      | Query                                                                                                                                                                         | Expected Result      |
      | Select orderid from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' order by id asc               | IS_NOT_NULL_OR_BLANK |
      | Select priceinr from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' order by id asc              | $PRICE_INR           |
      | Select priceusd from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' order by id asc              | $PRICE_USD           |
      | Select gateway from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' order by id asc               | invoice-Razorpay     |
      | Select gateway from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'  order by id asc              | invoice-Razorpay     |
      | Select original_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' order by id asc        | $ORIGINAL_PRICE      |
      #| Select discount_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'                        | $DISCOUNT            |
      | Select final_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'  order by id asc          | $FINAL_PRICE         |
      #| Select servicetax_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'       | $SERVICETAX          |
      | Select invoice_no from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'   order by id asc          | IS_NOT_NULL_OR_BLANK |
      | Select gateway_transaction_id from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' order by id asc | IS_NOT_NULL_OR_BLANK |
      | Select payment_type from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'  order by id asc         |                    5 |
    
      | Select orderid from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' order by id desc               | IS_NOT_NULL_OR_BLANK |
      | Select priceinr from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' order by id desc              | $PRICE_INR           |
      | Select priceusd from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' order by id desc              | $PRICE_USD           |
      | Select gateway from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' order by id desc               | invoice-CCAvenue     |
      | Select original_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' order by id desc         | $ORIGINAL_PRICE      |
      #| Select discount_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'                        | $DISCOUNT            |
      | Select final_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'  order by id desc          | $FINAL_PRICE         |
      #| Select servicetax_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'       | $SERVICETAX          |
      | Select invoice_no from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'   order by id desc           | IS_NOT_NULL_OR_BLANK |
      | Select gateway_transaction_id from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' order by id desc | IS_NOT_NULL_OR_BLANK |
      | Select payment_type from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'  order by id desc          |                    6 |
    And Assert following data in "user_course" table
      | Query                                                                                  | Expected Result |
      | select course_id from user_courses where user_id='$USER_ID' and course_id='$COURSE_ID' | $COURSE_ID      |
      | select is_paid from user_courses where user_id='$USER_ID'                              |               1 |
    And Assert following data in "users" table
      | Query                                                                                  | Expected Result |
      | select customer_status from users where id='$USER_ID'                                  | 2 |
    And Assert following data in "emi_details" table
      | Query                                                                                  | Expected Result      |
      | select order_id from emi_details where user_id='$USER_ID'                                   | IS_NOT_NULL_OR_BLANK |
      | select is_paid from emi_details where user_id='$USER_ID'                                    | 3                    |
      | select per_month_amount from emi_details where user_id='$USER_ID'                           | $FINAL_PRICE         |
      
Examples:
    | Course                  | Currency | Batch        | 
    |	Linux Administration    |  INR     | Open Batch   |
    
    
@Sanity @SalesAlert
Scenario Outline: Veriify duplicate Sales Alert case (TC_SA_RG_002)
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Customer Delight->Customers->User Registration" Page
    And Verify User registration form
    | #Email                    | name     | Country     |
    | leadforms@tech.edureka.in | test     | India       |
    And extract the user id
    And Navigate to "Sales->Sales Alert" Page
    Then verify Sales alert form
      |Coursetype| Course   | Currency   | Batch   | Amount | gateway              | Message                     |
      |Live      | <Course> | <Currency> | <Batch> |  22000 | Razorpay,CCAvenue    | Data saved successfully!    |   
   Then verify Sales alert form
      |Coursetype| Course   | Currency   | Batch   | Amount | gateway              | Message                        |
      |Live      | <Course> | <Currency> | <Batch> |  22000 | Razorpay,CCAvenue    | has already been generated     |   
      
Examples:
    | Course                  | Currency | Batch        | 
    |	Linux Administration    |  INR     | Open Batch   |