Feature: SalesAlert form feature

Background:
	And update password of admin
	
@Sanity @SalesAlert
Scenario Outline: Veriify Sales Alert for live course(TC_SA_001)
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Customer Delight->Customers->User Registration" Page
    And Verify User registration form
    | #Email                    | name     | Country     |
    | leadforms@tech.edureka.in | test     | India       |
    And extract the user id
    And Navigate to "Sales->Sales Alert" Page
    Then verify Sales alert form
      |Coursetype| Course   | Currency   | Batch   | Amount | gateway     | Message                     |
      |Live      | <Course> | <Currency> | <Batch> |  22000 | Razorpay    | Data saved successfully!    |   
    And extract the course id for "<Course>" through "Analytics_title"
    And extract course price information for currency "<Currency>"
    And extract course price in usd and inr 
    And extract original price and final price for Sales Alert form with currency conversion     
    And Assert following data in "Pre_orders" table
      | select orderid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' and courseid='$COURSE_ID'  order by id desc limit 1 | IS_NOT_NULL_OR_BLANK |
      | select courseid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' and courseid='$COURSE_ID' order by id desc limit 1 | $COURSE_ID           |
    And Assert following data in "post_orders" table
      | Query                                                                                                                                                                         | Expected Result      |
      | Select orderid from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'                | IS_NOT_NULL_OR_BLANK |
      | Select priceinr from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'               | $PRICE_INR           |
      | Select priceusd from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'               | $PRICE_USD           |
      | Select gateway from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'                | invoice-Razorpay     |
      | Select original_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'         | $ORIGINAL_PRICE      |
      #| Select discount_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'         | $DISCOUNT            |
      | Select final_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'            | $FINAL_PRICE         |
      #| Select servicetax_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'       | $SERVICETAX          |
      | Select invoice_no from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'             | IS_NOT_NULL_OR_BLANK |
      | Select gateway_transaction_id from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' | IS_NOT_NULL_OR_BLANK |
      | Select payment_type from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'           |                    1 |
    And Assert following data in "user_course" table
      | Query                                                                                  | Expected Result |
      | select course_id from user_courses where user_id='$USER_ID' and course_id='$COURSE_ID' | $COURSE_ID      |
      | select is_paid from user_courses where user_id='$USER_ID'                              |               1 |
    And Assert following data in "users" table
      | Query                                                                                  | Expected Result |
      | select customer_status from users where id='$USER_ID' | 2 |
    
Examples:
    | Course                  | Currency | Batch        | 
    |	Linux Administration    |  INR     | Open Batch   |
    
    
@Sanity @SalesAlert
Scenario Outline: Veriify Sales Alert for master course(TC_SA_002)
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Customer Delight->Customers->User Registration" Page
    And Verify User registration form
    | #Email                    | name     | Country     |
    | leadforms@tech.edureka.in | test     | India       |
    And extract the user id
    And Navigate to "Sales->Sales Alert" Page
    Then verify Sales alert form
      |Coursetype| Course   | Currency   | Batch   | Amount | gateway       | Message                     |
      |Master      | <Course> | <Currency> | <Batch> |  22000 | Razorpay    | Data saved successfully!    |   
    And extract the course id for "<Course>" through "Analytics_title"
    And extract course price information for currency "<Currency>"
    And extract course price in usd and inr 
    And extract original price and final price for Sales Alert form with currency conversion 
    #And Check Child courses in "user_courses" table
    And Assert following data in "Pre_orders" table
      | select orderid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' and courseid='$COURSE_ID'  order by id desc limit 1 | IS_NOT_NULL_OR_BLANK |
      | select courseid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' and courseid='$COURSE_ID' order by id desc limit 1 | $COURSE_ID           |
    And Assert following data in "post_orders" table
      | Query                                                                                                                                                                         | Expected Result      |
      | Select orderid from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'                | IS_NOT_NULL_OR_BLANK |
      | Select priceinr from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'               | $PRICE_INR           |
      | Select priceusd from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'               | $PRICE_USD           |
      | Select gateway from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'                | invoice-Razorpay     |
      | Select original_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'         | $ORIGINAL_PRICE      |
      #| Select discount_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'         | $DISCOUNT            |
      | Select final_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'            | $FINAL_PRICE         |
      #| Select servicetax_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'       | $SERVICETAX          |
      | Select invoice_no from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'             | IS_NOT_NULL_OR_BLANK |
      | Select gateway_transaction_id from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' | IS_NOT_NULL_OR_BLANK |
      | Select payment_type from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'           |                    1 |
    And Assert following data in "user_course" table
      | Query                                                                                  | Expected Result |
      | select course_id from user_courses where user_id='$USER_ID' and course_id='$COURSE_ID' | $COURSE_ID      |
      | select is_paid from user_courses where user_id='$USER_ID'                              |               1 |
    And Assert following data in "users" table
      | Query                                                                                  | Expected Result |
      | select customer_status from users where id='$USER_ID' | 2 |
    #And Check Child courses in "user_leads" table        
    
Examples:
    | Course                              | Currency | Batch        | 
    |	Cloud Architect Masters Program     |  USD     | Open Batch   |
    
    
@Sanity @SalesAlert
Scenario Outline: Veriify Sales Alert for active not visible course(TC_SA_003)
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Customer Delight->Customers->User Registration" Page
    And Verify User registration form
    | #Email                    | name     | Country     |
    | leadforms@tech.edureka.in | test     | India       |
    And extract the user id
    And Navigate to "Sales->Sales Alert" Page
    Then verify Sales alert form
      |Coursetype| Course   | Currency   | Batch   | Amount | gateway                             |  Message                    |
      |Active (Not visible on Site)      | <Course> | <Currency> | <Batch> |  2000 | Razorpay    | Data saved successfully!    |    
    And extract the course id for "<Course>" through "Analytics_title and not visible"
    And extract course price information for currency "<Currency>"
    And extract course price in usd and inr 
    And extract original price and final price for Sales Alert form with currency conversion     
    And Assert following data in "Pre_orders" table
      | select orderid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' and courseid='$COURSE_ID'  order by id desc limit 1 | IS_NOT_NULL_OR_BLANK |
      | select courseid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' and courseid='$COURSE_ID' order by id desc limit 1 | $COURSE_ID           |
    And Assert following data in "post_orders" table
      | Query                                                                                                                                                                         | Expected Result      |
      | Select orderid from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'                | IS_NOT_NULL_OR_BLANK |
      | Select priceinr from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'               | $PRICE_INR           |
      | Select priceusd from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'               | $PRICE_USD           |
      | Select gateway from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'                | invoice-Razorpay     |
      | Select original_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'         | $ORIGINAL_PRICE      |
      #| Select discount_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'         | $DISCOUNT            |
      | Select final_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'            | $FINAL_PRICE         |
      #| Select servicetax_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'       | $SERVICETAX          |
      | Select invoice_no from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'             | IS_NOT_NULL_OR_BLANK |
      | Select gateway_transaction_id from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' | IS_NOT_NULL_OR_BLANK |
      | Select payment_type from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'           |                    1 |
    And Assert following data in "user_course" table
      | Query                                                                                  | Expected Result |
      | select course_id from user_courses where user_id='$USER_ID' and course_id='$COURSE_ID' | $COURSE_ID      |
      | select is_paid from user_courses where user_id='$USER_ID'                              |               1 |
    And Assert following data in "users" table
      | Query                                                                                  | Expected Result |
      | select customer_status from users where id='$USER_ID' | 2 |
    
Examples:
    | Course                              | Currency | Batch        | 
    |	AngularJS                           |  SGD     | Open Batch   |
    
    
@Sanity @SalesAlert
Scenario Outline: Veriify Sales Alert for SP visible (TC_SA_003)
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Customer Delight->Customers->User Registration" Page
    And Verify User registration form
    | #Email                    | name     | Country     |
    | leadforms@tech.edureka.in | test     | India       |
    And extract the user id
    And Navigate to "Sales->Sales Alert" Page
    Then verify Sales alert form
      |Coursetype| Course                | Currency   | Batch   | Amount | gateway                | Message                    |
      |Self-Paced (Visible on Site)      | <Course> | <Currency> | <Batch> |  2000 | Razorpay    | Data saved successfully!   |   
    And extract the course id for "<Course>" through "Analytics_title"
    And extract course price in usd and inr 
    And extract original price and final price for Sales Alert form with currency conversion     
    And Assert following data in "Pre_orders" table
      | select orderid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' and courseid='$COURSE_ID'  order by id desc limit 1 | IS_NOT_NULL_OR_BLANK |
      | select courseid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' and courseid='$COURSE_ID' order by id desc limit 1 | $COURSE_ID           |
    And Assert following data in "post_orders" table
      | Query                                                                                                                                                                         | Expected Result      |
      | Select orderid from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'                | IS_NOT_NULL_OR_BLANK |
      | Select priceinr from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'               | $PRICE_INR           |
      | Select priceusd from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'               | $PRICE_USD           |
      | Select gateway from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'                | invoice-Razorpay     |
      | Select original_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'         | $ORIGINAL_PRICE      |
      #| Select discount_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'         | $DISCOUNT            |
      | Select final_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'            | $FINAL_PRICE         |
      #| Select servicetax_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'       | $SERVICETAX          |
      | Select invoice_no from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'             | IS_NOT_NULL_OR_BLANK |
      | Select gateway_transaction_id from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' | IS_NOT_NULL_OR_BLANK |
      | Select payment_type from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'           |                    1 |
    And Assert following data in "user_course" table
      | Query                                                                                  | Expected Result |
      | select course_id from user_courses where user_id='$USER_ID' and course_id='$COURSE_ID' | $COURSE_ID      |
      | select is_paid from user_courses where user_id='$USER_ID'                              |               1 |
    And Assert following data in "users" table
      | Query                                                                                  | Expected Result |
      | select customer_status from users where id='$USER_ID' | 2 |
    
Examples:
    | Course                                                    | Currency | Batch        | 
    |	Microsoft Project 2013 Training                           |  BSD     | Open Batch   |
    
    
@Sanity @SalesAlert
Scenario Outline: Veriify Sales Alert for  SP not visible (TC_SA_003)
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Customer Delight->Customers->User Registration" Page
    And Verify User registration form
    | #Email                    | name     | Country     |
    | leadforms@tech.edureka.in | test     | India       |
    And extract the user id
    And Navigate to "Sales->Sales Alert" Page
    Then verify Sales alert form
      |Coursetype                            | Course     | Currency   | Batch   | Amount | gateway     | Message                    |
      |Self-Paced (Not visible on Site)      | <Course>   | <Currency> | <Batch> |  22000 | Razorpay    | Data saved successfully!   |  
    And extract the course id for "<Course>" through "Analytics_title and not visible SP"
    And extract course price information for currency "<Currency>"
    And extract course price in usd and inr 
    And extract original price and final price for Sales Alert form with currency conversion     
    And Assert following data in "Pre_orders" table
      | select orderid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' and courseid='$COURSE_ID'  order by id desc limit 1 | IS_NOT_NULL_OR_BLANK |
      | select courseid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' and courseid='$COURSE_ID' order by id desc limit 1 | $COURSE_ID           |
    And Assert following data in "post_orders" table
      | Query                                                                                                                                                                         | Expected Result      |
      | Select orderid from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'                | IS_NOT_NULL_OR_BLANK |
      | Select priceinr from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'               | $PRICE_INR           |
      | Select priceusd from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'               | $PRICE_USD           |
      | Select gateway from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'                | invoice-Razorpay     |
      | Select original_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'         | $ORIGINAL_PRICE      |
      #| Select discount_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'         | $DISCOUNT            |
      | Select final_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'            | $FINAL_PRICE         |
      #| Select servicetax_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'       | $SERVICETAX          |
      | Select invoice_no from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'             | IS_NOT_NULL_OR_BLANK |
      | Select gateway_transaction_id from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' | IS_NOT_NULL_OR_BLANK |
      | Select payment_type from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'           |                    1 |
    And Assert following data in "user_course" table
      | Query                                                                                  | Expected Result |
      | select course_id from user_courses where user_id='$USER_ID' and course_id='$COURSE_ID' | $COURSE_ID      |
      | select is_paid from user_courses where user_id='$USER_ID'                              |               1 |
    And Assert following data in "users" table
      | Query                                                                                  | Expected Result |
      | select customer_status from users where id='$USER_ID' | 2 |
    
Examples:
    | Course                              | Currency | Batch        | 
    |	PMI Agile Certified Practitioner    |  INR     | Open Batch   |
    
@Sanity @SalesAlert
Scenario Outline: Veriify Sales Alert for offline course (TC_SA__001)
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Customer Delight->Customers->User Registration" Page
    And Verify User registration form
    | #Email                    | name     | Country     |
    | leadforms@tech.edureka.in | test     | India       |
    And extract the user id
    And Navigate to "Sales->Sales Alert" Page
    Then verify Sales alert form
      |Coursetype                            | Course     | Currency   | Batch   | Amount | gateway     | Message                    |
      |Offline                               | <Course>   | <Currency> | <Batch> |  22000 | Razorpay    | Data saved successfully!   |  
    And extract the course id for "Knowledge Hut Certified Scrum Master" through "Analytics_title"
    And extract course price information for currency "<Currency>"
    And extract course price in usd and inr 
    And extract original price and final price for Sales Alert form with currency conversion 
    And Check Child courses in "user_courses" table
    #And Check Child courses in "user_leads" table     
    And Assert following data in "Pre_orders" table
      | select orderid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' and courseid='$COURSE_ID'  order by id desc limit 1 | IS_NOT_NULL_OR_BLANK |
      | select courseid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' and courseid='$COURSE_ID' order by id desc limit 1 | $COURSE_ID           |
    And Assert following data in "post_orders" table
      | Query                                                                                                                                                                         | Expected Result      |
      | Select orderid from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'                | IS_NOT_NULL_OR_BLANK |
      | Select priceinr from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'               | $PRICE_INR           |
      | Select priceusd from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'               | $PRICE_USD           |
      | Select gateway from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'                | invoice-Razorpay     |
      | Select original_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'         | $ORIGINAL_PRICE      |
      #| Select discount_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'         | $DISCOUNT            |
      | Select final_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'            | $FINAL_PRICE         |
      #| Select servicetax_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'       | $SERVICETAX          |
      | Select invoice_no from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'             | IS_NOT_NULL_OR_BLANK |
      | Select gateway_transaction_id from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' | IS_NOT_NULL_OR_BLANK |
      | Select payment_type from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'           |                    1 |
    And Assert following data in "user_course" table
      | Query                                                                                  | Expected Result |
      | select course_id from user_courses where user_id='$USER_ID' and course_id='$COURSE_ID' | $COURSE_ID      |
      | select is_paid from user_courses where user_id='$USER_ID'                              |               1 |
    And Assert following data in "users" table
      | Query                                                                                  | Expected Result |
      | select customer_status from users where id='$USER_ID' | 2 |
    
Examples:
    | Course                                                    | Currency | Batch        | 
    |	Certified Scrum Master Training                           |  INR     | Open Batch   |    
    