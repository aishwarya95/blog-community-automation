Feature: CourseCard Feature


@Sanity @Home
Scenario Outline: Verify the  Course Card Details on Homepage "<SectionName>"
    Given user navigates to edureka website
    And check course info on "<SectionName>"

    
 Examples:
			| SectionName        				|
			|	HomePage:TrendingCourses  |
			| HomePage:MasterCourseCard |
			| HomePage:RecentAdditions  |
    
@Sanity @AllCourses 
Scenario Outline: Verify the  Course Card Details on All courses page 
    Given user navigates to edureka website   
    And navigates to "<SectionName>" page
    And check course info on "<SectionName>"

Examples:
			| SectionName        				|
			|	All Courses Page          |    
  
@Sanity @CategoryPage
Scenario Outline: Verify the  Course Card Details on Category page
    Given user navigates to edureka website
    And navigates to category "<Course>" page
    And check course info on "<Page>"
    
Examples:
			| Course        									  | Page  				|
			|	DevOps											      | CategoryPage  |