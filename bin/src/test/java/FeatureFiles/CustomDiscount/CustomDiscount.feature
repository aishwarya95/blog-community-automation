Feature: Custom Discount feature

Background:
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Sales->Custom Link Discount Admin" Page

  @Sanity  @CustomDiscount
  Scenario Outline: Verify Custom discount from admin for live course(TC_CD_001) 
    And Check Custom Discount form
      | Value                          | Case                    | CourseType        | 
      | <Value>                        | <Case>                  |  live             | 
   
    
    Examples: 
      | Case                                    | Value |
      | check Copy All button                   | 80    |
      | check Discount update for Existing User | 90    |
      | check Discount update for New User      | 90    |
      
     
     
  @Sanity  @CustomDiscount
  Scenario Outline: Verify Custom discount from admin for master course(TC_CD_002)
       And Check Custom Discount form
      | Value                          | Case                    | CourseType        | 
      | <Value>                        | <Case>                  |  master             | 
   
    
    Examples: 
      | Case                                    | Value |
      | check Copy All button                   | 80    |
      | check Discount update for Existing User | 90    |
      | check Discount update for New User      | 90    |
      
 
   @Sanity  @CustomDiscount
  Scenario Outline: Verify Custom discount from admin for SP course(TC_CD_003)
       And Check Custom Discount form
      | Value                          | Case                    | CourseType        | 
      | <Value>                        | <Case>                  |  sp               | 
    
    
    Examples: 
      | Case                                    | Value |
      | check Copy All button                   | 80    |
      | check Discount update for Existing User | 90    |
      | check Discount update for New User      | 100    |
     