Feature: PayFullPrice Feature
 
Scenario: Verify user is able to purchase course using payfull price link on course page
    Given user navigates to edureka website
     When searches for course "DevOps Certification Training" and navigates to course landing page.
     And change the currency to "INR"
      And clicks on pay full price link
      And fills up payfull price form with below details
      | #Email             | PhoneNo    | 
      | rajukarm@tech.edureka.in | 9845984590 |
      And extract the user id
      And extract the course id for slug "devops"
      And Assert following data in "user_events" table
      |select orderid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' |IS_NOT_NULL_OR_BLANK|
	  |select courseid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'  |$COURSE_ID|
	  |select gateway from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'|IS_BLANK|
	  |select token from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'|IS_BLANK|
     Then verify below course price information displayed to user
      | Total Price | GST   | Total Payable | 
      | 16,995      | 3,059 | 20,054        | 
      
Scenario: Verify user is not able to purchase course using payfull price link on course page with invalid email address
    Given user navigates to edureka website
     When searches for course "DevOps Certification Training" and navigates to course landing page.
      And clicks on pay full price link
      And fills up payfull price form with below details
      | #Email             | PhoneNo    | 
      | rajukarm@@tech.edureka.in1 | 9845984590 |
      Then verify following "Enter a valid Email ID" error message is displayed on pay full price screen related to "email"
      
Scenario: Verify user is able to purchase course using payfull price link on course page with invalid phone nnumber
    Given user navigates to edureka website
     When searches for course "DevOps Certification Training" and navigates to course landing page.
      And clicks on pay full price link
      And fills up payfull price form with below details
      | #Email             | PhoneNo    | 
      | rajukarm@@tech.edureka.in | 98459845901 |
      Then verify following "Please enter a valid mobile number." error message is displayed on pay full price screen related to "phoneno"
   
Scenario: Verify user is able to purchase course using payfull price link on course page with invalid phone nnumber
    Given user navigates to edureka website
     When searches for course "DevOps Certification Training" and navigates to course landing page.
      And clicks on pay full price link
      And fills up payfull price form with below details
      | #Email             | PhoneNo    | 
      | rajukarm@@tech.edureka.in1 | 98459845901 |
      Then verify following "Enter a valid Email ID" error message is displayed on pay full price screen related to "email"
      And verify following "Please enter a valid mobile number." error message is displayed on pay full price screen related to "phoneno"
   