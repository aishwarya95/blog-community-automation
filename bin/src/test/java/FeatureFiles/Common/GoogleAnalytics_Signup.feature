Feature: Request batch operations
 
  Scenario: Verify user able to request a batch for specific course
    Given user navigates to edureka website
    And login with global credentials
    And click on search bar
    And set course name as "DevOps"
    And assert following analytics data
    |Event Category|Event Action|Event Label|
    |Search_v2|Homepage Search_Landings|$EMAIL_ID;;Instructor-Led Online Training with 24X7 Lifetime Support \| Edureka|
    And click on course "DevOps Certification Training" from the search list
    And assert following analytics data
     |Event Category|Event Action|Event Label|
     |Search_v2|Homepage_Trending Search|Recommedation -3;$EMAIL_ID;;Instructor-Led Online Training with 24X7 Lifetime Support \| Edureka|
    And fills up request a batch form with below details
      | Date | Email     | PhoneNo    | 
      | 15   | $EMAIL_ID | $PHONE_NO |
    And assert following analytics data
      |Event Category|Event Action|Event Label|
      |CLP|First_Scroll|Lead: Lead, CourseName: DevOps, , Country:-, UserId:$USER_ID, PageTitle:DevOps Training \| DevOps Certification Course - Edureka|
      |Inquiry_Funnel|Request_Batch_Popup_Started_Typing|Lead: Lead, CourseName: $COURSE, , Country:-, UserId:$USER_ID, PageTitle:DevOps Training \| DevOps Certification Course - Edureka|
      |Inquiry_Funnel|Clicked_On_Request_Batch|Lead: Lead, CourseName: $COURSE, , Country:-, UserId:$USER_ID, PageTitle:DevOps Training \| DevOps Certification Course - Edureka|
      |Inquiry_Funnel|Request_Batch_Popup_Shown|Lead: Lead, CourseName: $COURSE, , Country:-, UserId:$USER_ID, PageTitle:DevOps Training \| DevOps Certification Course - Edureka|
      |Inquiry_Funnel|Request_Batch_Popup_Successful_Submission|Lead: Lead, CourseName: $COURSE, , Country:-, UserId:$USER_ID, PageTitle:DevOps Training \| DevOps Certification Course - Edureka|