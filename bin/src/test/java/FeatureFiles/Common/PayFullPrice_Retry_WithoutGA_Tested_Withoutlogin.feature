Feature: PayFullPrice Feature
 
Scenario: Verify user is able to purchase course using payfull price link on course page
    Given user navigates to edureka website
     When searches for course "DevOps Certification Training" and navigates to course landing page.
     And change the currency to "INR"
      And clicks on pay full price link
      And fills up payfull price form with below details
      | #Email             | PhoneNo    | 
      | rajukarm@tech.edureka.in | 9845984590 |
      And extract the user id
      And extract the course id for slug "devops"
      And Assert following data in "user_events" table
      |select orderid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' |IS_NOT_NULL_OR_BLANK|
	  |select courseid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'  |$COURSE_ID|
	  |select gateway from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'|IS_BLANK|
	  |select token from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'|IS_BLANK|
	  And select the batch "06 Sep Fri - Sat Timings: 09:30 PM - 12:30 AM"
	  And click on proceed to payment
#     And extract discount from order summary page
      And click on pay securely
      And set course name as "DevOps"
      And set currency as "inr"
      And select "netbanking" as payment method
      And select "ICICI Bank" as bank
      And make the payment through razorpay gateway
      Then verify confirmation message displayed to user
      And Assert following data in "user_events" table
      |select orderid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' |IS_NOT_NULL_OR_BLANK|
	  |select courseid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'  |$COURSE_ID|
	  |select gateway from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'|Razorpay|
	  |select token from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'|IS_NOT_NULL_OR_BLANK|
	  And extract course price information for currency "INR"
	  And extract course price in usd and inr
      And calculate service tax with percentage "18"
      And calculate final price including taxation
      And Assert following data in "post_orders" table
	  |Query|Expected Result|
	  |Select orderid from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' |IS_NOT_NULL_OR_BLANK|
	  |Select priceinr from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'|$PRICE_INR|
	  |Select priceusd from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'|$PRICE_USD|
	  |Select gateway from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'|Razorpay|
	  |Select original_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'|$ORIGINAL_PRICE|
	  |Select discount_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'|$DISCOUNT|
	  |Select final_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'|$FINAL_PRICE|
	  |Select servicetax_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'|$SERVICETAX|
	  |Select invoice_no from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'|IS_NOT_NULL_OR_BLANK|
	  And Assert following data in "user_course" table
	  |Query|Expected Result|
	  |select course_id from user_courses where user_id='$USER_ID'|$COURSE_ID|
      |select is_paid from user_courses where user_id='$USER_ID'|1|
      And Assert following data in "user_course" table
      |select customer_status from users where id='$USER_ID'|2|
      And Assert following data in "customer_records" table      
      |select count(*) from customer_records where user_id='$USER_ID' and course_id='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'|1|
      