Feature: SalesLead feature

  Background: 
    And update password of admin

  @Sanity
  Scenario Outline: Verify Add/Modify Business Unit
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Miscellaneous->Modify Business Units" Page
    Then verify Add BU
      | Name   | Contact India   | Contact US   | #Email  |
      | <Name> | <Contact India> | <Contact US> | <Email> |
    And Assert following data in "business_units" table
      | Query                                                                | ExpectedResult  |
      | select name from business_units order by id desc limit 1             | <Name>          |
      | select contact_india from business_units order by id desc limit 1    | <Contact India> |
      | select contact_us from business_units order by id desc limit 1       | <Contact US>    |
      | select contact_email from business_units order by id desc limit 1    | <Email>         |
    And Verify Edit BU
    And Assert following data in "business_units" table
      | Query                                                                | ExpectedResult              |
      | select name from business_units order by id desc limit 1             | <Name>                      |
      | select contact_india from business_units order by id desc limit 1    | +91-0987654321              |
      | select contact_us from business_units order by id desc limit 1       | +1-9999999999               |
      | select contact_email from business_units order by id desc limit 1    | checkbuedit@tech.edureka.in |

    Examples: 
      | Name   | Contact India  | Contact US    | Email                   |
      | BU New | +91-1234567890 | +1-8888888888 | checkbu@tech.edureka.in |
