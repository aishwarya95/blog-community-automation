Feature: SalesLead feature

  Background: 
    And update password of admin

  @Regression
  Scenario Outline: Verify if user is able to BOOK A PGP INTERVIEW SLOT for <Course>
    Given user navigates to edureka admin portal
    And login to admin portal
    Then Inactivate existing slots for below course
      | Course      |
      | <Course_id> |
    And Navigate to "PGP & JGP->PGP-Faculties->Add" Page
    Then Add dummy faculty
    And Navigate to "PGP & JGP->PGP - Slots->Add" Page
    And Check Upload PGP slot functionality
      | #Email                  | Courseid    |
      | aishwarya.nr@edureka.co | <Course_id> |
    And Navigate to "Sales->Lead->Sales Lead" Page
    Then verify sales lead form for email
      | #Email                    | Course   |
      | saleslead@tech.edureka.in | <Course> |
    Then verify pgp slot booking for user
      | #Email    | Course      |
      | $EMAIL_ID | <Course_id> |
    And extract the user id
    And extract the lead id
    And Assert following data in "pgp_users_academic_details" table
      | Query                                                                         | ExpectedResult       |
      | select course_id from pgp_users_academic_details where user_id='$USER_ID'     | <Course_id>          |
      | select created from pgp_users_academic_details where user_id='$USER_ID'       | IS_NOT_Null_or_Blank |
      | select modified from pgp_users_academic_details where user_id='$USER_ID'      | IS_NOT_Null_or_Blank |
    And Assert following data in "pgp_interview_slot_bookings" table
      | Query                                                                     | ExpectedResult       |
      | select created from pgp_interview_slot_bookings where user_id='$USER_ID'  | IS_NOT_Null_or_Blank |
      | select modified from pgp_interview_slot_bookings where user_id='$USER_ID' | IS_NOT_Null_or_Blank |
      | select status from pgp_interview_slot_bookings where user_id='$USER_ID'   |                    0 |

    Examples: 
      | Course             | Course_id |
      | PGP AI and ML NITW |       961 |
