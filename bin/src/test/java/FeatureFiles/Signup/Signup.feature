Feature: Signup Scenario's.

Scenario: Verify User is able to signup with valid input (TC_HP_SP_001)
    Given user navigates to edureka website
    When fills up signup form with below details
      | #Email              | PhoneNumber | code|
      | signup1@tech.edureka.in | 9035001160  | 91|
    And set following password on "Ramu@123#"
    Then signup should be successful and user lands on Home page
    And extract the user id
    And extract the lead id
    And Assert following data in "user" table
      | Query                                                        | ExpectedResult       | 
      | select first_name from users where email='$EMAIL_ID'         | IS_NOT_Null_or_Blank | 
      | select last_name from users where email='$EMAIL_ID'          | IS_NOT_NULL_OR_Blank | 
      | select is_corp from users where email='$EMAIL_ID'            | 0                    | 
      | select corp_id from users where email='$EMAIL_ID'            | 0                    | 
      | select preferred_timezone from users where email='$EMAIL_ID' | EST                  | 
      | select preffered_currency from users where email='$EMAIL_ID' | USD                  | 
      | select preffered_country from users where email='$EMAIL_ID'  | 1                    | 
      | select customer_status from users where email='$EMAIL_ID'    | 1                    | 
      | select code from users where email='$EMAIL_ID'               | +91                  | 
      | select mobile from users where email='$EMAIL_ID'             | $PHONE_NO            | 
      | select country from users where email='$EMAIL_ID'            | US                   | 
      | select utm_source from users where email='$EMAIL_ID'         | Dir                  | 
      | select utm_campaign from users where email='$EMAIL_ID'       | Dir                  | 
      | select utm_medium from users where email='$EMAIL_ID'         | Dir                  | 
      | select first_utm_source from users where email='$EMAIL_ID'   | Dir                  | 
      | select first_utm_campaign from users where email='$EMAIL_ID' | Dir                  | 
      | select first_utm_medium from users where email='$EMAIL_ID'   | Dir                  |
      | select created from users where email='$EMAIL_ID' 			 | IS_NOT_Null_or_Blank |
      | select modified from users where email='$EMAIL_ID' 			 | IS_NOT_Null_or_Blank |
    And Assert following data in "users_groups" table
      | Query                                                      | ExpectedResult       | 
      | select group_id from users_groups where user_id='$USER_ID' | 2 					  |
    And Assert following data in "user_events" table
      | Query                                                          | ExpectedResult| 
      | select course_id from user_events where user_id='$USER_ID'     | 100     | 
      | select event_context from user_events where user_id='$USER_ID' | Sign Up | 
      | select event_type from user_events where user_id='$USER_ID'    | HS      |  
    And Assert following data in "utm-params" table
      | Query                                                        | ExpectedResult       | 
      | select campaign_source from utm_params where user_id='$USER_ID' and lead_id='$LEAD_ID'  | Dir | 
      | select campaign_medium from utm_params where user_id='$USER_ID' and lead_id='$LEAD_ID'  | Dir | 
      | select campaign_term from utm_params where user_id='$USER_ID' and lead_id='$LEAD_ID'    | Dir | 
      | select campaign_content from utm_params where user_id='$USER_ID' and lead_id='$LEAD_ID' | Dir |   
   And Assert following data in "user_leads" table
      | Query                                                        	  | ExpectedResult       | 
      | select course_id from user_leads where user_id='$USER_ID'         | 100                  | 
      | select first_name from user_leads where user_id='$USER_ID'        | IS_NOT_NULL_OR_BLANK | 
      | select last_name from user_leads where user_id='$USER_ID'         | IS_NOT_NULL_OR_BLANK | 
      | select email from user_leads where user_id='$USER_ID'             | $EMAIL_ID            | 
      | select phone from user_leads where user_id='$USER_ID'             | +$CODE-$PHONE_NO     | 
      | select website_action from user_leads where user_id='$USER_ID'    | Sign Up              | 
      | select event_type from user_leads where user_id='$USER_ID'        | HS                   |
      | select zoholead_id from user_leads where user_id='$USER_ID'       | IS_NOT_NULL_OR_BLANK | 
      | select zoho_potential_id from user_leads where user_id='$USER_ID' | IS_NOT_NULL_OR_BLANK |         
    And Assert following data in "ambassador" table
      | select email_url from ambassadors where user_id='$USER_ID'  | IS_NOT_NULL_OR_BLANK | 
     | select mobile_url from ambassadors where user_id='$USER_ID' | IS_NOT_NULL_OR_BLANK |  
 
#Scenario: Verify User is able to signup with valid password on second attempt (TC_HP_SP_PWD_002)
    #Given user navigates to edureka website
     #When fills up signup form with below details
      #| #Email              | PhoneNumber |code|
      #| signup6@tech.edureka.in | 9035001160  |91|
      #And set following password on "abcd"
      #Then verify  following "Enter a valid Password" error message is displayed on signup screen related to "password"
      #And set following password on "Ramu@123#"
      #Then signup should be successful and user lands on Home page
#
#Scenario: Verify user is not able to signup with invalid phone number (TC_HP_SP_002)
    #Given user navigates to edureka website
     #When fills up signup form with below details
      #| ##Email               | PhoneNumber |code| 
      #| signup2@tech.edureka.in | 90350011601  |91| 
    #And verify  following "Please enter a valid mobile number." error message is displayed on signup screen related to "phoneno"
    #
#	
#Scenario: Verify user is not able to signup with invalid email (TC_HP_SP_003)
   #Given user navigates to edureka website
    #When fills up signup form with below details
      #| Email               | PhoneNumber |code|
      #| signup3@@tech.edureka.in1 | 9035001160  | 91|
    #Then verify  following "Enter a valid Email address" error message is displayed on signup screen related to "email"
 #
#Scenario: Verify user is not able to signup with invalid email and phone number (TC_HP_SP_004)
    #Given user navigates to edureka website
     #When fills up signup form with below details
      #| Email               | PhoneNumber |code| 
      #| signup4@@tech.edureka.in1 | 90350011601  | 91|
    #Then verify  following "Enter a valid Email address" error message is displayed on signup screen related to "email"
    #And verify  following "Please enter a valid mobile number." error message is displayed on signup screen related to "phoneno"
 #
 #
#Scenario: Verify User is not able to signup with invalid password (TC_HP_SP_PWD_001)
    #Given user navigates to edureka website
     #When fills up signup form with below details
      #| ##Email              | PhoneNumber |code| 
      #| signup5@tech.edureka.in | 9035001160  |91| 
      #And set following password on "abcd"
      #Then verify  following "Enter a valid Password" error message is displayed on signup screen related to "password"
        #
      #
#Scenario: Negativce scenario 1 - expected to fail in DB Assertion
    #Given user navigates to edureka website
    #When fills up signup form with below details
      #| #Email              | PhoneNumber | code|
      #| signup7@tech.edureka.in | 9035001160  | 91|
    #And set following password on "Ramu@123#"
    #Then signup should be successful and user lands on Home page
    #And extract the user id
    #And extract the lead id
    #And Assert following data in "user" table
      #| Query                                                        | ExpectedResult       |
      #| select first_name from users where email='$EMAIL_ID'         | IS_NOT_Null_or_Blank |
      #| select last_name from users where email='$EMAIL_ID'          | IS_NOT_NULL_OR_Blank |
      #| select is_corp from users where email='$EMAIL_ID'            | 0                    |
      #| select corp_id from users where email='$EMAIL_ID'            | 0                    |
      #| select preferred_timezone from users where email='$EMAIL_ID' | EST                 |
      #| select preffered_currency from users where email='$EMAIL_ID' | USD                 |
      #| select preffered_country from users where email='$EMAIL_ID'  | 1                   |
      #| select customer_status from users where email='$EMAIL_ID'    | 1                    |
      #| select code from users where email='$EMAIL_ID'               | +91                  |
      #| select mobile from users where email='$EMAIL_ID'             | $PHONE_NO            |
      #| select country from users where email='$EMAIL_ID'            | US                   |
      #| select utm_source from users where email='$EMAIL_ID'         | Dir                  |
      #| select utm_campaign from users where email='$EMAIL_ID'       | Dir                  |
      #| select utm_medium from users where email='$EMAIL_ID'         | Dir                  |
      #| select first_utm_source from users where email='$EMAIL_ID'   | Dir                  |
      #| select first_utm_campaign from users where email='$EMAIL_ID' | Dir                  |
      #| select first_utm_medium from users where email='$EMAIL_ID'   | Dir                  |
      #| select created from users where email='$EMAIL_ID'                        | IS_NOT_Null_or_Blank |
      #| select modified from users where email='$EMAIL_ID'                       | IS_NOT_Null_or_Blank |
