Feature: CourseAssignment feature

  @Sanity  @CourseAssignment
  Scenario Outline: Verify Course Assignment functionality for live course(TC_CA_001)
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Customer Delight->Customers->User Registration" Page
    And Verify User registration form
      | #Email                    | name | Country |
      | leadforms@tech.edureka.in | test | India   |
    And Navigate to "Customer Delight->Customers->Course Assignment" Page
    And Check Course Assignment form
      | #Email    | Course   | Batch      | Reason              |
      | $EMAIL_ID | <Course> | Open Batch | Issues with content |
    And extract the user id
    And Assert following data in "user_course" table
      | Query                                                       | Expected Result |
      | select course_id from user_courses where user_id='$USER_ID' |             776 |
      | select is_paid from user_courses where user_id='$USER_ID'   |               1 |
    And Assert following data in "users" table
      | select customer_status from users where id='$USER_ID' | 2 |
    And Assert following data in "customer_records" table
      | select count(*) from customer_records where user_id='$USER_ID' and course_id='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' | 1 |

    Examples: 
      | Course     | Currency | Batch      |
      | DevOps - 7 | INR      | Open Batch |

  @Sanity  @CourseAssignment
  Scenario Outline: Verify Course Assignment functionality for Self Paced course(TC_CA_001)
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Customer Delight->Customers->User Registration" Page
    And Verify User registration form
      | #Email                    | name | Country |
      | leadforms@tech.edureka.in | test | India   |
    And Navigate to "Customer Delight->Customers->Course Assignment" Page
    And Check Course Assignment form
      | #Email    | Course   | Batch      | Reason              |
      | $EMAIL_ID | <Course> | Open Batch | Issues with content |
    And extract the user id
    And Assert following data in "user_course" table
      | Query                                                       | Expected Result |
      | select course_id from user_courses where user_id='$USER_ID' |             599 |
      | select is_paid from user_courses where user_id='$USER_ID'   |               1 |
    And Assert following data in "users" table
      | select customer_status from users where id='$USER_ID' | 2 |
    And Assert following data in "customer_records" table
      | select count(*) from customer_records where user_id='$USER_ID' and course_id='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' | 1 |

    Examples: 
      | Course                      | Batch      |
      | Continuous Integration with | Open Batch |

  @Sanity  @CourseAssignment
  Scenario Outline: Verify Course Assignment functionality for Master course(TC_CA_001)
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Customer Delight->Customers->User Registration" Page
    And Verify User registration form
      | #Email                    | name | Country |
      | leadforms@tech.edureka.in | test | India   |
    And Navigate to "Customer Delight->Customers->Course Assignment" Page
    And Check Course Assignment form
      | #Email    | Course   | Batch      | Reason              |
      | $EMAIL_ID | <Course> | Open Batch | Issues with content |
    And extract the user id
    And extract the course id for "<Course>" through "Analytics_title"
    And Check Child courses in "user_courses" table
    And Assert following data in "user_course" table
      | Query                                                                                            | Expected Result |
      | select course_id from user_courses where user_id='$USER_ID' order by id asc limit 1              | $COURSE_ID      |
      | select is_paid from user_courses where user_id='$USER_ID'  order by id asc limit 1               |               1 |
    And Assert following data in "users" table
      | select customer_status from users where id='$USER_ID' | 2 |
    And Assert following data in "customer_records" table
      | select count(*) from customer_records where user_id='$USER_ID' and course_id='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' | 1 |

    Examples: 
      | Course                              | Batch      |
      | Automation Engineer Masters Program | Open Batch |
