Feature: PgpPayments feature

  Background: 
    And update password of admin

  @Sanity
  Scenario Outline: Verify Pgp upfront payment through Sales Alert (TC_PGP_001) for <Course> in <Currency>
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Sales->Lead->Sales Lead" Page
    Then verify sales lead form for email
      | #Email                    | Course   |
      | saleslead@tech.edureka.in | PGD AI and ML NITW |
    And Navigate to "PGP Payments->PGP Sales Alert" Page
    Then verify pgp Upfront Sales Alert payment
      | Email     | Course_pay | Currency   | Gateway   | PaidInterviewFee    | AlertMessage            | Amount |
      | $EMAIL_ID | <Course>   | <Currency> | <Gateway> | FALSE               | Data saved successfully | 5000   |
    
    And extract the user id
    And extract the course id for "<Course>" through "Analytics_title"
    And extract course price in usd and inr
    And Check Child courses in "user_courses" table
    #And Check Child courses in "user_leads" table
    And Assert following data in "Pre_orders" table
      | select orderid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' order by id desc limit 1 | IS_NOT_NULL_OR_BLANK |
      | select courseid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' order by id desc limit 1 | 817                  |
    And Assert following data in "post_orders" table
      | Query                                                                                                                                                                         | Expected Result      |
      | Select orderid from post_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'                | IS_NOT_NULL_OR_BLANK |
      | Select priceinr from post_orders where userid='$USER_ID'and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'               | $PRICE_INR           |
      | Select priceusd from post_orders where userid='$USER_ID'  and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'               | $PRICE_USD           |
      | Select gateway from post_orders where userid='$USER_ID'  and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'                | invoice-<Gateway>     |
      | Select original_value from post_orders where userid='$USER_ID'  and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'         | $ORIGINAL_PRICE      |
      #| Select discount_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'         | $DISCOUNT            |
      | Select final_value from post_orders where userid='$USER_ID'  and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'            | $FINAL_PRICE         |
      #| Select servicetax_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'       | $SERVICETAX          |
      | Select invoice_no from post_orders where userid='$USER_ID'  and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'             | IS_NOT_NULL_OR_BLANK |
      | Select gateway_transaction_id from post_orders where userid='$USER_ID'  and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' | IS_NOT_NULL_OR_BLANK |
      | Select payment_type from post_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'           |                    5  |
    
    And Assert following data in "users" table
      | Query                                                 | Expected Result |
      | select customer_status from users where id='$USER_ID' |               3 |

    Examples: 
      | Course             | Currency | Gateway  | Course_Display                                                        |
      | PGD AI and ML NITW | INR      | Razorpay | Post Graduate Diploma in Artificial Intelligence and Machine Learning |
      | PGD AI and ML NITW | USD      | Paypal   | Post Graduate Diploma in Artificial Intelligence and Machine Learning |
      | PGD AI and ML NITW | SGD      | PayU     | Post Graduate Diploma in Artificial Intelligence and Machine Learning |
