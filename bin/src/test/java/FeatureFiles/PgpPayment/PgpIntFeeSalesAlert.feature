Feature: PgpPayments feature

  Background: 
    And update password of admin

  @Sanity
  Scenario Outline: Verify Pgp Interview Fee Sales Alert (TC_PGP_SA_001) for <Course> and currency <Currency>
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Sales->Lead->Sales Lead" Page
    Then verify sales lead form for email
      | #Email                    | Course   |
      | saleslead@tech.edureka.in | <Course> |
    And Navigate to "PGP Payments->PGP Sales Alert" Page
    Then verify pgp interview payment fee sales alert
      | #Email    | Course_pay   | Currency   | Gateway   | Fee   | Agent   |
      | $EMAIL_ID | <Course_pay> | <Currency> | <Gateway> | <Fee> | <Agent> |
    And extract the user id
    And extract the course id for "<Course_Display>" through "Display_title"
    And extract course price information for currency "<Currency>"
    And extract course price in usd and inr
    And Assert following data in "post_orders" table
      | Query                                                                                                                                                                | Expected Result      |
      | Select orderid from post_orders where userid='$USER_ID' and courseid=817 and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'                | IS_NOT_NULL_OR_BLANK |
      | Select priceusd from post_orders where userid='$USER_ID' and courseid=817 and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'               |                   20 |
      | Select gateway from post_orders where userid='$USER_ID' and courseid=817 and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'                | <Invoice>            |
      | Select final_value from post_orders where userid='$USER_ID' and courseid=817 and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'            | <Fee>                |
      | Select invoice_no from post_orders where userid='$USER_ID' and courseid=817 and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'             | IS_NOT_NULL_OR_BLANK |
      | Select gateway_transaction_id from post_orders where userid='$USER_ID' and courseid=817 and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' | IS_NOT_NULL_OR_BLANK |
      | Select payment_type from post_orders where userid='$USER_ID' and courseid=817 and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'           |                    2 |

    Examples: 
      | Course             | Course_pay                    | Currency | Gateway | Fee | Agent             | Invoice        | Course_Display                                                        |
      | PGD AI and ML NITW | PGD AI and ML NITW - 3 - 1268 | INR      | Razorpay | 1000 | aaushi@edureka.co | invoice-Razorpay | Post Graduate Diploma in Artificial Intelligence and Machine Learning |
      | PGD AI and ML NITW | PGD AI and ML NITW - 3 - 1268 | USD      | PayPal  |  20 | aaushi@edureka.co | invoice-Paypal | Post Graduate Diploma in Artificial Intelligence and Machine Learning |
