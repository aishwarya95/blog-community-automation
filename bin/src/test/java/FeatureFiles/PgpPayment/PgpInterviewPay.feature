Feature: PgpPayments feature

  Background: 
    And update password of admin

  @Sanity
  Scenario Outline: Verify Pgp Interview payment (TC_PGP_001) for <Course>
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Sales->Lead->Sales Lead" Page
    Then verify sales lead form for email
      | #Email                    | Course   |
      | saleslead@tech.edureka.in | <Course> |
    And Navigate to "PGP Payments->PGP Payment Link" Page
    Then verify pgp interview payment
      | #Email    | Course_pay   | Currency   | Gateway   |
      | $EMAIL_ID | <Course_pay> | <Currency> | <Gateway> |
    And click on pay securely for "RazorPay"
    And select "netbanking" as payment method
    And select "ICICI Bank" as bank
    And make the payment through razorpay gateway
    Then verify confirmation message displayed to user
    And extract the user id
    #And extract the course id for "<Course_Display>" through "Display_title"
    #And extract course price information for currency "<Currency>"
    #And extract course price in usd and inr
    And Assert following data in "post_orders" table
      | Query                                                                                                                                                                | Expected Result      |
      | Select orderid from post_orders where userid='$USER_ID' and courseid=817 and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'                | IS_NOT_NULL_OR_BLANK |
      | Select priceinr from post_orders where userid='$USER_ID' and courseid=817 and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'               |                 1000 |
      | Select priceusd from post_orders where userid='$USER_ID' and courseid=817 and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'               |                   20 |
      | Select gateway from post_orders where userid='$USER_ID' and courseid=817 and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'                | invoice-Razorpay     |
      | Select final_value from post_orders where userid='$USER_ID' and courseid=817 and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'            |                 1000 |
      | Select servicetax_value from post_orders where userid='$USER_ID' and courseid=817 and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'       |                  153 |
      | Select invoice_no from post_orders where userid='$USER_ID' and courseid=817 and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'             | IS_NOT_NULL_OR_BLANK |
      | Select gateway_transaction_id from post_orders where userid='$USER_ID' and courseid=817 and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' | IS_NOT_NULL_OR_BLANK |
      | Select payment_type from post_orders where userid='$USER_ID' and courseid=817 and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'           |                    2 |

    Examples: 
      | Course             | Course_pay           | Currency | Gateway  | Course_Display                                                        |
      | PGD AI and ML NITW | PGD AI and ML NITW 3 | INR      | Razorpay | Post Graduate Diploma in Artificial Intelligence and Machine Learning |

  @Sanity
  Scenario Outline: Verify Pgp Interview payment (TC_PGP_002) for <Course> and currency <Currency>
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Sales->Lead->Sales Lead" Page
    Then verify sales lead form for email
      | #Email                    | Course   |
      | saleslead@tech.edureka.in | <Course> |
    And Navigate to "PGP Payments->PGP Payment Link" Page
    Then verify pgp interview payment
      | #Email    | Course_pay   | Currency   | Gateway   |
      | $EMAIL_ID | <Course_pay> | <Currency> | <Gateway> |
    And click on paypal
    And make the payment through Paypal gateway
    Then verify confirmation message displayed to user
    And extract the user id
    And Assert following data in "post_orders" table
      | Query                                                                                                                                                                | Expected Result      |
      | Select orderid from post_orders where userid='$USER_ID' and courseid=817 and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'                | IS_NOT_NULL_OR_BLANK |
      | Select priceinr from post_orders where userid='$USER_ID' and courseid=817 and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'               |                 1000 |
      | Select priceusd from post_orders where userid='$USER_ID' and courseid=817 and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'               |                   20 |
      | Select gateway from post_orders where userid='$USER_ID' and courseid=817 and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'                | invoice-Paypal       |
      | Select final_value from post_orders where userid='$USER_ID' and courseid=817 and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'            | <Final_value>         |
      | Select servicetax_value from post_orders where userid='$USER_ID' and courseid=817 and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'       |                    0 |
      | Select invoice_no from post_orders where userid='$USER_ID' and courseid=817 and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'             | IS_NOT_NULL_OR_BLANK |
      | Select gateway_transaction_id from post_orders where userid='$USER_ID' and courseid=817 and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' | IS_NOT_NULL_OR_BLANK |
      | Select payment_type from post_orders where userid='$USER_ID' and courseid=817 and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'           |                    2 |

    Examples: 
      | Course             | Course_pay           | Currency | Gateway | Course_Display                                                        | Final_value |
      | PGD AI and ML NITW | PGD AI and ML NITW 3 | USD      | Paypal  | Post Graduate Diploma in Artificial Intelligence and Machine Learning | 20 |
      | PGD AI and ML NITW | PGD AI and ML NITW 3 | GBP      | Paypal  | Post Graduate Diploma in Artificial Intelligence and Machine Learning |          19 |
