Feature: CustomLink feature

  @Sanity  @CustomlinkValidity
  Scenario Outline: Verify extend custom link validity(TC_ECL_001)
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Customer Delight->Customers->User Registration" Page
    And Verify User registration form
      | #Email                    | name | Country |
      | leadforms@tech.edureka.in | test | India   |
    And extract the user id
    And extract the course id for "<Course>" through "Display_title"
    And extract course price information for currency "<Currency>"
    And Navigate to "Sales->Create Custom Link" Page
    Then verify Custom link form
      | Course   | Currency   | Batch   | Amount | giveEducash | provideEMI | gateway  | checkDiscountlimit | MultiplePayment |
      | <Course> | <Currency> | <Batch> |  22000 | TRUE        | FALSE      | CCAvenue | TRUE               | FALSE     |
    And Create a New session
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Sales->Update Custom Link Validity" Page
    And Check Extend Custom Link Validity function,extend link to "45 mins" and assert "Updated discount validity" message
    And Check updated validity value "$EMAIL_ID" for "45"
    
    Examples: 
      | Course                        | Currency | Batch      |
      | DevOps Certification Training | INR      | Open Batch |
     
      
      
  @Sanity  @CustomlinkValidity  
  Scenario Outline: Verify extend custom link validity for Paypal EMI(TC_ECL_002)
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Customer Delight->Customers->User Registration" Page
    And Verify User registration form
      | #Email                    | name | Country |
      | leadforms@tech.edureka.in | test | India   |
    And extract the user id
    And extract the course id for "<Course>" through "Display_title"
    And extract course price information for currency "<Currency>"
    And Navigate to "Sales->Create Custom Link" Page
    Then verify Custom link form
      | Course   | Currency   | Batch   | Amount | giveEducash | provideEMI | gateway      | checkDiscountlimit | MultiplePayment |
      | <Course> | <Currency> | <Batch> |  22000 | TRUE        | TRUE       | Paypal-EMI   | TRUE               | FALSE           |
    And Create a New session
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Sales->Update Custom Link Validity" Page
    And Check Extend Custom Link Validity function,extend link to "3 hours" and assert "validity updation for paypal subscription is not allowed" message
      
        Examples: 
      | Course                        | Currency | Batch      |
      | RPA Developer Masters Program | USD      | Open Batch |
     