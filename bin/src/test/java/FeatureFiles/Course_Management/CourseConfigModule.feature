Feature: CourseManagement feature

  Background: 
    And update password of admin

  @Sanity
  Scenario Outline: Verify Course Config Forms in Course Management Module(TC_CM_001)
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Course Management ->Manage Courses" Page
    And extract the course id for "<Course>" through "Display_title"
    Then In Course Management search for "<Course>"
    Then Verify "Course Config"  modules in Course Management
    And Assert following data in "courses" table
      | Query                                                           | ExpectedResult       |
      | select mailchimp_shortCode from courses where id='$COURSE_ID'   | $SHORT_CODE          |
      | select short_name from courses where id='$COURSE_ID'            | $SHORT_NAME          |
      | select mailchimp_welcomeMail from courses where id='$COURSE_ID' | $WELCOME_MAIL        |
      | select mailchimp_enrollMail from courses where id='$COURSE_ID'  | $ENROLL_MAIL         |
      | select sparkpost_welcomeMail from courses where id='$COURSE_ID' | $SPWELCOME_MAIL      |
      | select sparkpost_enrollMail from courses where id='$COURSE_ID'  | $SPENROLL_MAIL       |
  
   
    Examples: 
      | Course                        |
      | DevOps Certification Training |
