Feature: PayFullPrice Feature
 
Scenario: Verify user is able to purchase course using payfull price link on course page
    Given user navigates to edureka website
    And login with global credentials
    And click on search bar
    And set course name as "DevOps"
    And set currency as "inr"
    And click on course "DevOps Certification Training" from the search list
    And change the currency to "INR"
    And clicks on pay full price link
    And assert following analytics data
    |Event Category|Event Action|Event Label|
    |Enroll_Funnel_CLP|Clicked_On_Enroll_Batch_Table|Lead: Lead, CourseName: $COURSE, , Country:-, UserId:$USER_ID, PageTitle:DevOps Training \| DevOps Certification Course - Edureka|
     And fills up payfull price form with below details
      | #Email             | PhoneNo    | 
      | rajukarm@tech.edureka.com1 | 98459845901 |
      And assert following analytics data
      |Event Category|Event Action|Event Label|
      |Enroll_Funnel_CLP|Enroll_Batch_Table_PopUp_Started_Typing|Lead: Lead, CourseName: DevOps, , Country:$COUNTRY, UserId:$USER_ID, PageTitle:DevOps Training \| DevOps Certification Course - Edureka|
      |Enroll_Funnel_CLP|Enroll_Batch_Table_PopUp_Error_Message|Lead: Lead, CourseName: DevOps, Email, , Country:$COUNTRY, UserId:$USER_ID, PageTitle:DevOps Training \| DevOps Certification Course - Edureka|
      |Enroll_Funnel_CLP|Enroll_Batch_Table_PopUp_Error_Message|Lead: Lead, CourseName: DevOps, Mobile, , Country:$COUNTRY, UserId:$USER_ID, PageTitle:DevOps Training \| DevOps Certification Course - Edureka|
      And fills up payfull price form with below details
      | #Email             | PhoneNo    | 
      | rajukarm@tech.edureka.com | 9845984590 |
      And assert following analytics data
      |Event Category|Event Action|Event Label|
      |Enroll_Funnel_CLP|Enroll_Batch_Table_Successful_Submission|Lead: Lead, CourseName: DevOps, , Country:$COUNTRY, UserId:$USER_ID, PageTitle:DevOps Training \| DevOps Certification Course - Edureka|
	And select the batch "06 Sep Fri - Sat Timings: 09:30 PM - 12:30 AM"
	And assert following analytics data
	|Event Category|Event Action|Event Label|
    |Checkout Flow|Step2_Batch_Selected|UserId:$USER_ID, ,page title:https://ecom.stage3.edureka.in/payment|
	And click on proceed to payment
	And assert following analytics data
    |Event Category|Event Action|Event Label|
	|Checkout Flow|Step2_Clicked_Proceed_to_Payment|UserId:$USER_ID, ,page title:https://ecom.stage3.edureka.in/payment|
#     And extract discount from order summary page
      And click on pay securely
      And assert following analytics data
      |Event Category|Event Action|Event Label|
	  |Checkout Flow|Step3_Clicked_Pay_Securely|Currency : $CURRENCY--UserId: $USER_ID---|