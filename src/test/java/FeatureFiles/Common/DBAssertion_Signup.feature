Feature: Signup Scenario's.

Scenario: Verify User is able to signup with valid input
    Given user navigates to edureka website
     When fills up signup form with below details
      | #Email              | PhoneNumber | 
      | rajkumarc1003@gmail.com | 9035001160  | 
      And set following password on "Ramu@123#"
     Then signup should be successful and user lands on Home page
     And extract the user id
     And extract the lead id
   And Assert following data in "all" table
    | Query                                                        | ExpectedResult       | 
      | select course_id from user_events where user_id='$USER_ID'     | 100     | 
      | select event_context from user_events where user_id='$USER_ID' | Sign Up | 
      | select event_type from user_events where user_id='$USER_ID'    | HS      |  
      | select campaign_source from utm_params where user_id='$USER_ID' and lead_id='$LEAD_ID'  | Dir | 
      | select campaign_medium from utm_params where user_id='$USER_ID' and lead_id='$LEAD_ID'  | Dir | 
      | select campaign_term from utm_params where user_id='$USER_ID' and lead_id='$LEAD_ID'    | Dir | 
      | select campaign_content from utm_params where user_id='$USER_ID' and lead_id='$LEAD_ID' | Dir |   
      | select first_name from users where email='$EMAIL_ID'         | IS_NOT_Null_or_Blank | 
      | select last_name from users where email='$EMAIL_ID'          | IS_NOT_NULL_OR_Blank | 
      | select is_corp from users where email='$EMAIL_ID'            | 0                    | 
      | select corp_id from users where email='$EMAIL_ID'            | 0                    | 
      | select preferred_timezone from users where email='$EMAIL_ID' | EST                  | 
      | select preffered_currency from users where email='$EMAIL_ID' | USD                  | 
      | select preffered_country from users where email='$EMAIL_ID'  | 1                    | 
      | select customer_status from users where email='$EMAIL_ID'    | 1                    | 
      | select code from users where email='$EMAIL_ID'               | +1                   | 
      | select mobile from users where email='$EMAIL_ID'             | $PHONE_NO            | 
      | select country from users where email='$EMAIL_ID'            | US                   | 
      | select utm_source from users where email='$EMAIL_ID'         | Dir                  | 
      | select utm_campaign from users where email='$EMAIL_ID'       | Dir                  | 
      | select utm_medium from users where email='$EMAIL_ID'         | Dir                  | 
      | select first_utm_source from users where email='$EMAIL_ID'   | Dir                  | 
      | select first_utm_campaign from users where email='$EMAIL_ID' | Dir                  | 
      | select first_utm_medium from users where email='$EMAIL_ID'   | Dir                  |
      | select email_url from ambassadors where user_id='$USER_ID'  | IS_NOT_NULL_OR_BLANK | 
      | select mobile_url from ambassadors where user_id='$USER_ID' | IS_NOT_NULL_OR_BLANK |  
      | select group_id from users_groups where user_id='$USER_ID' | 2 |
      | select zoholead_id from user_leads where user_id='$USER_ID'       | IS_NOT_NULL_OR_BLANK | 
      | select zoho_potential_id from user_leads where user_id='$USER_ID' | IS_NOT_NULL_OR_BLANK | 
      | select course_id from user_leads where user_id='$USER_ID'         | 100                  | 
      | select first_name from user_leads where user_id='$USER_ID'        | IS_NOT_NULL_OR_BLANK | 
      | select last_name from user_leads where user_id='$USER_ID'         | IS_NOT_NULL_OR_BLANK | 
      | select email from user_leads where user_id='$USER_ID'             | $EMAIL_ID               | 
      | select phone from user_leads where user_id='$USER_ID'             | $PHONE_NO               | 
      | select website_action from user_leads where user_id='$USER_ID'    | Sign Up              | 
      | select event_type from user_leads where user_id='$USER_ID'        | HS                   | 