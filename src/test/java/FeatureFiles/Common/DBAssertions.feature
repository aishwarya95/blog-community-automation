Feature: DB Scenario's testing

Scenario: Extracting userid and course id
And extract the user id
And extract the course id
And Assert following data in "user" table
      | Query                                                                            | ExpectedResult | 
      | select first_name from users where email='manjunath.gundappa@e2open.com'         | manjunath1      | 
      | select last_name from users where email='manjunath.gundappa@e2open.com'          | IS_NOT_Null_or_Blank       | 
      | select is_corp from users where email='manjunath.gundappa@e2open.com'            | 00              | 
      | select corp_id from users where email='manjunath.gundappa@e2open.com'            | 0              | 
      | select preferred_timezone from users where email='manjunath.gundappa@e2open.com' | IST1            | 
      | select preferred_currency from users where email='manjunath.gundappa@e2open.com' | INR            | 
      | select preferred_country from users where email='manjunath.gundappa@e2open.com'  | 121             | 
      | select customer_status from users where email='manjunath.gundappa@e2open.com'    | 1              | 
      | select code from users where email='manjunath.gundappa@e2open.com'               | 911             | 
      | select mobile from users where email='manjunath.gundappa@e2open.com'             | 9090111100     | 
      | select country from users where email='manjunath.gundappa@e2open.com1'            | IN             | 
      | select utm_source from users where email='manjunath.gundappa@e2open.com1'         | Dir            | 
      | select utm_campaign from users where email='manjunath.gundappa@e2open.com1'       | Dir            | 
      | select utm_medium from users where email='manjunath.gundappa@e2open.com'         | Dir            | 
      | select first_utm_source from users where email='manjunath.gundappa@e2open.com'   | Dir            | 
      | select first_utm_campaign from users where email='manjunath.gundappa@e2open.com' | Dir            | 
      | select first_utm_medium from users where email='manjunath.gundappa@e2open.com'   | Dir            | 
  
  