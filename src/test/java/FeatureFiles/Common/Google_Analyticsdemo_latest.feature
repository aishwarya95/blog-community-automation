Feature: Google analytics verification.


Scenario: Google analytics verification for click on request batch operation
    Given user navigates to edureka website
    And login with global credentials
    And click on search bar
    And set course name as "DevOps"
    And click on course "DevOps Certification Training" from the search list
	And fills up request a batch form with below details
      | Date | #Email             | PhoneNo    | code|
      | 15   | rajukarm@tech.edureka.co | 9845984590 |91|
     And assert following analytics data
      |Event Category|Event Action|Event Label|
      |Inquiry_Funnel|Request_Batch_Popup_Started_Typing|Lead: Lead, CourseName: DevOps, , Country:-, UserId:$USER_ID, PageTitle:DevOps Training & Certification Course with Live Training - Edureka|
      |Inquiry_Funnel|Clicked_On_Request_Batch|Lead: Lead, CourseName: DevOps, , Country:-, UserId:$USER_ID, PageTitle:DevOps Training & Certification Course with Live Training - Edureka|
      |Inquiry_Funnel|Request_Batch_Popup_Shown|Lead: Lead, CourseName: DevOps, , Country:-, UserId:$USER_ID, PageTitle:DevOps Training & Certification Course with Live Training - Edureka|
      |Inquiry_Funnel|Request_Batch_Popup_Successful_Submission|Lead: Lead, CourseName: DevOps, , Country:-, UserId:$USER_ID, PageTitle:DevOps Training & Certification Course with Live Training - Edureka|


Scenario: Google analytics verification for request batch operation with invalid email
    Given user navigates to edureka website
    And login with global credentials
    And click on search bar
    And set course name as "DevOps"
    And click on course "DevOps Certification Training" from the search list
	And fills up request a batch form with below details
      | Date | #Email             | PhoneNo    | code|
      | 15   | $EMAIL1111111111 | $PHONE_NO |91|
     And assert following analytics data
      |Event Category|Event Action|Event Label|
      |Enroll_Funnel_CLP|Enroll_Batch_Table_PopUp_Error_Message|Lead: Lead, CourseName: DevOps, Email, , Country:-, UserId:$USER_ID, PageTitle:DevOps Training & Certification Course with Live Training - Edureka|
      
Scenario: Google analytics verification for request batch operation with invalid password
    Given user navigates to edureka website
    And login with global credentials
    And click on search bar
    And set course name as "DevOps"
    And click on course "DevOps Certification Training" from the search list
	And fills up request a batch form with below details
      | Date | #Email             | PhoneNo    |code| 
      | 15   | $EMAIL | $PHONE_NO1111111111 |91|
     And assert following analytics data
      |Event Category|Event Action|Event Label|
      |Enroll_Funnel_CLP|Enroll_Batch_Table_PopUp_Error_Message|Lead: Lead, CourseName: DevOps, Email, , Country:-, UserId:$USER_ID, PageTitle:DevOps Training & Certification Course with Live Training - Edureka|
      


Scenario: Verify Google analytics for click on search bar event
    Given user navigates to edureka website
    And login with global credentials
    And click on search bar
    And assert following analytics data
    |Event Category|Event Action|Event Label|
    |Search_v2|Homepage Search_Landings|$EMAIL_ID;;Instructor-Led Online Training with 24X7 Lifetime Support \| Edureka|
    
Scenario: Verify Google analytics for course selection event
    Given user navigates to edureka website
    And login with global credentials
    And click on search bar
    And click on course "DevOps Certification Training" from the search list
    And assert following analytics data
     |Event Category|Event Action|Event Label|
     |Search_v2|Homepage_Trending Search|Recommedation -3;$EMAIL_ID;;Instructor-Led Online Training with 24X7 Lifetime Support \| Edureka|
    
 Scenario: Google analytics verification for First scroll action on course landing page 
    Given user navigates to edureka website
    And login with global credentials
    And click on search bar
    And set course name as "DevOps"
    And click on course "DevOps Certification Training" from the search list
    And fills up request a batch form with below details
      | Date | Email     | PhoneNo    | 
      | 15   | $EMAIL | $PHONE_NO |
    And assert following analytics data
      |Event Category|Event Action|Event Label|
      |CLP|First_Scroll|Lead: NoLead, CourseName: DevOps, , Country:India, UserId:$USER_ID, PageTitle:DevOps Training \| DevOps Certification Course - Edureka|
 
Scenario: Google analytics verification for click on upcoming batch on course landing page
    Given user navigates to edureka website
    And login with global credentials
    And click on search bar
    And set course name as "DevOps"
    And click on course "DevOps Certification Training" from the search list
	And click on upcoming batch
	And assert following analytics data
	|Event Category|Event Action|Event Label|
	|CLP|Clicked_On_Batches_Nav|Lead: Lead, CourseName: DevOps, , Country:-, UserId:$USER_ID, PageTitle:DevOps Training & Certification Course with Live Training - Edureka|

      
Scenario: Google analytics verification for click on pay full price action
    Given user navigates to edureka website
    And login with global credentials
    And click on search bar
    And set course name as "DevOps"
    And set currency as "inr"
    And click on course "DevOps Certification Training" from the search list
    And change the currency to "INR"
    And clicks on pay full price link
    And assert following analytics data
    |Event Category|Event Action|Event Label|
    |Enroll_Funnel_CLP|Clicked_On_Enroll_Batch_Table|Lead: Lead, CourseName: $COURSE, , Country:-, UserId:$USER_ID, PageTitle:DevOps Training \| DevOps Certification Course - Edureka|

Scenario: Google analytics verification for invalid email and phone number action of payment screen
    Given user navigates to edureka website
    And login with global credentials
    And click on search bar
    And set course name as "DevOps"
    And set currency as "inr"
    And click on course "DevOps Certification Training" from the search list
    And change the currency to "INR"
    And clicks on pay full price link
    And fills up payfull price form with below details
      | #Email             | PhoneNo    | 
      | rajukarm@tech.edureka.com1 | 98459845901 |
      And assert following analytics data
      |Event Category|Event Action|Event Label|
      |Enroll_Funnel_CLP|Enroll_Batch_Table_PopUp_Started_Typing|Lead: Lead, CourseName: DevOps, , Country:$COUNTRY, UserId:$USER_ID, PageTitle:DevOps Training \| DevOps Certification Course - Edureka|
      |Enroll_Funnel_CLP|Enroll_Batch_Table_PopUp_Error_Message|Lead: Lead, CourseName: DevOps, Email, , Country:$COUNTRY, UserId:$USER_ID, PageTitle:DevOps Training \| DevOps Certification Course - Edureka|
      |Enroll_Funnel_CLP|Enroll_Batch_Table_PopUp_Error_Message|Lead: Lead, CourseName: DevOps, Mobile, , Country:$COUNTRY, UserId:$USER_ID, PageTitle:DevOps Training \| DevOps Certification Course - Edureka| 


Scenario: Google analytics verification for successful form submission on pay full price link
    Given user navigates to edureka website
    And login with global credentials
    And click on search bar
    And set course name as "DevOps"
    And set currency as "inr"
    And click on course "DevOps Certification Training" from the search list
    And change the currency to "INR"
    And clicks on pay full price link
    And fills up payfull price form with below details
      | #Email             | PhoneNo    | 
      | rajukarm@tech.edureka.com | 9845984590 |
    And assert following analytics data
      |Event Category|Event Action|Event Label|
      |Enroll_Funnel_CLP|Enroll_Batch_Table_Successful_Submission|Lead: Lead, CourseName: DevOps, , Country:$COUNTRY, UserId:$USER_ID, PageTitle:DevOps Training \| DevOps Certification Course - Edureka|

Scenario: Google analytics verification for successfull batch selection on payment screen
    Given user navigates to edureka website
    And login with global credentials
    And click on search bar
    And set course name as "DevOps"
    And set currency as "inr"
    And click on course "DevOps Certification Training" from the search list
    And change the currency to "INR"
    And clicks on pay full price link
    And fills up payfull price form with below details
      | #Email             | PhoneNo    | 
      | rajukarm@tech.edureka.com | 9845984590 |
    And select the batch "06 Sep Fri - Sat Timings: 09:30 PM - 12:30 AM"
	And assert following analytics data
	|Event Category|Event Action|Event Label|
    |Checkout Flow|Step2_Batch_Selected|UserId:$USER_ID, ,page title:https://ecom.stage3.edureka.in/payment|
    
    
Scenario: Google analytics verification for click on proceed to payment
    Given user navigates to edureka website
    And login with global credentials
    And click on search bar
    And set course name as "DevOps"
    And set currency as "inr"
    And click on course "DevOps Certification Training" from the search list
    And change the currency to "INR"
    And clicks on pay full price link
    And fills up payfull price form with below details
      | #Email             | PhoneNo    | 
      | rajukarm@tech.edureka.com | 9845984590 |
	And assert following analytics data
	|Event Category|Event Action|Event Label|
    |Checkout Flow|Step2_Batch_Selected|UserId:$USER_ID, ,page title:https://ecom.stage3.edureka.in/payment|
	And click on proceed to payment
	And assert following analytics data
    |Event Category|Event Action|Event Label|
	|Checkout Flow|Step2_Clicked_Proceed_to_Payment|UserId:$USER_ID, ,page title:https://ecom.stage3.edureka.in/payment|
	
	
Scenario: Google analytics verification for click on pay securly
    Given user navigates to edureka website
    And login with global credentials
    And click on search bar
    And set course name as "DevOps"
    And set currency as "inr"
    And click on course "DevOps Certification Training" from the search list
    And change the currency to "INR"
    And clicks on pay full price link
    And fills up payfull price form with below details
      | #Email             | PhoneNo    | 
      | rajukarm@tech.edureka.com | 9845984590 |
    And click on proceed to payment
    And click on pay securely
    And assert following analytics data
      |Event Category|Event Action|Event Label|
	  |Checkout Flow|Step3_Clicked_Pay_Securely|Currency : $CURRENCY--UserId: $USER_ID---|
    
  
Scenario: Verify Google analytics for Enter home page event (Not Found)
    Given user navigates to edureka website
    And assert following analytics data
      |Event Category|Event Action|Event Label|
      |ViewCourse|PageLoad|Home|
     
Scenario: Verify Google analytics for click on signup event(Not Found)
    Given user navigates to edureka website
    When fills up signup form with below details
      | #Email              | PhoneNumber | code|
      | rajkumarc1003@edureka.tech.co | 9035001160  | 91|
    And assert following analytics data
      |Event Category|Event Action|Event Label|
      |ClickSignup|Signup|Home|
      
Scenario: Verify Google analytics for course landing page (Not Found)
    Given user navigates to edureka website
    And login with global credentials
    And click on search bar
    And click on course "DevOps Certification Training" from the search list
    And assert following analytics data
     |Event Category|Event Action|Event Label|
     |ViewCourse|PageLoad|DevOps Certification Training|