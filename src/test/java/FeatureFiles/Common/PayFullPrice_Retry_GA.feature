Feature: PayFullPrice Feature
 
Scenario: Verify user is able to purchase course using payfull price link on course page
    Given user navigates to edureka website
     When searches for course "DevOps Certification Training" and navigates to course landing page.
     And change the currency to "INR"
     And clicks on pay full price link
      And fills up payfull price form with below details
      | #Email             | PhoneNo    | code|
      | rajukarm@tech.edureka.in | 9845984590 |91|
      And extract the user id
      And extract the course id for slug "devops"
      And Assert following data in "user_events" table
      |select orderid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' |IS_NOT_NULL_OR_BLANK|
	  |select courseid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'  |$COURSE_ID|
	  |select gateway from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'|IS_BLANK|
	  |select token from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'|IS_BLANK|
	  And select the batch "06 Sep Fri - Sat Timings: 09:30 PM - 12:30 AM"
	  And assert following analytics data
	  |Event Category|Event Action|Event Label|
      |Checkout Flow|Step2_Batch_Selected|UserId:$USER_ID, ,page title:https://ecom.stage3.edureka.in/payment|
	  And click on proceed to payment
	  And assert following analytics data
      |Event Category|Event Action|Event Label|
	  |Checkout Flow|Step2_Clicked_Proceed_to_Payment|UserId:$USER_ID, ,page title:https://ecom.stage3.edureka.in/payment|
#     And extract discount from order summary page
      And click on pay securely
      And set course name as "DevOps"
      And set currency as "inr"
      And assert following analytics data
      |Event Category|Event Action|Event Label|
	  |Checkout Flow|Step3_Clicked_Pay_Securely|Currency : $CURRENCY--UserId: $USER_ID---|
      And select "netbanking" as payment method
      And select "ICICI Bank" as bank
      And make the payment through razorpay gateway
      Then verify confirmation message displayed to user
      And Assert following data in "user_events" table
      |select orderid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' |IS_NOT_NULL_OR_BLANK|
	  |select courseid from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'  |$COURSE_ID|
	  |select gateway from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'|Razorpay|
	  |select token from pre_orders where userid='$USER_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'|IS_NOT_NULL_OR_BLANK|
	  And extract course price information for currency "INR"
	  And extract course price in usd and inr
      And calculate service tax with percentage "18"
      And calculate final price including taxation
      And Assert following data in "post_orders" table
	  |Query|Expected Result|
	  |Select orderid from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME' |IS_NOT_NULL_OR_BLANK|
	  |Select priceinr from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'|$PRICE_INR|
	  |Select priceusd from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'|$PRICE_USD|
	  |Select gateway from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'|Razorpay|
	  |Select original_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'|$ORIGINAL_PRICE|
	  |Select discount_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'|$DISCOUNT|
	  |Select final_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'|$FINAL_PRICE|
	  |Select servicetax_value from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'|$SERVICETAX|
	  |Select invoice_no from post_orders where userid='$USER_ID' and courseid='$COURSE_ID' and created > '$TEST_CASE_START_TIME' and modified > '$TEST_CASE_START_TIME'|IS_NOT_NULL_OR_BLANK|
	  