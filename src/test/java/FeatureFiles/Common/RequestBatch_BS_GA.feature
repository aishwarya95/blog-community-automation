Feature: Request batch operations
 
  Scenario: Verify user able to request a batch for specific course
    Given user navigates to edureka website
     And click on search bar
#     And assert following analytics data
#      |Event Category|Event Action|Event Label|
# Testing Done     |Search_v2|Homepage Search_Landings|$EMAIL_ID;;Instructor-Led Online Training with 24X7 Lifetime Support \| Edureka|
     When searches for course "DevOps Certification Training" and navigates to course landing page.
#     And assert following analytics data
#      |Event Category|Event Action|Event Label|
# Testing Done     |ViewCourse|PageLoad|DevOps Certification Training|
#     And fills up request a batch form with below details
#      | Date | #Email             | PhoneNo    | 
#      | 15   | rajukarm@gmail.com1 | 98459845901 |
#     And assert following analytics data
#      |Event Category|Event Action|Event Label|
#      |CLP|First_Scroll|Lead: Lead, CourseName: DevOps, , Country:$COUNTRY, UserId:$USER_ID, PageTitle:DevOps Training \| DevOps Certification Course - Edureka|
#      |Inquiry_Funnel|Request_Batch_Popup_Started_Typing|Lead: Lead, CourseName: DevOps, , Country:$COUNTRY, UserId:$USER_ID, PageTitle:DevOps Training \| DevOps Certification Course - Edureka|
#      |Inquiry_Funnel|Clicked_On_Request_Batch|Lead: Lead, CourseName: DevOps, , Country:$COUNTRY, UserId:$USER_ID, PageTitle:DevOps Training \| DevOps Certification Course - Edureka|
# Testing Done      |Inquiry_Funnel|Request_Batch_Popup_Shown|Lead: Lead, CourseName: DevOps, , Country:$COUNTRY, UserId:$USER_ID, PageTitle:DevOps Training \| DevOps Certification Course - Edureka|
#     And fills up request a batch form with below details
#      | Date | #Email             | PhoneNo    | 
#      | 15   | rajukarm100@gmail.com | 9845984590 |
#      And assert following analytics data
#      |Event Category|Event Action|Event Label|
# Testing Done      |Inquiry_Funnel|Request_Batch_Popup_Successful_Submission|Lead: Lead, CourseName: DevOps, , Country:$COUNTRY, UserId:$USER_ID, PageTitle:DevOps Training \| DevOps Certification Course - Edureka|
      And clicks on pay full price link
#      And assert following analytics data
#      |Event Category|Event Action|Event Label|
#      |Enroll_Funnel_CLP|Clicked_On_Enroll_Batch_Table|Lead: Lead, CourseName: DevOps, , Country:$COUNTRY, UserId:$USER_ID, PageTitle:DevOps Training \| DevOps Certification Course - Edureka|
# Testing Done with NoLead      |Enroll_Funnel_CLP|Enroll_Batch_Table_PopUp_Shown|Lead: Lead, CourseName: DevOps, , Country:$COUNTRY, UserId:$USER_ID, PageTitle:DevOps Training \| DevOps Certification Course - Edureka|
#      And fills up payfull price form with below details
#      | #Email             | PhoneNo    | 
#      | rajukarm@gmail.com1 | 98459845901 |
#      And assert following analytics data
#      |Event Category|Event Action|Event Label|
#      |Enroll_Funnel_CLP|Enroll_Batch_Table_PopUp_Started_Typing|Lead: Lead, CourseName: DevOps, , Country:$COUNTRY, UserId:$USER_ID, PageTitle:DevOps Training \| DevOps Certification Course - Edureka|
#      |Enroll_Funnel_CLP|Enroll_Batch_Table_PopUp_Error_Message|Lead: Lead, CourseName: DevOps, Email, , Country:$COUNTRY, UserId:$USER_ID, PageTitle:DevOps Training \| DevOps Certification Course - Edureka|
#      |Enroll_Funnel_CLP|Enroll_Batch_Table_PopUp_Error_Message|Lead: Lead, CourseName: DevOps, Mobile, , Country:$COUNTRY, UserId:$USER_ID, PageTitle:DevOps Training \| DevOps Certification Course - Edureka|
      And fills up payfull price form with below details
      | #Email             | PhoneNo    | 
      | rajukarm@gmail.com | 9845984590 |
      And assert following analytics data
      |Event Category|Event Action|Event Label|
      |Enroll_Funnel_CLP|Enroll_Batch_Table_Successful_Submission|Lead: Lead, CourseName: DevOps, , Country:$COUNTRY, UserId:$USER_ID, PageTitle:DevOps Training \| DevOps Certification Course - Edureka|
      And click on proceed to payment
      
      And click on pay securely
            