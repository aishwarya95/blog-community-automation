Feature: SalesLead feature

Background:
	And update password of admin

  @Sanity
  Scenario Outline: Verify Sales lead form (TC_SL_UP_001) for <Course>
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Sales->Lead->Sales Lead" Page
    Then verify sales lead form for email
      | #Email                    | Course   |
      | saleslead@tech.edureka.in | <Course> |
    And extract the user id
    And extract the lead id
    And Assert following data in "user" table
      | Query                                                         | ExpectedResult       |
      | select first_name from users where email='$EMAIL_ID'          | IS_NOT_Null_or_Blank |
      | select last_name from users where email='$EMAIL_ID'           | IS_NOT_NULL_OR_Blank |
      #| select preferred_timezone from users where email='$EMAIL_ID'  | IST                  |
      #| select preffered_currency from users where email='$EMAIL_ID'  | INR                  |
      #| select preffered_country from users where email='$EMAIL_ID'   |                   12 |
      | select customer_status from users where email='$EMAIL_ID'     |                    1 |
      | select country from users where email='$EMAIL_ID'             | IN                   |
    And Assert following data in "user_leads" table
      | Query                                                        	     | ExpectedResult       |
      | select course_id from user_leads where user_id='$USER_ID'          | <Course_id>          |
      | select first_name from user_leads where user_id='$USER_ID'         | IS_NOT_NULL_OR_BLANK |
      | select last_name from user_leads where user_id='$USER_ID'          | IS_NOT_NULL_OR_BLANK |
      | select email from user_leads where user_id='$USER_ID'              | $EMAIL_ID            |
      | select phone from user_leads where user_id='$USER_ID'              |           1234567890 |
      | select website_action from user_leads where user_id='$USER_ID'     | Lead by Sales        |
      | select event_type from user_leads where user_id='$USER_ID'         | UP                   |
    | select zoholead_id from user_leads where user_id='$USER_ID'        | IS_NOT_NULL_OR_BLANK |
    | select zoho_potential_id from user_leads where user_id='$USER_ID'  | IS_NOT_NULL_OR_BLANK |
    And Assert following data in "user_events" table
      | Query                                                           | ExpectedResult |
      | select course_id from user_events where user_id='$USER_ID'      | <Course_id>    |
      | select event_context from user_events where user_id='$USER_ID'  | Lead by Sales  |
      | select event_type from user_events where user_id='$USER_ID'     | UP             |

    Examples: 
      | Course                                        | Course_id |
      | Advanced MS Excel                             |       546 |
      | Big Data and Hadoop                           |       888 |
      | Big Data Architect Masters Program            |       585 |
      | Post Graduate Program in Big Data Engineering |      1085 |
