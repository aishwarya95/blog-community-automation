Feature: AdminLead feature

Background:
	And update password of admin

@Sanity
Scenario Outline: Verify User is able to upload b2c bulk leads (TC_UP_001)
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Sales->Lead->Upload Bulk Lead" Page
    And Check Upload B2C lead functionality
    | #Email                    | Courseid    | Leadtype|
    | leadforms@tech.edureka.in | 776,520,14  | <Lead type>      |
    And extract the user id
    And extract the lead id
    And Assert following data in "user" table
      | Query                                                         | ExpectedResult       |
      | select first_name from users where email='$EMAIL_ID'          | IS_NOT_Null_or_Blank |
      | select last_name from users where email='$EMAIL_ID'           | IS_NOT_NULL_OR_Blank |
      #| select preferred_timezone from users where email='$EMAIL_ID'  | IST                  |
      #| select preffered_currency from users where email='$EMAIL_ID'  | INR                  |
      #| select preffered_country from users where email='$EMAIL_ID'   |                   12 |
      | select customer_status from users where email='$EMAIL_ID'     |                    1 |
      | select country from users where email='$EMAIL_ID'             | IN                   |
      | select first_utm_source from users where email='$EMAIL_ID'    | $UTM_SOURCE          |
      | select first_utm_campaign from users where email='$EMAIL_ID'  | $UTM_CAMPAIGN        |
      | select first_utm_medium from users where email='$EMAIL_ID'    | $UTM_MEDIUM          |
      | select created from users where email='$EMAIL_ID'             | IS_NOT_Null_or_Blank |
      | select modified from users where email='$EMAIL_ID'            | IS_NOT_Null_or_Blank |
    And Assert following data in "user_leads" table
      | Query                                                        	     | ExpectedResult       |
      | select course_id from user_leads where user_id='$USER_ID'          |                  $COURSE_ID |
      | select first_name from user_leads where user_id='$USER_ID'         | IS_NOT_NULL_OR_BLANK |
      | select last_name from user_leads where user_id='$USER_ID'          | IS_NOT_NULL_OR_BLANK |
      | select email from user_leads where user_id='$USER_ID'              | $EMAIL_ID            |
      | select phone from user_leads where user_id='$USER_ID'              | $PHONE_NO        |
      | select website_action from user_leads where user_id='$USER_ID'     | <EventContext>       |
      | select event_type from user_leads where user_id='$USER_ID'         | <EventType>          |
      | select zoholead_id from user_leads where user_id='$USER_ID'        | IS_NOT_NULL_OR_BLANK |
      | select zoho_potential_id from user_leads where user_id='$USER_ID'  | IS_NOT_NULL_OR_BLANK |
    And Assert following data in "user_events" table
      | Query                                                           | ExpectedResult         |
      | select course_id from user_events where user_id='$USER_ID'      |  $COURSE_ID            |
      | select event_context from user_events where user_id='$USER_ID'  | <EventContext> 				 |
      | select event_type from user_events where user_id='$USER_ID'     | <EventType>            |
      | select utm_source from user_events where user_id='$USER_ID' 		|	$UTM_SOURCE	  				 |
      | select utm_campaign from user_events where user_id='$USER_ID' 	|	$UTM_CAMPAIGN					 |
      | select utm_medium from user_events where user_id='$USER_ID' 		|	$UTM_MEDIUM					   |
    And Assert following data in "utm_params" table
      | Query                                                           | ExpectedResult         |
      | select campaign_source from utm_params where user_id='$USER_ID' 		|	$UTM_SOURCE	  				 |
      | select campaign_term from utm_params where user_id='$USER_ID' 			|	$UTM_TERM					 |
      | select campaign_medium from utm_params where user_id='$USER_ID' 		|	$UTM_MEDIUM					   |
      | select campaign_content from utm_params where user_id='$USER_ID' 		|	$UTM_CONTENT					   |
      | select campaign_name from utm_params where user_id='$USER_ID' 	  	|	$UTM_CAMPAIGN					   |

Examples: 
      | Lead type								 | EventContext           | EventType 	|
      | UP                       | Webinar								| UP          |
      | Webinar                  | Webinar								| Webinar     |
    
    
    