Feature: Admin Convert Lead To Potential feature

  Background: 
    And update password of admin

  @Sanity @ConvertToPotential
  Scenario Outline: Verify Convert Lead to Potential Form and Check User_leads table(TC_CP_001)
    Given user navigates to edureka admin portal
    And login to admin portal
    And Navigate to "Sales->Lead->Sales Lead" Page
    Then verify sales lead form for email
      | #Email                    | Course   |
      | saleslead@tech.edureka.in | <Course> |
    And extract the user id
    And extract the lead id
    And extract the course id for "<Course>" through "Analytics_title"
    And Update Lead ids and Potential ids
    And Navigate to "Sales->Lead->Convert To Potential" Page
    Then verify Convert lead to Potential form
     | Course                                        | AlertMessage   |
     | Post Graduate Program in Big Data Engineering | Success        |
    And Assert following data in "user" table
      | Query                                                         | ExpectedResult       |
      | select first_name from users where email='$EMAIL_ID'          | IS_NOT_Null_or_Blank |
      | select last_name from users where email='$EMAIL_ID'           | IS_NOT_NULL_OR_Blank |
      #| select preferred_timezone from users where email='$EMAIL_ID'  | IST                  |
      #| select preffered_currency from users where email='$EMAIL_ID'  | INR                  |
      #| select preffered_country from users where email='$EMAIL_ID'   |                   12 |
      | select customer_status from users where email='$EMAIL_ID'     |                    1 |
      | select country from users where email='$EMAIL_ID'             | IN                   |
      | select created from users where email='$EMAIL_ID'             | IS_NOT_Null_or_Blank |
      | select modified from users where email='$EMAIL_ID'            | IS_NOT_Null_or_Blank |
    And Assert following data in "user_leads" table
      | Query                                                        	     | ExpectedResult       |
      | select course_id from user_leads where user_id='$USER_ID'          | $COURSE_ID           |
      | select first_name from user_leads where user_id='$USER_ID'         | IS_NOT_NULL_OR_BLANK |
      | select last_name from user_leads where user_id='$USER_ID'          | IS_NOT_NULL_OR_BLANK |
      | select email from user_leads where user_id='$USER_ID'              | $EMAIL_ID            |
      | select phone from user_leads where user_id='$USER_ID'              | 1234567890           |
      | select zoholead_id from user_leads where user_id='$USER_ID'        | IS_NOT_NULL_OR_BLANK |
      | select zoho_potential_id from user_leads where user_id='$USER_ID'  | IS_NOT_NULL_OR_BLANK |
   

    Examples: 
      | Course                                        | EventContext |
      | Post Graduate Program in Big Data Engineering |         1085 |
