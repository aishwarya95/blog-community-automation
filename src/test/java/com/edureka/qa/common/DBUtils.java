package com.edureka.qa.common;

import com.edureka.qa.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import cucumber.api.DataTable;
import junit.framework.Assert;


public class DBUtils {
	
	ApplicationLibrary app;
	public static HashMap<String,String> DbutilsTestData = new HashMap<String,String>();

	
	public static Connection getMySqlConnection() throws Exception {		
		String url = "jdbc:mysql://" + Constants.db_server+ ":" + Constants.db_port + "/";
		String userName = Constants.db_username;
		String password = Constants.db_password;
		String dbName = Constants.db_name;
		Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
		return DriverManager.getConnection(
				url + dbName, userName, password);
	}
	
	public static void connectionCloseAll() throws Exception
	{  
					  Constants.db_connection.close();
					  Constants.db_statement.close();			
	}
	
	
public static ResultSet executeQuery(String Query) throws Exception
	{
		   ResultSet rs = null;	   
			   rs = (ResultSet) Constants.db_statement
						.executeQuery(Query);   
		   return rs;
	} 

/**public static String fetchQueryResult(String Query) throws Exception
{
	ResultSet rs = null;	   
	   rs = (ResultSet) Constants.db_statement
				.executeQuery(Query);   
	   if(rs!=null)
		 {
			 if(rs.next())
				 return rs.getString(1);
		 }
		 
		 return "-1";
 
} **/
	
	public static int getCourseId(String Slug) throws Exception
	{
		 String Query=Constants.course_id_query.replace("$SLUG",Slug);
		 ResultSet rs=Constants.db_statement
					.executeQuery(Query);   
		 if(rs!=null)
		 {
			 if(rs.next())
				 return rs.getInt(1);
		 }
		 
		 return -1;
	}
	
	public static int getUserId(String UserName,Date SourceDate) throws Exception
	{
		String Query=Constants.user_id_query.replace("$EMAIL_ID",UserName).replace("$TEST_CASE_START_TIME", DateTimeConversion.convertTimeToSpecificTimeZone(SourceDate, Constants.mysql_instance_timezone)).replace("$TEST_CASE_START_TIME", DateTimeConversion.convertTimeToSpecificTimeZone(SourceDate, Constants.mysql_instance_timezone));
		 ResultSet rs=Constants.db_statement.executeQuery(Query);   
		 if(rs!=null)
		 {
			 if(rs.next())
				 return rs.getInt(1);
		 }
		 return -1;
	}
	
	
	public static int getUserId(String UserName) throws Exception
	{
		String Query="Select id from users where email='$EMAIL_ID'".replace("$EMAIL_ID",UserName);
		 ResultSet rs=Constants.db_statement.executeQuery(Query);   
		 if(rs!=null)
		 {
			 if(rs.next())
				 return rs.getInt(1);
		 }
		 return -1;
	}
	
	
	public static String getActualResult(String Query) throws Exception {
	    ResultSet rs=(ResultSet) Constants.db_statement
				.executeQuery(Query);
	    if(rs!=null)
	    {
	    	if(rs.next())
	    	return rs.getString(1);
	    	else
	    	{
	    		throw new Exception ("No Records found for query "+Query+"\n");
	    	}
	    }
	    return "-1";
	}

	public static int getLeadId(String userid, Date SourceDate) throws SQLException, ParseException {
		String Query=Constants.lead_id_query.replace("$USER_ID",userid).replace("$TEST_CASE_START_TIME", DateTimeConversion.convertTimeToSpecificTimeZone(SourceDate, Constants.mysql_instance_timezone)).replace("$TEST_CASE_START_TIME", DateTimeConversion.convertTimeToSpecificTimeZone(SourceDate, Constants.mysql_instance_timezone));
		 ResultSet rs=Constants.db_statement.executeQuery(Query);   
		 if(rs!=null)
		 {
			 if(rs.next())
				 return rs.getInt(1);
		 }
		 return -1;
	}	
	
	/**
	 * @author sowmya
	 * @param datetime
	 * @return
	 * @throws Exception
	 */
	public static String setpassword() throws Exception
	{
		try {
			 String pwd = Constants.admin_pwdkey;
			 String email = Constants.admin_email;
			 String activateQuery="update users set password=\"$PWD\" where email=\"$EMAIL_ID\";".replace("$PWD", pwd).replace("$EMAIL_ID", email);
			 Constants.db_statement.executeUpdate(activateQuery);  
		
		}
		catch(Exception e)
		{
			Assert.fail();
		}
		 return null;
	}
	/**
	 * @author sowmya
	 * @param Currency
	 * @return
	 * @throws Exception
	 */
	public static float getCurrencyConvertion(String Currency) throws Exception
	{
		try {
			 String Query="select currency_rate from currency_rates where currency_name=\"$CURRENCY\";".replace("$CURRENCY", Currency);
			 ResultSet rs=Constants.db_statement.executeQuery(Query);   
			 if(rs!=null)
			 {
				 if(rs.next())
					 return rs.getFloat(1);
			 }
			 return -1;		}
		catch(Exception e)
		{
			Assert.fail();
		}
		 return 1;
	}
	/**
	 * @author sowmya
	 * @param userid
	 * @return
	 * @throws Exception
	 */
	public static int getEdurekacash(String userid) throws Exception
	{
		try {
			 String Query="select edureka_cash from ambassadors where user_id=\"$USER_ID\";".replace("$USER_ID",userid);
			 ResultSet rs=Constants.db_statement.executeQuery(Query);   
			 if(rs!=null)
			 {
				 if(rs.next())
					 return rs.getInt(1);
			 }
			 return -1;		}
		catch(Exception e)
		{
			Assert.fail();
		}
		 return 1;
	}
	/**
	 * @author sowmya
	 * @param value
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public static int getcoursepidfrom(String value,String type) throws Exception
	{
		try {
			String Query = null;
			switch(type)
			{
			case "Analytics_title":
				 Query="select id from courses where analytics_title=\"$ANALYTICS_TITLE\" and active=1 and visibility=1;".replace("$ANALYTICS_TITLE", value);
				 break;
				 
			case "Analytics_title and not visible":
				 Query="select id from courses where analytics_title=\"$ANALYTICS_TITLE\" and active=1 and visibility=0;".replace("$ANALYTICS_TITLE", value);
				 break;
				 
			case "Analytics_title and not visible SP":
				 Query="select id from courses where analytics_title=\"$ANALYTICS_TITLE\" and active=1 and visibility=0 and course_type=2;".replace("$ANALYTICS_TITLE", value);
				 break;
				 
			case "Display_title":
				 Query="select id from courses where display_title=\"$DISPLAY_TITLE\" and active=1 and visibility=1;".replace("$DISPLAY_TITLE", value);
				 break;
				 
	        default:
	        	System.out.println("invalid scenario");
			}
			 ResultSet rs=Constants.db_statement.executeQuery(Query);   
			 if(rs!=null)
			 {
				 if(rs.next())
					 return rs.getInt(1);
			 }
			 return -1;		}
		catch(Exception e)
		{
        	System.out.println(e);
			Assert.fail();
		}
		 return 1;
	}
	
	/**
	 * @author aishwarya
	 * @param courseid
	 * @throws Exception
	 */
	public static String inactivatePgpSlots(String course) throws Exception
	{
		try {
			 String activateQuery="update pgp_interview_slots set is_active=0 where course_id='$COURSE_ID';".replace("$COURSE_ID",course);
			 Constants.db_statement.executeUpdate(activateQuery);		
		}
		catch(Exception e)
		{
			Assert.fail();
		}
		 return null;
	}
	
	/**
	 * @author sowmya
	 * @param coursetype
	 * @return
	 * @throws Exception
	 */
	public static  String fetchsettingtable(String coursetype) throws Exception
	{
		try {
			 String Query=null;
			 switch(coursetype)
			 {
			 case "live":
				 Query="select value from settings where name =\"custom_link.max_discount_live\"";
				 break;
				 
			 case "master":
				 Query="select value from settings where name =\"custom_link.max_discount_masters\"";
				 break;
			
			 case "sp":
				 Query="select value from settings where name =\"custom_link.max_discount_self_paced\"";
				 break;
				 
			default:
				System.out.println("Invalid course type");
				
			 }
			 
			 ResultSet rs=Constants.db_statement.executeQuery(Query);  
			 if(rs!=null)
			 {
				 if(rs.next())
					 return rs.getString(1);
			 }		}
		catch(Exception e)
		{
			Assert.fail();
		}
		 return null;
	}
	/**
	 * @author sowmya
	 * @param tablename
	 * @param userid
	 * @param courseid
	 * @throws Exception
	 */
	public static void checkcourseformastercourses(String tablename, String userid, String courseid, String email)
			throws Exception {
		try {
			HashMap<String, String> PrerequisiteData = new HashMap<String, String>();
			PrerequisiteData.put("817", "1040");
			PrerequisiteData.put("861", "1041");
			PrerequisiteData.put("845", "1042");
			PrerequisiteData.put("905", "256");
			PrerequisiteData.put("914", "1057");
			PrerequisiteData.put("934", "1044");
			String Query = null;
			getCoursegroup(courseid);
			String coursegroup=DbutilsTestData.get("coursegroup");

			String MasterQuery = "select child_course_id from master_courses where course_id=\"" + courseid
					+ "\" and \r\n" + "group_visibility=1 order by display_order desc;";
			ArrayList<String> ExpectedCourses = new ArrayList<String>(13);
			ArrayList<String> ActualCourses = new ArrayList<String>(13);

			ResultSet rs = Constants.db_statement.executeQuery(MasterQuery);

			if (rs != null) {
				while (rs.next())
					ExpectedCourses.add(rs.getString(1));
			}
			ExpectedCourses.add(courseid);
			if(coursegroup.equalsIgnoreCase("817")||coursegroup.equalsIgnoreCase("861")||coursegroup.equalsIgnoreCase("845")||coursegroup.equalsIgnoreCase("905")||coursegroup.equalsIgnoreCase("914")||coursegroup.equalsIgnoreCase("934")) {
				ExpectedCourses.add(PrerequisiteData.get(coursegroup));
			}
			switch (tablename) {
			case "user_leads":
				Thread.sleep(TimeUnit.SECONDS.toMillis(100));
				Query = "select course_id from user_leads where email=\"$EMAIL_ID\" order by id asc;"
						.replace("$EMAIL_ID", email);
				break;

			case "user_courses":
				Query = "select course_id from user_courses where user_id=\"$USER_ID\" and is_paid=1 order by id desc;"
						.replace("$USER_ID", userid);
				break;
			}
			ResultSet userrs = Constants.db_statement.executeQuery(Query);

			for (int i = 0; i <= 6; i++) {
				if (userrs != null) {
					while (userrs.next()) {
						ActualCourses.add(userrs.getString(1));
					}
					break;
				} else {
					checkcourseformastercourses(tablename, userid, courseid, email);
				}
			}
			ApplicationLibrary.pass("Expected Result " + ExpectedCourses + "Actual Result " + ExpectedCourses);
			Assert.assertEquals(ExpectedCourses, ActualCourses);
		} catch (Exception e) {
			System.out.println(e);
			Assert.fail();
		}
	}
	/**
	 * @author sowmya
	 * @param email
	 * @return
	 */
	public static String getUrlParam(String email)
	{
		String urlparam = null;
		String discountvalidity=null;
		try
		{
			String Queryvalidity="select discount_validity from batch_discounts where user_email=\""+email+"\" order by id desc;";
    		ResultSet rs1=DBUtils.executeQuery(Queryvalidity);
    		if(rs1!=null)
    		{
    			if(rs1.next())
    			{
    				discountvalidity = rs1.getString(1);
    				DbutilsTestData.put("validity",discountvalidity);
    			}
    		}
			String Query="select url_parameter from batch_discounts where user_email=\""+email+"\" order by id desc;";
    		ResultSet rs=DBUtils.executeQuery(Query);
    		if(rs!=null)
    		{
    			if(rs.next())
    			{
    				urlparam = rs.getString(1);
    				return urlparam;
    			}
    		}
    		
    		
		}
		catch(Exception e)
		{
			System.out.println(e);
			Assert.fail();		
		}
		return urlparam;
	}
	
	/**
	 * @author sowmya
	 * @param courseid
	 * @throws Exception
	 */
	public static  void  getCoursegroup(String courseid) throws Exception
	{
		String result = null;
		try {
			 String Query="select course_group from courses where id="+courseid+" order by id desc;";
			 ResultSet rs=executeQuery(Query);
			 if(rs!=null)
	    		{
	    			if(rs.next())
	    			{
	    				result = rs.getString(1);
	    				DbutilsTestData.put("coursegroup",result);
	    			}
	    		}
		}
		catch(Exception e)
		{
			System.out.println(e);
			Assert.fail();
		}
	}
	/**
	 * @author sowmya
	 * @param email
	 * @param courseid
	 * @throws Exception
	 */
	public static  void  updateidstonull(String email,String courseid) throws Exception
	{
		String result = null;
		try {
			 PreparedStatement ps;
				String Queryupdate="update user_leads \r\n" + 
						"set zoholead_id = null ,\r\n" + 
						"zoho_potential_id = null \r\n" + 
						"where email=\""+email+"\" and \r\n" + 
						"course_id = "+courseid+" order by id desc;";
				ps=Constants.db_connection.prepareStatement(Queryupdate);
				ps.executeUpdate(Queryupdate);
		}
		catch(Exception e)
		{
			System.out.println(e);
			Assert.fail();
		}
	}
	/**
	 * @author sowmya
	 * @param courseid
	 * @return
	 * @throws Exception
	 */
	public static String  fetchanalyticsdisplaytitle(String courseid) throws Exception
	{
		String result = null;
		try {
			 PreparedStatement ps;
				String Query="select display_title from courses where id="+courseid+";";
				 ResultSet rs=executeQuery(Query);
				 if(rs!=null)
		    		{
		    			if(rs.next())
		    			{
		    				result = rs.getString(1);
		    				DbutilsTestData.put("displaytitle",result);
		    				return result;
		    			}
		    		}
		}
		catch(Exception e)
		{
			System.out.println(e);
			Assert.fail();
		}
		return result;
	}
}

