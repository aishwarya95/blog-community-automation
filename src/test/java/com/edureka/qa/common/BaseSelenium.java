package com.edureka.qa.common;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.fasterxml.jackson.databind.deser.SettableAnyProperty;

import net.bytebuddy.dynamic.TypeResolutionStrategy.Passive;
import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil; 

import com.edureka.qa.signup.*;

public class BaseSelenium  {
	
	public  ObjectRepositoryReader ObjectRepositoryReader;
	protected WebDriver driver;
	WebDriverWait wait;
	public HashMap<String,String> TestData = new HashMap<String,String>();
	public StringBuffer error_message=new StringBuffer();
	public BrowserMobProxy proxy;
	public Date TestCase_StartTime=new Date();
	
	protected enum LOCATOR_TYPE {
		ID, NAME, LINK, XPATH, JSSCRIPT, PARTIALLINK, CSS
	};
	
	public BaseSelenium() throws Exception
	{
		TestData.put("slugurl", "");
		TestData.put("email", "");
		TestData.put("userid", "");
		TestData.put("courseid", "");
		TestData.put("phoneno", "");
		TestData.put("leadid", "");
		TestData.put("currency", "");
		TestData.put("course", "");
		TestData.put("originalprice", "-1");
		TestData.put("priceusd", "-1");
		TestData.put("priceinr", "-1");
		TestData.put("discount", "0");
		TestData.put("servicetax", "-1");
		TestData.put("finalprice", "-1");
		TestData.put("code", "-1");
		TestData.put("EU", "-1");
		TestData.put("gclid", "");
		TestData.put("utm_source", "");
		TestData.put("utm_campaign", "");
		TestData.put("utm_medium", "");	
		TestData.put("utm_campaign","");
		TestData.put("utm_term","");
		TestData.put("utm_content","");
		TestData.put("educashvalue","");
		TestData.put("educashpoints","");
		TestData.put("urlparameter","");
		TestData.put("multiplepaymentvalue","");
		TestData.put("upfront","");
		TestData.put("url_param","");
		TestData.put("courseassignment","");
		TestData.put("mailchimp_shortCode","");
		TestData.put("short_name","");
		TestData.put("mailchimp_welcomeMail","");
		TestData.put("mailchimp_enrollMail","");
		TestData.put("sparkpost_welcomeMail","");
		TestData.put("sparkpost_enrollMail","");

    }
	
	static String workingDir = System.getProperty("user.dir");
	public static void  initialize() throws Exception
	{
		try
		{
		if(!Constants.is_initialized)
		{
			Constants.prop.load(new FileInputStream(new File(workingDir+"/Environment.properties")));
			String env= System.getProperty("envVariable");
			System.out.println("Test Environment:- "+ env);
			switch (env) {
			case "stage3":
				Constants.base_url=Constants.prop.getProperty("BaseURLstage3");
				Constants.db_name=Constants.prop.getProperty("DbNamestage3");
				break;
				
			case "stage2":
				Constants.base_url=Constants.prop.getProperty("BaseURLstage2");
				Constants.db_name=Constants.prop.getProperty("DbNamestage2");
				break;
			
			case "stage1":
				Constants.base_url=Constants.prop.getProperty("BaseURLstage1");
				Constants.db_name=Constants.prop.getProperty("DbNamestage1");
				break;
				
			case "stage":
				Constants.base_url=Constants.prop.getProperty("BaseURLstage");
				Constants.db_name=Constants.prop.getProperty("DbNamestage");
				break;
				
			case "uat":
				Constants.base_url=Constants.prop.getProperty("BaseURLuat");
				Constants.db_name=Constants.prop.getProperty("DbNameuat");
				break;
				
			case "a2":
				Constants.base_url=Constants.prop.getProperty("BaseURLa2");
				Constants.db_name=Constants.prop.getProperty("DbNamea2");
				break;
				
			default :
				Constants.base_url=Constants.prop.getProperty("BaseURLstage3");
				System.out.println(Constants.base_url);
				Constants.db_name=Constants.prop.getProperty("DbNamestage3");
				break;
				
			}
			Constants.db_server=Constants.prop.getProperty("DbServerName");
			Constants.db_username=Constants.prop.getProperty("DbUserName");
			Constants.db_password=Constants.prop.getProperty("DbPassword");
			Constants.db_port=Constants.prop.getProperty("DbPort");
			Constants.db_connection=DBUtils.getMySqlConnection();
			Constants.db_statement=Constants.db_connection.createStatement();
			Constants.prefix1=System.currentTimeMillis()+"";
			Random r = new Random();
			Constants.prefix2=(r.nextInt(10))+"";
            System.out.println("Prefix generated for this Run ="+Constants.prefix1);
            Constants.is_initialized=true;
			Runtime.getRuntime().addShutdownHook(new Thread() 
		    { 
		      public void run() 
		      { 
		    	  
		        System.out.println("Closing DB Connections!");
		        try {
					DBUtils.connectionCloseAll();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		      } 
		    }); 
		    System.out.println("Initialization Completed!"); 
		}
		}
		catch(Exception e)
		{
			System.out.println("Error during initialization, Hence Quittting");
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	public static void  initializeVariable() throws Exception
	{
		try
		{
		if(!Constants.is_initialized_variable)
		{
			Constants.prop.load(new FileInputStream(new File(workingDir+"/Automation.properties")));
			Constants.browser=Constants.prop.getProperty("Browser");
			Constants.firefox_driver_path=workingDir+"/"+Constants.prop.getProperty("FirefoxDriverPath");
			Constants.chrome_driver_path=workingDir+"/"+Constants.prop.getProperty("ChromeDriverPath");
			Constants.ie_driver_path=Constants.prop.getProperty("IEDriverPath");
			Constants.explicit_wait_time=Integer.parseInt(Constants.prop.getProperty("Explicit_wait_time"));
			Constants.implicit_wait_time=Integer.parseInt(Constants.prop.getProperty("Implicit_wait_time"));
			Constants.page_load_time=Integer.parseInt(Constants.prop.getProperty("Max_page_load_time"));
			Constants.allcoursespage_url=Constants.base_url+Constants.prop.getProperty("AllCourseURL");
			Constants.categorypage_url=Constants.base_url+Constants.prop.getProperty("CategoryPageURL");
			Constants.course_id_query=Constants.prop.getProperty("CourseIdQuery");
			Constants.user_id_query=Constants.prop.getProperty("UserIdQuery");
			Constants.lead_id_query=Constants.prop.getProperty("LeadIdQuery");
			Constants.course_price_query=Constants.prop.getProperty("CoursePriceQuery");
			Constants.price_usd_inr_query=Constants.prop.getProperty("PriceUSDINRQuery");
			Constants.ccenable_query=Constants.prop.getProperty("CCAvenueQueryEnable");
            Constants.ccdisable_query=Constants.prop.getProperty("CCAvenueQueryDisable");
            Constants.sql_safe_state=Constants.prop.getProperty("SQL_SAFE_STATEQuery");
            Constants.razorpay_default=Constants.prop.getProperty("RazorpayDefaultQuery");
            Constants.ccavenue_default=Constants.prop.getProperty("CCAvenueDefaultQuery");
			Constants.timestamp_threshold=Long.parseLong(Constants.prop.getProperty("TimeStampThreshold"));
			Constants.general_wait_time=Integer.parseInt(Constants.prop.getProperty("General_wait_time"));
			Constants.global_email_suffix=Constants.prop.getProperty("GlobalEmailSuffix");
			Constants.global_password=Constants.prop.getProperty("GlobalPassword");
			Constants.global_phonenumber=Constants.prop.getProperty("GlobalPhoneNumber");
			Constants.global_country_code=Constants.prop.getProperty("GlobalCountryCode");
			Constants.global_email=System.currentTimeMillis()+Constants.global_email_suffix;
            Constants.paypal_email=Constants.prop.getProperty("Paypal_email");
            Constants.paypal_pwd=Constants.prop.getProperty("Paypal_pwd");
            Constants.mysql_instance_timezone=Constants.prop.getProperty("Mysql_Instance_TimeZone");
            Constants.server_instance_timezone=Constants.prop.getProperty("Server_Instance_Timezone");
            Constants.proxy_server_request_capture_wait_time=Integer.parseInt(Constants.prop.getProperty("ProxyServerRequestCapturewaitTime"));
            Constants.gtmetrix_url=Constants.prop.getProperty("GTMetrix_URL");
            Constants.schema_url=Constants.prop.getProperty("Schema_URL");
            Constants.admin_email=Constants.prop.getProperty("Admin_Email");
            Constants.admin_pwd=Constants.prop.getProperty("Admin_Pwd");
            Constants.admin_pwdkey=Constants.prop.getProperty("Admin_Pwdkey");
            Constants.default_server=Constants.prop.getProperty("Default_Server");
            Constants.fullload_time_home=Constants.prop.getProperty("Fullloadtime_valueHOME");
            Constants.fullload_time_clp=Constants.prop.getProperty("Fullloadtime_valueCLP");
            Constants.fullload_time_mlp=Constants.prop.getProperty("Fullloadtime_valueMLP");
//            Constants.clp_url=Constants.prop.getProperty("CLPURL");
			Constants.course_type_query=Constants.prop.getProperty("CourseTypeQuery");
            System.setProperty(ChromeDriverService.CHROME_DRIVER_SILENT_OUTPUT_PROPERTY, "true");
            System.setProperty("webdriver.gecko.driver", Constants.firefox_driver_path);
            System.setProperty("webdriver.chrome.driver", Constants.chrome_driver_path);
            Constants.is_initialized_variable=true;
		}
		}
		catch(Exception e)
		{
			System.out.println("Error during initialization of Variables, Hence Quittting");
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	public void  OpenBrowser() throws Exception
	{
		try
		{
			proxy = getProxyServer();
			Proxy seleniumProxy = getSeleniumProxy(proxy);
			if(Constants.browser.toLowerCase().equals("firefox"))
			{
				FirefoxOptions options = new FirefoxOptions();
				options.setCapability(CapabilityType.PROXY, seleniumProxy);
				driver=new FirefoxDriver(options);
			}
			else if (Constants.browser.toLowerCase().equals("chrome"))
			{
				ChromeOptions options = new ChromeOptions();
 				options.setCapability(CapabilityType.PROXY, seleniumProxy);
 				options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
 				options.setCapability(ChromeOptions.CAPABILITY, true);
 				options.addArguments("--ignore-certificate-errors");
 				options.addArguments("--enable-automation");
 				options.addArguments("--window-size=1920x1080");
 				options.addArguments("--start-maximized");	
 				options.addArguments("--headless");
 				options.addArguments("--disable-gpu");
 				options.addArguments("--disable-infobars");
 				//options.addArguments("--remote-debugging-port=9222");
 				options.addArguments("--no-sandbox");
 				options.addArguments("--disable-setuid-sandbox");
 				options.addArguments("--disable-dev-shm-usage");
 				options.addArguments("--dns-prefetch-disable");
 				options.addArguments("--disable-infobars");
 				options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
 				if(TestData.get("EU").equalsIgnoreCase("true")) {
 				options.addExtensions(new File(workingDir+"/driver/extension_3_27_9_0.crx"));
 				}
 				options.setCapability("chrome-switches", Arrays.asList("--disable-extension", "--disable-logging", "--ignore-certificate-errors", "--log-level=0", "--silent", "--headless"));
 				options.setCapability("silent", true);
				driver=new ChromeDriver(options);
				//driver=new ChromeDriver();
				
			}
			else if (Constants.browser.toLowerCase().equals("ie"))
			{
				driver=new InternetExplorerDriver();
			}
			else
			{
				throw new Exception("Browser not supported");
			}
			driver.manage().timeouts().implicitlyWait(Constants.implicit_wait_time, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(Constants.explicit_wait_time, TimeUnit.SECONDS);	
			driver.manage().timeouts().implicitlyWait(Constants.page_load_time, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			proxy.newHar();
			driver.get(Constants.base_url);
			Thread.sleep(2000);
		}
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	public void click(String Locator, String LocatorType) throws Exception {
		//scrollIntoViewAndClick(Locator, LocatorType);
		switch (LOCATOR_TYPE.valueOf(LocatorType)) {
		case ID:
			driver.findElement(By.id(Locator)).click();
			break;

		case NAME:
			driver.findElement(By.name(Locator)).click();
			break;

		case XPATH:
			driver.findElement(By.xpath(Locator)).click();
			break;

		case CSS:
			driver.findElement(By.cssSelector(Locator)).click();
			break;

		case PARTIALLINK:
			driver.findElement(By.partialLinkText(Locator)).click();
			break;

		case LINK:
			driver.findElement(By.linkText(Locator)).click();
			break;
			

		default:
			throw new Exception(
					"Invalid Locator Type/Locator Type may not be supported");
		}
}

	
	public void type(String Locator, String LocatorType, String Value)
			throws Exception {
		switch (LOCATOR_TYPE.valueOf(LocatorType)) {
		case ID:
			driver.findElement(By.id(Locator)).sendKeys(Value);
			break;

		case NAME:
			driver.findElement(By.name(Locator)).sendKeys(Value);
			break;

		case XPATH:
			driver.findElement(By.xpath(Locator)).sendKeys(Value);
			break;

		case CSS:
			driver.findElement(By.cssSelector(Locator))
					.sendKeys(Value);
			break;

		default:
			throw new Exception(
					"Invalid Locator Type/Locator Type may not be supported");
		}
	}
	

	public void handleRadioButton(String Value, String Locator,
			String LocatorType, String action) throws Exception {
		switch (LOCATOR_TYPE.valueOf(LocatorType)) {
		case ID:
			if (action.equalsIgnoreCase("check")) {
				if (driver.findElement(By.id(Locator)).isSelected())
					System.out.println(Value + " is already selected");
				else {
					System.out.println(Value
							+ " is not selected , selecting now");
					driver.findElement(By.id(Locator)).click();
				}
			} else {
				if (driver.findElement(By.id(Locator)).isSelected()) {
					System.out.println(Value
							+ " is  selected , unselecting now");
					driver.findElement(By.id(Locator)).click();
				} else
					System.out.println(Value + " is already unselcted ");
			}
			break;

		case NAME:
			if (action.equalsIgnoreCase("check")) {
				if (driver.findElement(By.name(Locator)).isSelected())
					System.out.println(Value + " is already selected");
				else {
					System.out.println(Value
							+ " is not selected , selecting now");
					driver.findElement(By.name(Locator)).click();
				}
			} else {
				if (driver.findElement(By.name(Locator)).isSelected()) {
					System.out.println(Value
							+ " is  selected , unselecting now");
					driver.findElement(By.name(Locator)).click();
				} else
					System.out.println(Value + " is already unselcted ");
			}
			break;

		case XPATH:
			if (action.equalsIgnoreCase("check")) {
				if (driver.findElement(By.xpath(Locator)).isSelected())
					System.out.println(Value + " is already selected");
				else {
					System.out.println(Value
							+ " is not selected , selecting now");
					driver.findElement(By.xpath(Locator)).click();
				}
			} else {
				if (driver.findElement(By.xpath(Locator)).isSelected()) {
					System.out.println(Value
							+ " is  selected , unselecting now");
					driver.findElement(By.xpath(Locator)).click();
				} else
					System.out.println(Value + " is already unselcted ");
			}
			break;

		case CSS:
			if (action.equalsIgnoreCase("check")) {
				if (driver.findElement(By.cssSelector(Locator))
						.isSelected())
					System.out.println(Value + " is already selected");
				else {
					System.out.println(Value
							+ " is not selected , selecting now");
					driver.findElement(By.cssSelector(Locator))
							.click();
				}
			} else {
				if (driver.findElement(By.cssSelector(Locator))
						.isSelected()) {
					System.out.println(Value
							+ " is  selected , unselecting now");
					driver.findElement(By.cssSelector(Locator))
							.click();
				} else
					System.out.println(Value + " is already unselcted ");
			}
			break;

		default:
			throw new Exception(
					"Invalid Locator Type/Locator Type may not be supported");

		}
	}

	// Handle Check Box is used to check or uncheck the check box element

	public void handleCheckBox(String Value, String Locator,
			String LocatorType, String action) throws Exception {
		switch (LOCATOR_TYPE.valueOf(LocatorType)) {

		case ID:
			if (action.equalsIgnoreCase("check")) {
				if (driver.findElement(By.id(Locator)).isSelected())
					System.out.println(Value + " is already selected");
				else {
					System.out.println(Value
							+ " is not selected , selecting now");
					driver.findElement(By.id(Locator)).click();
				}
			} else {
				if (driver.findElement(By.id(Locator)).isSelected()) {
					System.out.println(Value
							+ " is  selected , unselecting now");
					driver.findElement(By.id(Locator)).click();
				} else
					System.out.println(Value + " is already unselcted ");
			}
			break;

		case NAME:
			if (action.equalsIgnoreCase("check")) {
				if (driver.findElement(By.name(Locator)).isSelected())
					System.out.println(Value + " is already selected");
				else {
					System.out.println(Value
							+ " is not selected , selecting now");
					driver.findElement(By.name(Locator)).click();
				}
			} else {
				if (driver.findElement(By.name(Locator)).isSelected()) {
					System.out.println(Value
							+ " is  selected , unselecting now");
					driver.findElement(By.name(Locator)).click();
				} else
					System.out.println(Value + " is already unselcted ");
			}
			break;

		case XPATH:
			if (action.equalsIgnoreCase("check")) {
				if (driver.findElement(By.xpath(Locator)).isSelected())
					System.out.println(Value + " is already selected");
				else {
					System.out.println(Value
							+ " is not selected , selecting now");
					driver.findElement(By.xpath(Locator)).click();
				}
			} else {
				if (driver.findElement(By.xpath(Locator)).isSelected()) {
					System.out.println(Value
							+ " is  selected , unselecting now");
					driver.findElement(By.xpath(Locator)).click();
				} else
					System.out.println(Value + " is already unselcted ");
			}
			break;

		case CSS:
			if (action.equalsIgnoreCase("check")) {
				if (driver.findElement(By.cssSelector(Locator))
						.isSelected())
					System.out.println(Value + " is already selected");
				else {
					System.out.println(Value
							+ " is not selected , selecting now");
					driver.findElement(By.cssSelector(Locator))
							.click();
				}
			} else {
				if (driver.findElement(By.cssSelector(Locator))
						.isSelected()) {
					System.out.println(Value
							+ " is  selected , unselecting now");
					driver.findElement(By.cssSelector(Locator))
							.click();
				} else
					System.out.println(Value + " is already unselcted ");
			}
			break;

		default:
			throw new Exception(
					"Invalid Locator Type/Locator Type may not be supported");
		}
	}

	// clears off the Existing Text if present on the item identified by Object
	// Locator

	public void clearDefaultText(String Locator, String LocatorType)
			throws Exception {
		switch (LOCATOR_TYPE.valueOf(LocatorType)) {
		case ID:
			driver.findElement(By.id(Locator)).clear();
			break;
		case NAME:
			driver.findElement(By.name(Locator)).clear();
			break;
		case XPATH:
			driver.findElement(By.xpath(Locator)).clear();
			break;
		case CSS:
			driver.findElement(By.cssSelector(Locator)).clear();
			break;
		default:
			throw new Exception(
					"Invalid Locator Type/Locator Type may not be supported");
		}
	}
	
	
	//Checks if an element exists on the page identified by object locator
	public boolean checkElementExist(String Locator, String LocatorType)
			throws Exception {
		boolean ElementExists = false;
		switch (LOCATOR_TYPE.valueOf(LocatorType)) {
		case ID:
			ElementExists = driver.findElements(By.id(Locator)).size() != 0;
			break;

		case NAME:
			ElementExists = driver.findElements(By.name(Locator))
					.size() != 0;
			break;

		case XPATH:
			ElementExists = driver.findElements(By.xpath(Locator))
					.size() != 0;
			break;

		case CSS:
			ElementExists = driver.findElements(
					By.cssSelector(Locator)).size() != 0;
			break;
		default:
			throw new Exception(
					"Invalid Locator Type/Locator Type may not be supported");
		}
	return ElementExists;	
}
	
	//Checks if an element is displayed on the page identified by object locator
	public boolean checkElementDisplayed(String Locator, String LocatorType)
	throws Exception {		
		boolean ElementDisplayed = false;

		switch (LOCATOR_TYPE.valueOf(LocatorType)) {
		case ID:
			ElementDisplayed = driver.findElement(By.id(Locator)).isDisplayed();
			break;

		case NAME:
			ElementDisplayed = driver.findElement(By.name(Locator)).isDisplayed();
			break;

		case XPATH:
			ElementDisplayed = driver.findElement(By.xpath(Locator)).isDisplayed();
			break;

		case CSS:
			ElementDisplayed = driver.findElement(By.cssSelector(Locator)).isDisplayed();
			break;
		default:
			throw new Exception(
					"Invalid Locator Type/Locator Type may not be supported");
		}
		return ElementDisplayed;
	}

	public void selectList(String Locator, String LocatorType, String Value)
			throws Exception {
		Select menu = null;
		switch (LOCATOR_TYPE.valueOf(LocatorType)) {
		case ID:
			menu = new Select(driver.findElement(By.id(Locator)));
			menu.selectByVisibleText(Value);
			break;

		case XPATH:
			menu = new Select(driver.findElement(By.xpath(Locator)));
			menu.selectByVisibleText(Value);
			break;

		case NAME:
			menu = new Select(driver.findElement(By.name(Locator)));
			menu.selectByVisibleText(Value);
			break;

		default:
			throw new Exception(
					"Invalid Locator Type/Locator Type may not be supported");
		}
	}

	public void multiSelect(String Locator, String LocatorType, String[] values)
			throws Exception {
		switch (LOCATOR_TYPE.valueOf(LocatorType)) {
		case ID:
			Select menu = new Select(driver
					.findElement(By.id(Locator)));
			for (int i = 0; i < values.length; i++) {
				menu.selectByVisibleText(values[i]);
			}
			break;

		case XPATH:
			System.out.println("Not Implement yet");
			break;

		default:
			throw new Exception(
					"Invalid Locator Type/Locator Type may not be supported");
		}
	}

	public String getText(String Locator, String LocatorType) throws Exception {

		String TextMessage = null;
		switch (LOCATOR_TYPE.valueOf(LocatorType)) {

		case ID:
			TextMessage = driver.findElement(By.id(Locator)).getText()
					.toString();
			break;

		case NAME:
			TextMessage = driver.findElement(By.name(Locator))
					.getText().toString();
			break;

		case XPATH:
			TextMessage = driver.findElement(By.xpath(Locator))
					.getText().toString();
			break;

		case CSS:
			TextMessage = driver.findElement(By.cssSelector(Locator))
					.getText().toString();
			break;

		default:
			throw new Exception(
					"Invalid Locator Type/Locator Type may not be supported");
		}
		return TextMessage.trim();
	}
//checks if a given attribute of an element is present or not
	public boolean checkAttributeExists(String Locator, String LocatorType,String AttrType,String AttrValue) throws Exception {

		boolean Attribute = false;
		switch (LOCATOR_TYPE.valueOf(LocatorType)) {

		case ID:
			Attribute = driver.findElement(By.id(Locator)).getAttribute(AttrType).contains(AttrValue);
			break;

		case NAME:
			Attribute = driver.findElement(By.name(Locator)).getAttribute(AttrType).contains(AttrValue);
			break;

		case XPATH:
			Attribute = driver.findElement(By.xpath(Locator)).getAttribute(AttrType).contains(AttrValue);
			break;

		case CSS:
			Attribute = driver.findElement(By.cssSelector(Locator)).getAttribute(AttrType).contains(AttrValue);
			break;

		default:
			throw new Exception(
					"Invalid Locator Type/Locator Type may not be supported");
		}
		return Attribute;
	}
	
	public void waitUntilElementClickable(String Locator,String LocatorType,int WaitTime) throws Exception
	{
		wait = new WebDriverWait(driver, WaitTime);
		switch (LOCATOR_TYPE.valueOf(LocatorType)) {

		case ID:
			   wait.until(ExpectedConditions.elementToBeClickable(By.id(Locator)));
			break;

		case NAME:
			wait.until(ExpectedConditions.elementToBeClickable(By.name(Locator)));
			break;

		case XPATH:
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Locator)));
			break;

		case CSS:
			wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(Locator)));
			break;

		default:
			throw new Exception(
					"Invalid Locator Type/Locator Type may not be supported");
		}
	}
	
	
	
	
	public void selectByVisibleText(String Locator,String LocatorType,String Text,String SelectionOption) throws Exception
	{
		Select dropdown;
		switch (LOCATOR_TYPE.valueOf(LocatorType)) {
		case ID:
			dropdown = new Select(driver.findElement(By.id(Locator)));
			break;

		case NAME:
			dropdown = new Select(driver.findElement(By.name(Locator)));
			break;

		case XPATH:
			dropdown = new Select(driver.findElement(By.xpath(Locator)));
			break;

		case CSS:
			dropdown = new Select(driver.findElement(By.cssSelector(Locator)));
			break;

		case PARTIALLINK:
			dropdown = new Select(driver.findElement(By.partialLinkText(Locator)));
			break;

		case LINK:
			dropdown = new Select(driver.findElement(By.linkText(Locator)));
			break;
		default:
			throw new Exception(
					"Invalid Locator Type/Locator Type may not be supported");
		}
		
		if(SelectionOption.toLowerCase().equals("visibletext"))
		  dropdown.selectByVisibleText(Text);
		else if (SelectionOption.toLowerCase().equals("value"))
			dropdown.selectByValue(Text);
		else if (SelectionOption.toLowerCase().equals("index"))
			dropdown.selectByIndex(Integer.parseInt(Text));
	}
	
	public void refresh() throws Exception
	{
		driver.navigate().refresh();
	}
	
	
	public void waitUntilElementVisible(String Locator,String LocatorType,int WaitTime) throws Exception
	{
		wait = new WebDriverWait(driver, WaitTime);
		switch (LOCATOR_TYPE.valueOf(LocatorType)) {

		case ID:
			   wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(Locator)));
			break;

		case NAME:
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(Locator)));
			break;

		case XPATH:
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Locator)));
			break;

		case CSS:
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(Locator)));
			break;

		default:
			throw new Exception(
					"Invalid Locator Type/Locator Type may not be supported");
		}
	}

	
	
	public void waitUntilElementPresent(String Locator,String LocatorType,int WaitTime) throws CustomException
	{
		wait = new WebDriverWait(driver, WaitTime);
		try
		{
		switch (LOCATOR_TYPE.valueOf(LocatorType)) {

		case ID:
			   wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id(Locator)));
			break;

		case NAME:
			wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.name(Locator)));
			break;

		case XPATH:
			wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(Locator)));
			break;

		case CSS:
			wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector(Locator)));
			break;

		default:
			throw new Exception(
					"Invalid Locator Type/Locator Type may not be supported");
		}
		}
		catch(Exception e)
		{
			throw new CustomException(driver, e.getMessage(),e);
		}
	}

	//Returns an attribute 
	public String getAttribute(String Locator, String LocatorType,String AttrType) throws CustomException {

		String Attribute = null;

		try
		{
		switch (LOCATOR_TYPE.valueOf(LocatorType)) {

		case ID:
			Attribute = driver.findElement(By.id(Locator)).getAttribute(AttrType);
			break;

		case NAME:
			Attribute = driver.findElement(By.name(Locator)).getAttribute(AttrType);
			break;

		case XPATH:
			Attribute = driver.findElement(By.xpath(Locator)).getAttribute(AttrType);
			break;

		case CSS:
			Attribute = driver.findElement(By.cssSelector(Locator)).getAttribute(AttrType);
			break;

		default:
			throw new Exception(
					"Invalid Locator Type/Locator Type may not be supported");
		}
		}
		catch(Exception e)
		{
			throw new CustomException(driver, e.getMessage(),e);
		}
		return Attribute;
	}
	//Performs Mouse Over Function 
	public void mouseOver(String Locator, String LocatorType) throws Exception {
		Actions actions = new Actions(driver);
		switch (LOCATOR_TYPE.valueOf(LocatorType)) {
		case ID:
			actions.moveToElement(driver.findElement(By.id(Locator)))
					.build().perform();
			break;

		case NAME:
			actions
					.moveToElement(
							driver.findElement(By.name(Locator)))
					.build().perform();
			break;

		case XPATH:
			actions.moveToElement(
					driver.findElement(By.xpath(Locator))).build()
					.perform();
			break;

		case CSS:
			actions.moveToElement(
					driver.findElement(By.cssSelector(Locator)))
					.build().perform();
			break;

		default:
			throw new Exception(
					"Invalid Locator Type/Locator Type may not be supported");
		}
	}
	
	// Verify Alert Message
	public void verifyAlert(String verifyTextMessage, String action)
			throws Exception {
			WebDriverWait wait = new WebDriverWait(driver,
					Constants.explicit_wait_time);
			if (wait.until(ExpectedConditions.alertIsPresent()) == null) {
				System.out.println("alert was not present");
				throw new Exception("Alert not Found Exception");
			} else {
				System.out.println("alert was present");
				Alert alert = driver.switchTo().alert();
				String AlertMessage = alert.getText();
				if (action.equalsIgnoreCase("accept"))
					alert.accept();
				else if (action.equalsIgnoreCase("cancel"))
					alert.dismiss();
				if (!AlertMessage.equals(verifyTextMessage)) {
					throw new Exception(
							"Alert Message is not same as Verify Text Message Expected = "
									+ verifyTextMessage + ", Actual = "
									+ AlertMessage);
				}
			}
	}
	
	//Returns a WebElement
	public WebElement getElement(String Locator, String LocatorType)throws Exception
	{
	
		WebElement element = null;
		switch (LOCATOR_TYPE.valueOf(LocatorType)) {
		case ID:
			element = driver.findElement(By.id(Locator));
			break;

		case NAME:
			element = driver.findElement(By.name(Locator));
			break;

		case XPATH:
			element = driver.findElement(By.xpath(Locator));
			break;

		case CSS:
			element = driver.findElement(By.cssSelector(Locator));
			break;

		default:
			throw new Exception(
					"Invalid Locator Type/Locator Type may not be supported");

		}
		return element;
	}
	
	
public List<WebElement> getWebElements (String Locator, String LocatorType) throws Exception {
	//List<String> SearchOutPut=new ArrayList<String>();	
	List<WebElement> WebObjects;
		switch (LOCATOR_TYPE.valueOf(LocatorType)) {
		case ID:
			WebObjects=driver.findElements(By.id(Locator));
			break;

		case NAME:
			WebObjects=driver.findElements(By.name(Locator));
			break;

		case XPATH:
			WebObjects=driver.findElements(By.xpath(Locator));
			break;

		case CSS:
			WebObjects=driver.findElements(By.cssSelector(Locator));
			break;

		case PARTIALLINK:
			WebObjects=driver.findElements(By.partialLinkText(Locator));
			break;

		case LINK:
			WebObjects=driver.findElements(By.linkText(Locator));
			break;
			
		default:
			throw new Exception(
					"Invalid Locator Type/Locator Type may not be supported");
		}
		return WebObjects;
	}


public List<String> getTextFromWebElements(List<WebElement> WebObjects ) throws Exception
{
	List<String> output=new ArrayList<String>();
	for(int i=0;i<WebObjects.size();i++)
	{
		output.add(i,WebObjects.get(i).getText());
	}
	return output;
	
}


public void scrollIntoViewAndClick(String Locator, String LocatorType) throws Exception
{
	WebElement WebObject;
		switch (LOCATOR_TYPE.valueOf(LocatorType)) {
		case ID:
			WebObject=driver.findElement(By.id(Locator));
			break;

		case NAME:
			WebObject=driver.findElement(By.name(Locator));
			break;

		case XPATH:
			WebObject=driver.findElement(By.xpath(Locator));
			break;

		case CSS:
			WebObject=driver.findElement(By.cssSelector(Locator));
			break;

		case PARTIALLINK:
			WebObject=driver.findElement(By.partialLinkText(Locator));
			break;

		case LINK:
			WebObject=driver.findElement(By.linkText(Locator));
			break;
			
		default:
			throw new Exception(
					"Invalid Locator Type/Locator Type may not be supported");
		}
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", WebObject);
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", WebObject);
}



public void scrollIntoView(String Locator, String LocatorType) throws Exception
{
	WebElement WebObject;
		switch (LOCATOR_TYPE.valueOf(LocatorType)) {
		case ID:
			WebObject=driver.findElement(By.id(Locator));
			break;

		case NAME:
			WebObject=driver.findElement(By.name(Locator));
			break;

		case XPATH:
			WebObject=driver.findElement(By.xpath(Locator));
			break;

		case CSS:
			WebObject=driver.findElement(By.cssSelector(Locator));
			break;

		case PARTIALLINK:
			WebObject=driver.findElement(By.partialLinkText(Locator));
			break;

		case LINK:
			WebObject=driver.findElement(By.linkText(Locator));
			break;
			
		default:
			throw new Exception(
					"Invalid Locator Type/Locator Type may not be supported");
		}
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", WebObject);
}




public BrowserMobProxy getProxyServer() 
	 {
		BrowserMobProxy proxy = new BrowserMobProxyServer();
		proxy.setTrustAllServers(true);
		proxy.start();
		return proxy;
	 }
	
	public Proxy getSeleniumProxy(BrowserMobProxy proxyServer) 
	{
		Proxy seleniumProxy = ClientUtil.createSeleniumProxy(proxyServer);
		try
		{
		System.setProperty("jsse.enableSNIExtension", "false");
		String hostIp = Inet4Address.getLocalHost().getHostAddress();
		seleniumProxy.setHttpProxy(hostIp + ":" + proxyServer.getPort());
		seleniumProxy.setSslProxy(hostIp + ":" + proxyServer.getPort());
		}
		catch (UnknownHostException e)
		{
		e.printStackTrace();
		Assert.fail("invalid Host Address");
		}
		return seleniumProxy;
	}
	
	public void stopProxyServer() throws Exception
	{
		proxy.stop();
	}
	
	public void switchToIFrame(String type,String frame,WebElement obj) throws Exception
	{
		if(type.equals("id")||type.equals("name"))
		{
			driver.switchTo().frame(frame);
		}
		else if(type.equals("webelement"))
		{
			driver.switchTo().frame(obj);
		}
		else
		{
			throw new Exception("Invalid frame type");
		}
	}
	
	public void switchToParentFrame() throws Exception
	{
	  driver.switchTo().parentFrame();	
	}
	
	public void switchToDefaultContent() throws Exception
	{
		driver.switchTo().defaultContent();
	}
	
	public WebElement getWebElement(String Locator, String LocatorType) throws Exception {
		WebElement element;
		switch (LOCATOR_TYPE.valueOf(LocatorType)) {
		case ID:
			return driver.findElement(By.id(Locator));

		case NAME:
			return driver.findElement(By.name(Locator));

		case XPATH:
			return driver.findElement(By.xpath(Locator));

		case CSS:
			return driver.findElement(By.cssSelector(Locator));
			

		case PARTIALLINK:
			return driver.findElement(By.partialLinkText(Locator));

		case LINK:
			return driver.findElement(By.linkText(Locator));
			
		default:
			throw new Exception(
					"Invalid Locator Type/Locator Type may not be supported");
		}
	}
	public String getCurrentURL() throws Exception
	{
	  return driver.getCurrentUrl();
	}
	
	/***
	 * @author rajeev
	 * @param url
	 */
	public void loadUrl(String url) {
		driver.navigate().to(url);
	}
	
	/***
	 * @author rajeev
	 */
	public void scrollToEnd() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}
	
	/***
	 * @author rajeev
	 */
	public void scrollbyPixel(String pixel) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,"+pixel+")");
	}
	
	public void newTab() {
		ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
	    driver.switchTo().window(tabs2.get(1));
	}
	/**
	 * @author rajeev
	 * @param originalHandle
	 */
	public void previousTab(String originalHandle) {
		driver.close();
		driver.switchTo().window(originalHandle);
	}
	
	/**
	 * @author rajeev
	 * @param element
	 */
	public void mouseOver(WebElement element) {
		Actions actions = new Actions(driver);
		actions.moveToElement(element).perform();
	}
	
	/**
	 * @author rajeev
	 * @param waitSecond
	 */
	public static void waitForSeconds(double waitSecond) {
		waitSecond = waitSecond * 1000;
		Calendar currentTime = Calendar.getInstance();
		long currentTimeMillis = currentTime.getTimeInMillis();
		long secCounter = 0;
		while( secCounter < waitSecond) {
			Calendar newTime = Calendar.getInstance();
			secCounter = (newTime.getTimeInMillis()) - (currentTimeMillis);
		}
	}
	
	/**
	 * @author sowmya
	 * @param url
	 * @return
	 */
	public String getWindowHandlesthroughURL(String url)
	{
		try {
			for (String windowHandle : driver.getWindowHandles()) {
				driver.switchTo().window(windowHandle);
				if(getCurrentURL().contains(url))
				{
					return windowHandle;
				}
				
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return null;
	}
	
	/**
	 * @author sowmya
	 * @return
	 */
	public boolean isAlertPresent() 
	{ 
	    try 
	    { 
	        driver.switchTo().alert(); 
	        return true; 
	    }   // try 
	    catch (NoAlertPresentException Ex) 
	    { 
	        return false; 
	    }   // catch 
	}   // isAlertPresent()
	/**
	 * @author sowmya
	 */
	public void scrollToUp() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(document.body.scrollHeight, 0)");
	}

}
