package com.edureka.qa.common;

import com.edureka.qa.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.commons.codec.binary.Base64;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.assertthat.selenium_shutterbug.core.Shutterbug;
import com.assertthat.selenium_shutterbug.utils.web.ScrollStrategy;
import com.mysql.cj.jdbc.ha.ReplicationMySQLConnection;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import io.qameta.allure.Attachment;

public class ApplicationLibrary extends BaseSelenium {

	public ObjectRepositoryReader obj;
	public String ObjectArray[];

	public ApplicationLibrary() throws Exception {
		super();
		ObjectRepositoryReader obj = new ObjectRepositoryReader();
		obj.readWebObjects("RequestBatch");
		this.obj = obj;

	}

	public void getScenario(cucumber.api.Scenario scenarios) {
		String presentscenario = scenarios.getId().split(";")[0];
		System.out.println(presentscenario);
		ObjectRepositoryReader obj = new ObjectRepositoryReader();
		try {
			switch (presentscenario) {
			case "userregistration-form-feature":
			case "adminlead-feature":
				obj.readWebObjects("AdminLeads");
				this.obj = obj;
				break;

			case "request-batch-operations":
				obj.readWebObjects("RequestBatch");
				this.obj = obj;
				break;

			case "signup-scenario":
				obj.readWebObjects("Signup");
				this.obj = obj;
				break;

			case "drop-us-a-query-operation-for-clp-and-mlp(login-and-logout)":
				obj.readWebObjects("DropUsAQuery");
				this.obj = obj;
				break;

			case "signup-scenarios":
				obj.readWebObjects("DropUsAQuery");
				this.obj = obj;
				break;

			case "admin-convert-lead-to-potential-feature":
			case "saleslead-feature":
				obj.readWebObjects("SalesLead");
				this.obj = obj;
				break;

			case "referral-credits-feature":
				obj.readWebObjects("Referral_Credits");
				this.obj = obj;
				break;

			case "customlink-feature":
				obj.readWebObjects("Custom_link");
				this.obj = obj;
				break;

			case "salesalert-form-feature":
				obj.readWebObjects("Sales_Alert");
				this.obj = obj;
				break;

			case "courseassignment-feature":
				obj.readWebObjects("CourseAssignment");
				this.obj = obj;
				break;

			case "custom-discount-feature":
				obj.readWebObjects("Custom_discount");
				this.obj = obj;
				break;

			case "pgppayments-feature":
				obj.readWebObjects("PgpPayments");
				this.obj = obj;
				break;
				
			case "modifysalesdata-form-feature":
				obj.readWebObjects("Modify_Sales_Data");
				this.obj = obj;
				break;

			case "coursemanagement-feature":
				obj.readWebObjects("CourseManagement");
				this.obj = obj;
				break;	
				
			default:
				System.out.println("Invalid scenario");
				fail("Invalid scenario");

			}

		} catch (IOException e) {
// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Attachment(value = "Pass", type = "text/plain")
	public static String pass(String message) {
		String output = message;
		return output;
	}

	@Attachment(value = "Fail", type = "text/plain")
	public String fail(String message) {
		System.out.println(message);
		String output = message;
		takeScreenShotShutterburg();
		return output;
	}

	/***
	 * @author rajeev
	 * @param message
	 * @param fail
	 * @throws Exception Add Fail Message to Allure Report with SCreenshot and go to
	 *                   next test
	 */
	public void fail(String message, boolean fail) throws Exception {
		if (fail) {
			fail(message);
			throw new Exception(message);
		} else {
			fail(message);
		}
	}

	public String replace(String Data) throws ParseException, Exception {
		String Temp = Data;
// ExpectedResponseData.get(0).replace("$USER_ID",
// app.TestData.get("userid")).replace("$COURSE_ID",app.TestData.get("courseid")).replace("$EMAIL_ID",app.TestData.get("email")).replace("$LEAD_ID",app.TestData.get("leadid")
// ).replace("$TEST_CASE_START_TIME",
// DateTimeConversion.convertTimeToSpecificTimeZone(app.TestCase_StartTime,
// Constants.mysql_instance_timezone)).replace("$TEST_CASE_START_TIME",
// DateTimeConversion.convertTimeToSpecificTimeZone(app.TestCase_StartTime,
// Constants.mysql_instance_timezone)); //un comment after unit testing
		Temp = Temp.replace("$USER_ID", TestData.get("userid"));
		Temp = Temp.replace("$COURSE_ID", TestData.get("courseid"));
		Temp = Temp.replace("$EMAIL_ID", TestData.get("email"));
		Temp = Temp.replace("$LEAD_ID", TestData.get("leadid"));
		Temp = Temp.replace("$TEST_CASE_START_TIME", DateTimeConversion
				.convertTimeToSpecificTimeZone(TestCase_StartTime, Constants.mysql_instance_timezone));
		Temp = Temp.replace("$TEST_CASE_START_TIME", DateTimeConversion
				.convertTimeToSpecificTimeZone(TestCase_StartTime, Constants.mysql_instance_timezone));
		Temp = Temp.replace("$ORIGINAL_PRICE", TestData.get("originalprice"));
		Temp = Temp.replace("$PRICE_USD", TestData.get("priceusd"));
		Temp = Temp.replace("$PRICE_INR", TestData.get("priceinr"));
		Temp = Temp.replace("$DISCOUNT", TestData.get("discount"));
		Temp = Temp.replace("$SERVICETAX", TestData.get("servicetax"));
		Temp = Temp.replace("$FINAL_PRICE", TestData.get("finalprice"));
		Temp = Temp.replace("$CURRENCY", TestData.get("currency"));
		Temp = Temp.replace("$COURSE", TestData.get("course"));
		Temp = Temp.replace("$PHONE_NO", TestData.get("phoneno"));
		Temp = Temp.replace("$CODE", TestData.get("code"));
		Temp = Temp.replace("$GCLID", TestData.get("gclid"));
		Temp = Temp.replace("$UTM_SOURCE", TestData.get("utm_source"));
		Temp = Temp.replace("$UTM_CAMPAIGN", TestData.get("utm_campaign"));
		Temp = Temp.replace("$UTM_MEDIUM", TestData.get("utm_medium"));
		Temp = Temp.replace("$UTM_CONTENT", TestData.get("utm_content"));
		Temp = Temp.replace("$UTM_TERM", TestData.get("utm_term"));
		Temp = Temp.replace("$EDUCASH_VALUE", TestData.get("educashvalue"));
		Temp = Temp.replace("$EDUCASH_POINTS", TestData.get("educashpoints"));
		Temp = Temp.replace("$UPFRONT", TestData.get("upfront"));
		Temp = Temp.replace("$URL_PARAM", TestData.get("url_param"));	
		Temp = Temp.replace("$SHORT_CODE", TestData.get("mailchimp_shortCode"));
		Temp = Temp.replace("$SHORT_NAME", TestData.get("short_name"));
		Temp = Temp.replace("$WELCOME_MAIL", TestData.get("mailchimp_welcomeMail"));
		Temp = Temp.replace("$ENROLL_MAIL", TestData.get("mailchimp_enrollMail"));
		Temp = Temp.replace("$SPWELCOME_MAIL", TestData.get("sparkpost_welcomeMail"));
		Temp = Temp.replace("$SPENROLL_MAIL", TestData.get("sparkpost_enrollMail"));

// this.obj=obj;
		return Temp;
	}

	public void launchBrowser() throws Exception {
		OpenBrowser();
	}

	public void createnewUser(String Email, String PhoneNumber, String Password, String code) throws Exception {
		OpenBrowser();
		signUpEmailPhInputScreen(Email, PhoneNumber, code);
		signUpPasswordScreen(Password);
		signupVerification();
	}

	public boolean compareErrorMessage(String ActualMessage, String ExpectedMessage) {
		if (ActualMessage != null) {
			if (ActualMessage.equals(ExpectedMessage))
				return true;
			else {
				error_message.append("Actual and Expected Email Error Message are different Actual Message="
						+ ActualMessage + " Expected Message=" + ExpectedMessage);
				return false;
			}
		} else {
			error_message.append("Actual Error Message is null");
			return false;
		}
	}

	public boolean invalidEmail_PhoneNo_Password_SignupError(String ErrorType, String ExpectedMessage)
			throws Exception {
		if (ErrorType.toLowerCase().equals("email"))
			ObjectArray = obj.getObjectArray("Signup_invalid_email_error");
		else if (ErrorType.toLowerCase().equals("phoneno"))
			ObjectArray = obj.getObjectArray("Signup_invalid_phone_error");
		else if (ErrorType.toLowerCase().equals("password"))
			ObjectArray = obj.getObjectArray("Signup_invalid_password_error");
		String ActualMessage = getText(ObjectArray[3], ObjectArray[2]);
		return (compareErrorMessage(ActualMessage, ExpectedMessage));
	}

	public void closeBrowser() {
		driver.close();
	}

	public void login(String UserName, String Password) throws Exception {
		ObjectArray = obj.getObjectArray("loginbutton");
		click(ObjectArray[3], ObjectArray[2]);
		ObjectArray = obj.getObjectArray("Signin");
		waitUntilElementVisible(ObjectArray[3], ObjectArray[2], Constants.explicit_wait_time);
		Thread.sleep(Constants.general_wait_time);
		ObjectArray = obj.getObjectArray("username");
		type(ObjectArray[3], ObjectArray[2], UserName);
		ObjectArray = obj.getObjectArray("password");
		type(ObjectArray[3], ObjectArray[2], Password);
		ObjectArray = obj.getObjectArray("Signin");
		click(ObjectArray[3], ObjectArray[2]);
		ObjectArray = obj.getObjectArray("Signup_verification");
		waitUntilElementVisible(ObjectArray[3], ObjectArray[2], Constants.explicit_wait_time);
		// ObjectArray=obj.getObjectArray("DropQuery_SubmitButton");
		// waitUntilElementClickable(ObjectArray[3],
		// ObjectArray[2],Constants.explicit_wait_time);
		// tetsing commit
		TestData.put("email", UserName);
		TestData.put("userid", DBUtils.getUserId(UserName) + "");
		TestData.put("phoneno", Constants.global_phonenumber);
	}

	public void signUpEmailPhInputScreen(String Email, String PhNo, String code) throws Exception {
		ObjectArray = obj.getObjectArray("SignUp");
		click(ObjectArray[3], ObjectArray[2]);
		Thread.sleep(Constants.general_wait_time);
		ObjectArray = obj.getObjectArray("Signup_country_code");
		click(ObjectArray[3], ObjectArray[2]);
		ObjectArray = obj.getObjectArray("Signup_country_code_selection");
		String Locator = ObjectArray[3];
		Locator = Locator.replace("$CODE", code);
		scrollIntoViewAndClick(Locator, ObjectArray[2]);
		ObjectArray = obj.getObjectArray("SignUp_Email");
		type(ObjectArray[3], ObjectArray[2], Email);
		ObjectArray = obj.getObjectArray("SignUp_PhNo");
		type(ObjectArray[3], ObjectArray[2], PhNo);
		ObjectArray = obj.getObjectArray("SignUp_Button");
		click(ObjectArray[3], ObjectArray[2]);
	}

	public void signUpPasswordScreen(String Password) throws Exception, InterruptedException {
		ObjectArray = obj.getObjectArray("SignUp_Password");
		waitUntilElementVisible(ObjectArray[3], ObjectArray[2], 30);
		ObjectArray = obj.getObjectArray("SignUp_Password");
		type(ObjectArray[3], ObjectArray[2], Password);
		ObjectArray = obj.getObjectArray("Start_Learning_Signup");
		click(ObjectArray[3], ObjectArray[2]);
	}

	/**
	 * public void InvalidEmailSignUp(String Email,String PhNo,String Password)
	 * throws CustomException, InterruptedException {
	 * ObjectArray=obj.getObjectArray("SignUp"); click( ObjectArray[3],
	 * ObjectArray[2]); ObjectArray=obj.getObjectArray("SignUp_Email"); type(
	 * ObjectArray[3], ObjectArray[2],Email);
	 * ObjectArray=obj.getObjectArray("SignUp_PhNo"); type( ObjectArray[3],
	 * ObjectArray[2],PhNo); ObjectArray=obj.getObjectArray("SignUp_Button"); click(
	 * ObjectArray[3], ObjectArray[2]);
	 * 
	 * }
	 * 
	 * public void InvalidPhNoSignUp(String Email,String PhNo,String Password)
	 * throws CustomException, InterruptedException {
	 * ObjectArray=obj.getObjectArray("SignUp"); click(ObjectArray[3],
	 * ObjectArray[2]); ObjectArray=obj.getObjectArray("SignUp_Email"); type(
	 * ObjectArray[3], ObjectArray[2],Email);
	 * ObjectArray=obj.getObjectArray("SignUp_PhNo"); type( ObjectArray[3],
	 * ObjectArray[2],PhNo); ObjectArray=obj.getObjectArray("SignUp_Button"); click(
	 * ObjectArray[3], ObjectArray[2]); }
	 * 
	 * public void invalidPasswordsignUp(String Email,String PhNo,String Password)
	 * throws CustomException, InterruptedException {
	 * ObjectArray=obj.getObjectArray("SignUp"); click( ObjectArray[3],
	 * ObjectArray[2]); ObjectArray=obj.getObjectArray("SignUp_Email"); type(
	 * ObjectArray[3], ObjectArray[2],Email);
	 * ObjectArray=obj.getObjectArray("SignUp_PhNo"); type( ObjectArray[3],
	 * ObjectArray[2],PhNo); ObjectArray=obj.getObjectArray("SignUp_Button"); click(
	 * ObjectArray[3], ObjectArray[2]);
	 * ObjectArray=obj.getObjectArray("SignUp_Password");
	 * waitUntilElementVisible(ObjectArray[3], ObjectArray[2],30);
	 * ObjectArray=obj.getObjectArray("SignUp_Password"); type( ObjectArray[3],
	 * ObjectArray[2],Password);
	 * ObjectArray=obj.getObjectArray("Start_Learning_Signup"); click(
	 * ObjectArray[3], ObjectArray[2]); }
	 * 
	 * public void invalidLogin(String UserName,String Password,String
	 * AssertionMessage) throws CustomException, InterruptedException {
	 * ObjectArray=obj.getObjectArray("loginbutton"); click( ObjectArray[3],
	 * ObjectArray[2]); ObjectArray=obj.getObjectArray("username"); type(
	 * ObjectArray[3], ObjectArray[2],UserName);
	 * ObjectArray=obj.getObjectArray("password"); type( ObjectArray[3],
	 * ObjectArray[2],Password); ObjectArray=obj.getObjectArray("login"); click(
	 * ObjectArray[3], ObjectArray[2]); }
	 * 
	 * public void payFullCourseValidScenario(String Email,String PhNo) throws
	 * CustomException, InterruptedException {
	 * 
	 * }
	 **/

	public void extractActualDBResultsForAssertion(DataTable AssertionDetails, String Query) throws Exception {
		HashMap<String, String> ExpectedData = new HashMap<String, String>();
		HashMap<String, String> ActualData = new HashMap<String, String>();
		List<List<String>> ExpectedResponseDataArray = AssertionDetails.raw();
		List<String> ExpectedResponseHeaders = ExpectedResponseDataArray.get(0);
		List<String> ExpectedResponseData = ExpectedResponseDataArray.get(1);
		for (int i = 0; i < ExpectedResponseHeaders.size(); i++) {
			ExpectedData.put(ExpectedResponseHeaders.get(i), ExpectedResponseData.get(i));
		}
		ResultSet rs = DBUtils.executeQuery(
				Query.replace("$userid", TestData.get("userid")).replaceAll("$courseid", TestData.get("courseid")));
		if (rs != null) {
			if (rs.next()) {
				for (int i = 0; i < ExpectedResponseHeaders.size(); i++) {
					ActualData.put(ExpectedResponseHeaders.get(i), rs.getString(ExpectedResponseHeaders.get(i)));
				}
			}
		}
		boolean flag = true;
		for (int i = 0; i < ExpectedResponseHeaders.size(); i++) {
			if (!(ActualData.get(ExpectedResponseHeaders.get(i)).toString()
					.equals(ExpectedData.get(ExpectedResponseHeaders.get(i)).toString()))) {
				flag = false;
				System.out.println(ExpectedResponseHeaders.get(i) + " Not Matching Actual Value="
						+ ActualData.get(ExpectedResponseHeaders.get(i)).toString() + " Expected value="
						+ ExpectedData.get(ExpectedResponseHeaders.get(i)).toString());
				error_message.append(ExpectedResponseHeaders.get(i) + " Not Matching Actual Value="
						+ ActualData.get(ExpectedResponseHeaders.get(i)).toString() + " Expected value="
						+ ExpectedData.get(ExpectedResponseHeaders.get(i)).toString() + "\n");
			}
		}
		if (!flag) {
			Assert.fail(error_message.toString());
		}
	}

	@Attachment
	public byte[] attachScreenShotToAllure(byte[] snapshot) {
		return snapshot;
	}

	public void takeScreenShotShutterburg() {
		Shutterbug.shootPage(driver, ScrollStrategy.WHOLE_PAGE).withName("Screenshot1").save();
		// save(System.getProperty("user.dir")+File.separator+"//ScreenShot1.png").w
		File file = new File(
				System.getProperty("user.dir") + File.separator + "screenshots" + File.separator + "Screenshot1.png");
		try {
			// Reading a Image file from file system
			FileInputStream imageInFile = new FileInputStream(file);
			byte imageData[] = new byte[(int) file.length()];
			imageInFile.read(imageData);

			// Converting Image byte array into Base64 String
			// String imageDataString = encodeImage(imageData);
			attachScreenShotToAllure(imageData);
			imageInFile.close();
			System.out.println("Image Successfully Manipulated!");
		} catch (FileNotFoundException e) {
			System.out.println("Image not found" + e);
		} catch (IOException ioe) {
			System.out.println("Exception while reading the Image " + ioe);
		}
	}

	public void signupVerification() throws Exception {
		ObjectArray = obj.getObjectArray("Signup_verification");
		waitUntilElementVisible(ObjectArray[3], ObjectArray[2], Constants.explicit_wait_time * 4);
	}

	public List<String> appendPrefix(List<String> Header, List<String> Data) throws Exception {
		List<String> UpdatedData = new ArrayList<String>();
		for (int i = 0; i < Header.size(); i++) {
			String datarepalced = replace(Data.get(i));
			if (Header.get(i).toString().trim().substring(0, 2).equals("##"))
				UpdatedData.add(i, Constants.prefix1 + Constants.prefix2 + datarepalced);
			else if (Header.get(i).toString().trim().substring(0, 1).equals("#"))
				UpdatedData.add(i, Constants.prefix1 + datarepalced);
			else
				UpdatedData.add(i, datarepalced);
		}
		return UpdatedData;
	}

	public List<String> dataTableToList(DataTable SearchOutput) throws Exception {
		List<List<String>> ExpectedSearchOutputs = SearchOutput.raw();
		List<String> Result = new ArrayList<String>();
		for (int i = 1; i < ExpectedSearchOutputs.size(); i++) {
			List<String> ExpecteSearchOutPut = ExpectedSearchOutputs.get(i);
			Result.add(i, ExpecteSearchOutPut.get(1));
		}
		return Result;
	}

	public boolean checkTime(String ActualTimeStamp, String ExpectedTimeStamp) throws ParseException {
		String ActualDate = ActualTimeStamp.split(" ")[2] + "-" + ActualTimeStamp.split(" ")[1] + "-"
				+ ActualTimeStamp.split(" ")[3] + " " + ActualTimeStamp.split(" ")[4];
		// String ExpectedDate=ExpectedTimeStamp.split("
		// ")[2]+"-"+ExpectedTimeStamp.split(" ")[1]+"-"+ExpectedTimeStamp.split("
		// ")[3]+" "+ExpectedTimeStamp.split(" ")[4];
		// String ExpectedDate=ExpectedTimeStamp.split("
		// ")[2]+"-"+ExpectedTimeStamp.split(" ")[1]+"-"+ExpectedTimeStamp.split("
		// ")[3]+" "+ExpectedTimeStamp.split(" ")[4];
		System.out.println("Actual Date=" + ActualDate);
		System.out.println("Expected Time=" + ExpectedTimeStamp);

		DateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		Date Actual = df.parse(ActualDate);
		Date Expected = new Date(ExpectedTimeStamp);
		long DiffSeconds = (Actual.getTime() - Expected.getTime()) / 1000;
		if (DiffSeconds > 0 && DiffSeconds < Constants.timestamp_threshold) {
			System.out.println("time within limit");
			return true;
		} else {
			System.out.println("time outside threshold limit");
			return false;
		}
	}

	public String removeDateTimeStampField(String EventLabel, String FieldName) throws Exception {
		return (EventLabel.substring(0, EventLabel.indexOf(FieldName)) + EventLabel
				.substring(EventLabel.indexOf(')', EventLabel.indexOf(FieldName)) + 1, EventLabel.length()));
	}

	public String extractDateTimeStampField(String EventLabel, String FieldName) throws Exception {
		return (EventLabel
				.substring(EventLabel.indexOf(FieldName), EventLabel.indexOf(')', EventLabel.indexOf(FieldName)) + 1)
				.replace(FieldName + ":", "").trim());
	}

	public String removeSearchTimeStampField(String s, String delimiter, int indextoSkip) throws Exception {
		String text = "";
		String a[] = s.split(delimiter);
		for (int i = 0; i < a.length; i++) {
			if (i != indextoSkip) {
				if (i + 1 != a.length)
					text = text + a[i] + ";";
				else
					text = text + a[i];
			} else {
				text = text + delimiter;
			}
		}
		return (text);
	}

	public String extractSearchTimeStampField(String s, String delimiter, int index) throws Exception {
		return s.split(delimiter)[index];
	}

	@Attachment
	public String attachment(String harlogs) {
		return harlogs;
	}

	/**
	 * @author rajeev
	 * @return generate 10 digit randon number
	 */
	public String generatePhoneNumber() {
		{
			int num1, num2, num3;
			int set2, set3;
			Random generator = new Random();
			num1 = generator.nextInt(7) + 1;
			num2 = generator.nextInt(8);
			num3 = generator.nextInt(8);
			set2 = generator.nextInt(643) + 100;
			set3 = generator.nextInt(8999) + 1000;
			String number = Integer.toString(num1) + Integer.toString(num2) + Integer.toString(num3)
					+ Integer.toString(set2) + Integer.toString(set3);
			return number;
		}
	}

	/**
	 * @author sowmya
	 * @func to login to admin portal
	 */
	public void logintoadmin() {
		// TODO Auto-generated method stub
		try {
			String[] ObjectArrayemail = obj.getObjectArray("UserName");
			String[] ObjectArraypwd = obj.getObjectArray("Password");
			String[] ObjectArraylogin = obj.getObjectArray("Login");

			type(ObjectArrayemail[3], ObjectArrayemail[2], Constants.admin_email);
			Thread.sleep(1000);
			type(ObjectArraypwd[3], ObjectArraypwd[2], Constants.admin_pwd);
			Thread.sleep(1000);

			click(ObjectArraylogin[3], ObjectArraylogin[2]);
			Thread.sleep(3000);
			loadUrl(Constants.base_url);

		} catch (Exception e) {

		}

	}

	/**
	 * @author sowmya
	 * @param selectCourse
	 * @throws Exception
	 */
	public void navigateToLinkPage(String selectCourse) throws Exception {
		try {
			String[] ObjectArraynav = obj.getObjectArray("Navigation_first");
			String[] ObjectArraysecond = obj.getObjectArray("Navigation_second");
			String[] ObjectArrayThird = obj.getObjectArray("Navigation_third");
			String firstLocator = null, secondLocator = null;
			secondLocator = ObjectArrayThird[3];
			firstLocator = ObjectArraysecond[3];
			scrollToUp();
			boolean flag = false;

			String[] path = selectCourse.split("->");

			List<WebElement> firstemenuelement = getWebElements(ObjectArraynav[3], ObjectArraynav[2]);
			for (int i = 0; i < firstemenuelement.size(); i++) {
				if (firstemenuelement.get(i).getAttribute("innerText").contains(path[0])) {
					flag = true;
					mouseOver(firstemenuelement.get(i));
					firstemenuelement.get(i).click();
					Thread.sleep(1000);
					firstLocator = firstLocator.replace("$I", String.valueOf(i + 1));
					secondLocator = secondLocator.replace("$J", String.valueOf(i + 1));
					break;
				}
			}

			Thread.sleep(1000);

			List<WebElement> secondemenuelement = getWebElements(firstLocator, ObjectArraysecond[2]);
			for (int i = 0; i < secondemenuelement.size(); i++) {
				if (secondemenuelement.get(i).getAttribute("innerText").contains(path[1])) {
					flag = true;
					mouseOver(secondemenuelement.get(i));
					secondemenuelement.get(i).click();
					Thread.sleep(1000);
					secondLocator = secondLocator.replace("$I", String.valueOf(i + 1));
					break;
				}
			}
			Thread.sleep(1000);
			if (path.length >= 3) {
				List<WebElement> thirdmenuelement = getWebElements(secondLocator, ObjectArrayThird[2]);
				for (int i = 0; i < thirdmenuelement.size(); i++) {
					if (thirdmenuelement.get(i).getAttribute("innerText").equalsIgnoreCase(path[2])) {
						flag = true;
						mouseOver(thirdmenuelement.get(i));
						thirdmenuelement.get(i).click();
						Thread.sleep(1000);
						break;
					}
				}
			}
			Thread.sleep(2000);

			if (!flag)
				throw new Exception(selectCourse + " not found in ");

			Thread.sleep(4000);
		} catch (Exception e) {
			fail("Failed to Navigate Course Landing Page   " + e, true);
		}
	}

	/**
	 * @author sowmya
	 * @param email
	 * @param courseid
	 * @param leadtype
	 * @throws Exception
	 */
	public void uploadbulkleadfunc(String email, String courseid, String leadtype) throws Exception {
		try {
			String[] ObjectArraychoosefile = obj.getObjectArray("b2c_choosefile");
			String[] ObjectArrayUPlead = obj.getObjectArray("b2c_Uplead");
			String[] ObjectArrayWebinarlead = obj.getObjectArray("b2c_Webinar");
			String[] ObjectArraysubmit = obj.getObjectArray("b2c_formsubmit");
			File inputFile = new File(workingDir + "/driver/Bulk_Upload_leads.csv");

			WebElement element = getWebElement(ObjectArraychoosefile[3], ObjectArraychoosefile[2]);
			csvreaderfunc(email, courseid, "B2C", inputFile);
			element.sendKeys(workingDir + "/driver/Bulk_Upload_leads.csv");

			Thread.sleep(3000);

			switch (leadtype) {
			case "UP":
				click(ObjectArrayUPlead[3], ObjectArrayUPlead[2]);
				Thread.sleep(2000);
				break;

			case "Webinar":
				click(ObjectArrayWebinarlead[3], ObjectArrayWebinarlead[2]);
				Thread.sleep(2000);
				break;

			default:
				Thread.sleep(1000);
				break;

			}

			click(ObjectArraysubmit[3], ObjectArraysubmit[2]);

			Thread.sleep(2000);

		} catch (Exception e) {
			fail("b2c Bulk upload got failed" + e, true);
		}
	}

	/**
	 * @author sowmya
	 * @param email
	 * @param courseid
	 * @param coursetype
	 * @param inputfile
	 * @throws Exception
	 */
	public void csvreaderfunc(String email, String courseid, String coursetype, File inputfile) throws Exception {
		try {
			String[] splitcourseid = courseid.split(",");

			// Read existing file
			CSVReader reader = new CSVReader(new FileReader(inputfile), ',');
			List<String[]> csvBody = reader.readAll();

			for (int i = 1; i <= 3; i++) {
				if (coursetype.contains("B2C")) {
					readcsvandupdateb2c(csvBody, i, splitcourseid, email);

				} else if (coursetype.contains("PGP")) {
					readcsvandupdatepgp(csvBody, i, splitcourseid, email);
				}
				reader.close();

				// Write to CSV file which is open
				CSVWriter writer = new CSVWriter(new FileWriter(inputfile), ',');
				writer.writeAll(csvBody);
				Thread.sleep(1000);
				writer.flush();
				writer.close();
			}
		} catch (Exception e) {
			fail("csv reader func got failed" + e, true);
		}
	}

	/**
	 * @author sowmya
	 * @param csvBody
	 * @param i
	 * @param splitcourseid
	 * @param email
	 * @throws Exception
	 */
	public void readcsvandupdateb2c(List<String[]> csvBody, int i, String[] splitcourseid, String email)
			throws Exception {
		try {
			TestData.put("utm_source", csvBody.get(i)[0]);
			TestData.put("utm_medium", csvBody.get(i)[1]);
			TestData.put("utm_campaign", csvBody.get(i)[2]);
			TestData.put("utm_term", csvBody.get(i)[3]);
			TestData.put("utm_content", csvBody.get(i)[4]);
			TestData.put("phoneno", csvBody.get(i)[9]);
			TestData.put("code", csvBody.get(i)[10]);

			// get CSV row column and replace with by using row and column
			csvBody.get(i)[5] = splitcourseid[i - 1];
			csvBody.get(i)[8] = generatePhoneNumber() + email;
			TestData.put("email", csvBody.get(i)[8]);
			TestData.put("courseid", csvBody.get(i)[5]);

			Thread.sleep(1000);
		} catch (Exception e) {
			fail(" while reading csv for b2c bulk upload upload got failed" + e, true);

		}
	}

	/**
	 * @author sowmya
	 * @param email
	 * @param courseid
	 * @throws Exception
	 */
	public void pgpuploadbulkleadfunc(String email, String courseid) throws Exception {
		try {
			String[] ObjectArraychoosefile = obj.getObjectArray("pgp_choosefile");
			String[] ObjectArraysubmit = obj.getObjectArray("pgp_submit");
			File inputFile = new File(workingDir + "/driver/pgp_sample_upload.csv");

			Thread.sleep(2000);

			WebElement element = getWebElement(ObjectArraychoosefile[3], ObjectArraychoosefile[2]);

			csvreaderfunc(email, courseid, "PGP", inputFile);
			Thread.sleep(2000);

			element.sendKeys(workingDir + "/driver/pgp_sample_upload.csv");

			Thread.sleep(2000);

			click(ObjectArraysubmit[3], ObjectArraysubmit[2]);

			Thread.sleep(2000);

		} catch (Exception e) {
			fail("pgp bulk upload upload got failed" + e, true);
		}
	}

	/**
	 * @author sowmya
	 * @param csvBody
	 * @param i
	 * @param splitcourseid
	 * @param email
	 * @throws Exception
	 */
	public void readcsvandupdatepgp(List<String[]> csvBody, int i, String[] splitcourseid, String email)
			throws Exception {
		try {
			TestData.put("utm_source", csvBody.get(i)[0]);
			TestData.put("utm_medium", csvBody.get(i)[1]);
			TestData.put("utm_campaign", csvBody.get(i)[2]);
			TestData.put("utm_term", csvBody.get(i)[3]);
			TestData.put("phoneno", csvBody.get(i)[8]);
			TestData.put("code", csvBody.get(i)[9]);
			TestData.put("exp", csvBody.get(i)[10]);

			// get CSV row column and replace with by using row and column
			csvBody.get(i)[4] = splitcourseid[i - 1];
			csvBody.get(i)[7] = generatePhoneNumber() + email;
			TestData.put("email", csvBody.get(i)[7]);
			TestData.put("courseid", csvBody.get(i)[4]);

			Thread.sleep(2000);
		} catch (Exception e) {
			fail("while reading csv for pgp bulk upload upload got failed" + e, true);
		}
	}

	/**
	 * @author sowmya
	 * @param email
	 * @param name
	 * @param country
	 * @throws Exception
	 */
	public void filluserregform(String email, String name, String country) throws Exception {
		try {
			String[] ObjectArrayemail = obj.getObjectArray("UserReg_email");
			String[] ObjectArrayfirstname = obj.getObjectArray("UserReg_firstname");
			String[] ObjectArraylastname = obj.getObjectArray("UserReg_lastname");
			String[] ObjectArraycountry = obj.getObjectArray("UserReg_country");
			String[] ObjectArraysubmit = obj.getObjectArray("UserReg_submit");
			String appendemail = generatePhoneNumber() + email;

			type(ObjectArrayemail[3], ObjectArrayemail[2], appendemail);
			TestData.put("email", appendemail);
			type(ObjectArrayfirstname[3], ObjectArrayfirstname[2], name);

			type(ObjectArraylastname[3], ObjectArraylastname[2], name);

			selectByVisibleText(ObjectArraycountry[3], ObjectArraycountry[2], country, "visibletext");
			Thread.sleep(1000);

			click(ObjectArraysubmit[3], ObjectArraysubmit[2]);
			Thread.sleep(3000);
		} catch (Exception e) {
			fail("user registration form got failed" + e, true);
		}
	}

	/**
	 * author aishwarya params email
	 * 
	 * @throws Exception
	 */
	public void saleslead(String email, String course) throws Exception {
		try {
			String LeadMedium[] = obj.getObjectArray("LeadMedium");
			String LeadCampaign[] = obj.getObjectArray("LeadCampaign");
			String LeadTerm[] = obj.getObjectArray("LeadTerm");
			String LeadContent[] = obj.getObjectArray("LeadContent");
			String CourseName[] = obj.getObjectArray("CourseName");
			String FirstName[] = obj.getObjectArray("FirstName");
			String LastName[] = obj.getObjectArray("LastName");
			String Email[] = obj.getObjectArray("Email");
			String PhoneNumber[] = obj.getObjectArray("PhoneNumber");
			String Country[] = obj.getObjectArray("Country");
			String CountrySelect[] = obj.getObjectArray("CountrySelect");
			String Assignee[] = obj.getObjectArray("Assignee");
			String AssigneeSelect[] = obj.getObjectArray("AssigneeSelect");
			String Submit[] = obj.getObjectArray("Submit");
			click(LeadMedium[3], LeadMedium[2]);
			clearDefaultText(LeadMedium[3], LeadMedium[2]);
			type(LeadMedium[3], LeadMedium[2], "Test medium");
			click(LeadCampaign[3], LeadCampaign[2]);
			clearDefaultText(LeadCampaign[3], LeadCampaign[2]);
			type(LeadCampaign[3], LeadCampaign[2], "Test campaign");
			click(LeadTerm[3], LeadTerm[2]);
			clearDefaultText(LeadTerm[3], LeadTerm[2]);
			type(LeadTerm[3], LeadTerm[2], "Test term");
			click(LeadContent[3], LeadContent[2]);
			clearDefaultText(LeadContent[3], LeadContent[2]);
			type(LeadContent[3], LeadContent[2], "Test content");
			click(CourseName[3], CourseName[2]);
			WebElement element = driver.findElement(By.id("coursename"));
			Select dropdown = new Select(element);
			dropdown.selectByVisibleText(course);
			click(FirstName[3], FirstName[2]);
			clearDefaultText(FirstName[3], FirstName[2]);
			type(FirstName[3], FirstName[2], "Test firstname");
			click(LastName[3], LastName[2]);
			clearDefaultText(LastName[3], LastName[2]);
			type(LastName[3], LastName[2], "Test lastname");
			click(Email[3], Email[2]);
			clearDefaultText(Email[3], Email[2]);
			type(Email[3], Email[2], email);
			click(PhoneNumber[3], PhoneNumber[2]);
			Thread.sleep(3000);
			driver.switchTo().alert().accept();
			Thread.sleep(1000);
			click(PhoneNumber[3], PhoneNumber[2]);
			clearDefaultText(PhoneNumber[3], PhoneNumber[2]);
			type(PhoneNumber[3], PhoneNumber[2], "1234567890");
			click(Country[3], Country[2]);
			click(CountrySelect[3], CountrySelect[2]);
			click(Assignee[3], Assignee[2]);
			click(AssigneeSelect[3], AssigneeSelect[2]);
			click(Submit[3], Submit[2]);
			Thread.sleep(Constants.general_wait_time);
			driver.switchTo().alert().accept();
			Thread.sleep(Constants.general_wait_time * 2);
		} catch (Exception e) {
			fail("Failed to verify saleslead form   " + e, true);
		}
	}

	/**
	 * author aishwarya
	 * 
	 * @throws Exception
	 */
	public void pgp_add_dummy_faculty() throws Exception {
		try {
			String Faculty_email[] = obj.getObjectArray("Faculty_email");
			String Faculty_fname[] = obj.getObjectArray("Faculty_fname");
			String Faculty_lname[] = obj.getObjectArray("Faculty_lname");
			String Faculty_mobile[] = obj.getObjectArray("Faculty_mobile");
			String Faculty_add[] = obj.getObjectArray("Faculty_add");
			Thread.sleep(3000);
			click(Faculty_email[3], Faculty_email[2]);
			type(Faculty_email[3], Faculty_email[2], "aishwarya.nr@edureka.co");
			click(Faculty_fname[3], Faculty_fname[2]);
			type(Faculty_fname[3], Faculty_fname[2], "firstname");
			click(Faculty_lname[3], Faculty_lname[2]);
			type(Faculty_lname[3], Faculty_lname[2], "lastname");
			click(Faculty_mobile[3], Faculty_mobile[2]);
			type(Faculty_mobile[3], Faculty_mobile[2], "1234567890");
			click(Faculty_add[3], Faculty_add[2]);
			Thread.sleep(2000);
		} catch (Exception e) {
			fail("Failed to add pgp dummy faculty   " + e, true);
		}
	}

	/**
	 * @author sowmya
	 * @param casetype
	 * @param email
	 * @param Expectedmessage
	 * @throws Exception
	 */
	public void verifySearchFunctionality(String casetype, String email, String Expectedmessage) throws Exception {
		try {
			String[] ObjectArrayuseremail = obj.getObjectArray("Referral_useremail");
			String[] ObjectArrayresponse = obj.getObjectArray("Referral_form_response");
			String[] ObjectArraysubmit = obj.getObjectArray("Referral_submit");

			type(ObjectArrayuseremail[3], ObjectArrayuseremail[2], email);
			Thread.sleep(1000);
			click(ObjectArraysubmit[3], ObjectArraysubmit[2]);
			Thread.sleep(1000);

			WebElement elementmessage = getWebElement(ObjectArrayresponse[3], ObjectArrayresponse[2]);
			String flashmessage = elementmessage.getAttribute("innerText");

			switch (casetype) {
			case "Non Registered User":
				Assert.assertEquals(Expectedmessage, flashmessage);
				break;

			case "User without Edureka cash":
				Assert.assertEquals(Expectedmessage, flashmessage);
				break;

			case "User with Edureka cash":
				Assert.assertEquals(Expectedmessage, flashmessage);
				break;
			}

		} catch (Exception e) {
			fail("search functionality got failed" + e, true);
		}
	}

	/**
	 * @author sowmya
	 * @param currency
	 * @param email
	 * @param action
	 * @param amount
	 * @throws Exception
	 */
	public void verifyupdateFunctionality(String currency, String email, String action, String amount)
			throws Exception {
		try {
			String[] ObjectArrayupdate = obj.getObjectArray("Referral_update");
			String[] ObjectArraycurrency = obj.getObjectArray("Referral_currencydropdown");
			String[] ObjectArrayemail = obj.getObjectArray("Referral_email");
			String[] ObjectArrayaction = obj.getObjectArray("Referral_action");
			String[] ObjectArrayamount = obj.getObjectArray("Referral_amount");
			String[] ObjectArraydesc = obj.getObjectArray("Referral_desc");
			String[] ObjectArrayexpiry = obj.getObjectArray("Referral_expirydate");
			String[] ObjectArraynext = obj.getObjectArray("Referral_expirydate_next");
			String[] ObjectArrayset = obj.getObjectArray("Referral_expirydate_set");
			String[] ObjectArraysubmit = obj.getObjectArray("Referral_submit_update");
			String[] ObjectArrayresponse = obj.getObjectArray("Referral_form_response");

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date dateWithoutTime = sdf.parse(sdf.format(new Date()));

			String[] today = String.valueOf(dateWithoutTime).split(" ");
			click(ObjectArrayupdate[3], ObjectArrayupdate[2]);
			Thread.sleep(2000);

			selectByVisibleText(ObjectArraycurrency[3], ObjectArraycurrency[2], currency, "visibletext");
			Thread.sleep(2000);
			type(ObjectArrayemail[3], ObjectArrayemail[2], email);

			Thread.sleep(1000);
			click(ObjectArrayaction[3], ObjectArrayaction[2]);
			Thread.sleep(3000);
			driver.switchTo().alert().accept();

			selectByVisibleText(ObjectArrayaction[3], ObjectArrayaction[2], action, "visibletext");
			Thread.sleep(1000);

			type(ObjectArrayamount[3], ObjectArrayamount[2], amount);

			type(ObjectArraydesc[3], ObjectArraydesc[2], "testauto");

			Thread.sleep(1000);

			if (action.contains("Credit")) {
				click(ObjectArrayexpiry[3], ObjectArrayexpiry[2]);
				Thread.sleep(1000);
				click(ObjectArraynext[3], ObjectArraynext[2]);
				Thread.sleep(1000);

				click(ObjectArrayset[3], ObjectArrayset[2]);
				Thread.sleep(1000);
			}
			click(ObjectArraysubmit[3], ObjectArraysubmit[2]);
			Thread.sleep(3000);

			WebElement elementmessage = getWebElement(ObjectArrayresponse[3], ObjectArrayresponse[2]);
			String flashmessage = elementmessage.getAttribute("innerText");

			Thread.sleep(1000);

			getEducashvalue(action, amount, currency);

			Assert.assertTrue(flashmessage.contains("You have successfuly"));
			Thread.sleep(1000);
		} catch (Exception e) {
			fail("search functionality got failed" + e, true);
		}
	}

	/**
	 * @author sowmya
	 * @param action
	 * @param value
	 * @param Currency
	 * @throws Exception
	 */
	public void getEducashvalue(String action, String value, String Currency) throws Exception {
		try {
			DecimalFormat df = new DecimalFormat("#.###");
			Float convertedvalue;
			int educash;
			Float amount = Float.valueOf(value);
			Float rate = DBUtils.getCurrencyConvertion(Currency);
			convertedvalue = amount / rate;
//			df.setRoundingMode(RoundingMode.FLOOR);

			TestData.put("educashvalue", String.valueOf(df.format(convertedvalue)));

			// 1 usd = 100 Edu cash
//			educash = convertedvalue *100;

			educash = (int) Math.round((convertedvalue * 100));

			TestData.put("educashpoints", String.valueOf(educash));

			Thread.sleep(1000);

		} catch (Exception e) {
			fail("getting edu cash functionality got failed" + e, true);

		}
	}

	/**
	 * @author sowmya
	 * @param course
	 * @param currency
	 * @param batch
	 * @param price
	 * @param addcashback
	 * @param givemi
	 * @param gateway
	 * @param amountlimitcheck
	 * @param multiplepayment
	 * @throws Exception
	 */
	public void fillcustomlinkform(String course, String currency, String batch, String price, String addcashback,
			String givemi, String gateway, boolean amountlimitcheck, String multiplepayment) throws Exception {
		try {
			String[] ObjectArrayemail = obj.getObjectArray("Customlink_email");
			String[] ObjectArraycode = obj.getObjectArray("Customlink_code");
			String[] ObjectArrayphone = obj.getObjectArray("Customlink_phoneno");
			Thread.sleep(2000);
			type(ObjectArrayemail[3], ObjectArrayemail[2], TestData.get("email"));
			// hardcoded code and phone no
			Thread.sleep(3000);
			if (isAlertPresent()) {
				refresh();
				Thread.sleep(2000);
				fillcustomlinkform(course, currency, batch, price, addcashback, givemi, gateway, amountlimitcheck,
						multiplepayment);
			}
			clearDefaultText(ObjectArrayphone[3], ObjectArrayphone[2]);
			type(ObjectArrayphone[3], ObjectArrayphone[2], "9876543210");

			selectByVisibleText(ObjectArraycode[3], ObjectArraycode[2], "+91", "visibletext");

			Thread.sleep(1000);
			// second section of form

			ObjectArray = obj.getObjectArray("Customlink_courseselect");

			fillsecondsectioncustomlink(amountlimitcheck, course, currency, batch, price, multiplepayment);
			fillthirdsectioncustomlink(addcashback, givemi, gateway, amountlimitcheck);

		} catch (Exception e) {
			fail("filling custom link functionality got failed" + e, true);
		}
	}

	/**
	 * @author sowmya
	 * @param amountlimitcheck
	 * @param course
	 * @param currency
	 * @param batch
	 * @param price
	 * @param multiplepayment
	 * @throws Exception
	 */
	public void fillsecondsectioncustomlink(boolean amountlimitcheck, String course, String currency, String batch,
			String price, String multiplepayment) throws Exception {
		try {
			String[] ObjectArraycourse = obj.getObjectArray("Customlink_course");
			String[] ObjectArraybatch = obj.getObjectArray("Customlink_batch");
			String[] ObjectArraycurrency = obj.getObjectArray("Customlink_currency");
			String[] ObjectArrayflatprice = obj.getObjectArray("Customlink_flatprice");
			String[] ObjectArrayaddcourse = obj.getObjectArray("Customlink_addcourse");
			String[] ObjectArraybg = obj.getObjectArray("Customlink_bg");
			String[] addcourse = multiplepayment.split(",");
			int finalamount = Integer.valueOf(TestData.get("originalprice"));

			TestData.put("multiplepaymentvalue", multiplepayment);
			Thread.sleep(1000);

			String[] ObjectArraycourseselect = obj.getObjectArray("Customlink_courseselect");
			WebElement elem = getWebElement(ObjectArraycourseselect[3], ObjectArraycourseselect[2]);
			elem.click();

//			JavascriptExecutor executor = (JavascriptExecutor)driver;
//			executor.executeScript("arguments[0].click();", getWebElement(ObjectArraycourseselect[3],ObjectArraycourseselect[2]));

			fetchcontentandselect(course, ObjectArraycourse);
			Thread.sleep(3000);
			selectByVisibleText(ObjectArraybatch[3], ObjectArraybatch[2], batch, "visibletext");

			if (addcourse[0].contains("TRUE")) {
				for (int i = 1; i < addcourse.length; i++) {
					click(ObjectArrayaddcourse[3], ObjectArrayaddcourse[2]);
					Thread.sleep(1000);
					fetchcontentandselect("Please Select Course", ObjectArraycourseselect);
					Thread.sleep(1000);
					TestData.put("courseid" + i,
							String.valueOf(DBUtils.getcoursepidfrom(addcourse[i], "Display_title")));
					fetchcontentandselect(addcourse[i], ObjectArraycourse);
					Thread.sleep(2000);

					List<WebElement> element = getWebElements(ObjectArraybatch[3], ObjectArraybatch[2]);
					Select dropdown = new Select(element.get(i));
					dropdown.selectByVisibleText("Open Batch");
					Thread.sleep(1000);
					break;

				}

			}
			selectByVisibleText(ObjectArraycurrency[3], ObjectArraycurrency[2], currency, "visibletext");

			TestData.put("currency", currency);

//
			if (amountlimitcheck) {
				int amountt = (int) (finalamount - (finalamount * 0.95));
				type(ObjectArrayflatprice[3], ObjectArrayflatprice[2], String.valueOf(amountt));
				Thread.sleep(3000);

				click(ObjectArraybg[3], ObjectArraybg[2]);
				Thread.sleep(3000);
				driver.switchTo().alert().accept();
			}
			finalamount = (int) (finalamount - (finalamount * 0.45));

			type(ObjectArrayflatprice[3], ObjectArrayflatprice[2], String.valueOf(finalamount));
			if (isAlertPresent()) {
				refresh();
				Thread.sleep(2000);
				driver.switchTo().alert().accept();
			}
			Thread.sleep(2000);
			if (!addcourse[0].contains("TRUE")) {
				validatecustomlinkTexts();
			}

		} catch (Exception e) {
			fail("second section " + e, true);
		}
	}

	/**
	 * @author sowmya
	 * @param addcashback
	 * @param givemi
	 * @param gateway
	 * @param amountlimitcheck
	 * @throws Exception
	 */
	public void fillthirdsectioncustomlink(String addcashback, String givemi, String gateway, boolean amountlimitcheck)
			throws Exception {
		try {
			String[] ObjectArraycashback = obj.getObjectArray("Customlink_cashback");
			String[] ObjectArrayemi = obj.getObjectArray("Customlink_emi");
			String[] ObjectArrayemimonth = obj.getObjectArray("Customlink_emimonth");
			String[] ObjectArraygateway = obj.getObjectArray("Customlink_gateway");
			String[] ObjectArrayvalidity = obj.getObjectArray("Customlink_validity");
			String[] ObjectArraygeneratelink = obj.getObjectArray("Customlink_generate");
			String[] ObjectArraybg = obj.getObjectArray("Customlink_bg");
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			Thread.sleep(3000);
			scrollIntoView(ObjectArraygeneratelink[3], ObjectArraygeneratelink[2]);
			if (addcashback.equalsIgnoreCase("TRUE")) {
//    			click(ObjectArraycashback[3],ObjectArraycashback[2]);
				Thread.sleep(3000);
				waitUntilElementPresent(ObjectArraycashback[3], ObjectArraycashback[2], Constants.general_wait_time);
				executor.executeScript("arguments[0].click();",
						getWebElement(ObjectArraycashback[3], ObjectArraycashback[2]));
				Thread.sleep(6000);
				driver.switchTo().alert().accept();

				Thread.sleep(1000);
			}
			if (givemi.equalsIgnoreCase("TRUE")) {
				String[] ObjectArraydebit = obj.getObjectArray("Customlink_emi_debit");
				String[] ObjectArraydebitbank = obj.getObjectArray("Customlink_emi_debitbank");

				String[] ObjectArrayemiselect = obj.getObjectArray("Customlink_emimonthselect");
				String[] ObjectArraydebitbankselect = obj.getObjectArray("Customlink_emi_debitbankselect");

				Thread.sleep(2000);
				waitUntilElementPresent(ObjectArrayemi[3], ObjectArrayemi[2], Constants.general_wait_time);
				executor.executeScript("arguments[0].click();", getWebElement(ObjectArrayemi[3], ObjectArrayemi[2]));
				Thread.sleep(2000);
				scrollIntoView(ObjectArraygeneratelink[3], ObjectArraygeneratelink[2]);

				if (gateway.contains("Razorpay")) {
					executor.executeScript("arguments[0].click();",
							getWebElement(ObjectArraydebit[3], ObjectArraydebit[2]));
					Thread.sleep(4000);

					waitUntilElementPresent(ObjectArraydebitbank[3], ObjectArraydebitbank[2],
							Constants.general_wait_time);
					scrollIntoView(ObjectArraygeneratelink[3], ObjectArraygeneratelink[2]);

					Thread.sleep(1000);
					click(ObjectArraydebitbankselect[3], ObjectArraydebitbankselect[2]);
					Thread.sleep(1000);
					selectByVisibleText(ObjectArraydebitbankselect[3], ObjectArraydebitbankselect[2], "ICICI Bank",
							"visibletext");
				}
//				executor.executeScript("arguments[0].click();",
//						getWebElement(ObjectArraydebitbank[3], ObjectArraydebitbank[2]));
				Thread.sleep(2000);

				click(ObjectArrayemiselect[3], ObjectArrayemiselect[2]);
				Thread.sleep(1000);
				selectByVisibleText(ObjectArrayemiselect[3], ObjectArrayemiselect[2], "2", "visibletext");
//				executor.executeScript("arguments[0].click();",
//						getWebElement(ObjectArrayemimonth[3], ObjectArrayemimonth[2]));
				Thread.sleep(1000);
				scrollIntoView(ObjectArraygeneratelink[3], ObjectArraygeneratelink[2]);
				Thread.sleep(2000);
			}
			Thread.sleep(2000);
			click(ObjectArraybg[3], ObjectArraybg[2]);
			Thread.sleep(3000);
			waitUntilElementVisible(ObjectArraygateway[3], ObjectArraygateway[2], Constants.general_wait_time);

			selectByVisibleText(ObjectArraygateway[3], ObjectArraygateway[2], gateway, "visibletext");

			Thread.sleep(2000);

			selectByVisibleText(ObjectArrayvalidity[3], ObjectArrayvalidity[2], "45 Min", "visibletext");

			Thread.sleep(3000);
			executor.executeScript("arguments[0].click();",
					getWebElement(ObjectArraygeneratelink[3], ObjectArraygeneratelink[2]));

			Thread.sleep(3000);

		} catch (Exception e) {
			fail("third section form got failed" + e, true);
		}
	}

	/**
	 * @author sowmya
	 * @throws Exception
	 */
	public void validatecustomlinkTexts() throws Exception {
		try {
			String[] ObjectArraypricetext = obj.getObjectArray("Customlink_coursepricetext");
			String[] ObjectArrayeducashtext = obj.getObjectArray("Customlink_educashtext");

			String courseprice = getText(ObjectArraypricetext[3], ObjectArraypricetext[2]);
			String actualeducash = getText(ObjectArrayeducashtext[3], ObjectArrayeducashtext[2]);
			String expectededucash = String.valueOf(DBUtils.getEdurekacash(TestData.get("userid")));
			Assert.assertEquals(TestData.get("originalprice"), courseprice);
			pass("Expected Original value->" + TestData.get("originalprice") + " Actual Original Value->"
					+ courseprice);
			Assert.assertEquals(expectededucash, actualeducash);
			pass("Expected Edu Cash value->" + expectededucash + " Actual Edu cash Value->" + actualeducash);

		} catch (Exception e) {
			fail("Texts are not matching" + e, true);
		}
	}

	/**
	 * @author sowmya
	 * @param course
	 * @param ObjectArray
	 * @throws Exception
	 */
	public void fetchcontentandselect(String course, String[] ObjectArray) throws Exception {
		try {
			Thread.sleep(1000);
			boolean flag = false;
			List<WebElement> element = getWebElements(ObjectArray[3], ObjectArray[2]);
			for (int i = 0; i < element.size(); i++) {
				if (element.get(i).getAttribute("innerText").contains(course)) {
					flag = true;
					Thread.sleep(1000);
					mouseOver(element.get(i));
					Thread.sleep(1000);
					element.get(i).click();
					break;
				}
			}
			if (!flag)
				throw new Exception(course + " not found in ");
		} catch (Exception e) {
			fail("fetchcontenet functionality got failed" + e, true);
		}
	}

	/**
	 * @author sowmya
	 * @throws Exception
	 */
	public void verifypreviewandsendmail() throws Exception {
		try {
			String[] ObjectArraycopylink = obj.getObjectArray("Customlink_linkcopy");
			String[] ObjectArraypreview = obj.getObjectArray("Customlink_preview");
			String[] ObjectArraysendmail = obj.getObjectArray("Customlink_sendmail");
			String windowhandle = null;
			String[] addcourse = TestData.get("multiplepaymentvalue").split(",");

//			String originalHandle = driver.getWindowHandle();
			click(ObjectArraycopylink[3], ObjectArraycopylink[2]);
			Thread.sleep(2000);
			click(ObjectArraysendmail[3], ObjectArraysendmail[2]);
			WebDriverWait wait = new WebDriverWait(driver, 15);
			wait.until(ExpectedConditions.alertIsPresent());
			Alert simpleAlert = driver.switchTo().alert();
			String actualalertText = simpleAlert.getText();

			pass("Send mail alert Text->" + actualalertText);
			Thread.sleep(4000);
			driver.switchTo().alert().accept();
			Thread.sleep(2000);
			click(ObjectArraypreview[3], ObjectArraypreview[2]);
			Thread.sleep(2000);

//			if (addcourse[0].contains("FALSE")) {
			windowhandle = getWindowHandlesthroughURL("preview");
			driver.switchTo().window(windowhandle);
			calculatediscountandfinalpriceonpreview("Preview");
			driver.close();

			windowhandle = getWindowHandlesthroughURL("admin_zoho");
			driver.switchTo().window(windowhandle);
			driver.close();

			windowhandle = getWindowHandlesthroughURL("custom_payment");
			driver.switchTo().window(windowhandle);
			Thread.sleep(2000);
			calculatediscountandfinalpriceonpreview("OSP");

//			} else {
//				windowhandle = getWindowHandlesthroughURL("preview");
//				driver.switchTo().window(windowhandle);
//				calculatediscountandfinalpriceonpreview("Preview");
//				String[] ObjectArraypaynow = obj.getObjectArray("Customlink_paynow");
//				click(ObjectArraypaynow[3], ObjectArraypaynow[2]);
//				Thread.sleep(4000);
//				driver.close();
//				Thread.sleep(1000);
//				windowhandle = getWindowHandlesthroughURL("admin_zoho");
//				driver.switchTo().window(windowhandle);
//				driver.close();
//				Thread.sleep(2000);
//				windowhandle = getWindowHandlesthroughURL("custom_payment");
//				driver.switchTo().window(windowhandle);
//				calculatediscountandfinalpriceonpreview("OSP");
//				refresh();
//				Thread.sleep(2000);
//			}

		} catch (Exception e) {
			fail("preview functionality is not working  " + e, true);
		}
	}

	/**
	 * @author sowmya
	 * @param pagename
	 * @throws Exception
	 */
	public void calculatediscountandfinalpriceonpreview(String pagename) throws Exception {
		try {
			String[] ObjectArraycourseprice = null;
			String[] ObjectArrayfinalprice = null;
			String[] ObjectArraygstvalue = null;
			String[] ObjectArraytotalvalue = null;
			String[] ObjectArrayeducashvalue = null;
			String[] ObjectArraydiscount = null;
			String[] ObjectArraysavings = null;
			String Actualsavings = null;
			String Actualcourseprice = null, Actualnetprice = null;
			String[] addcourse = TestData.get("multiplepaymentvalue").split(",");
			int grandtotal = 0;
			switch (pagename) {
			case "Preview":
				ObjectArraycourseprice = obj.getObjectArray("Preview_courseprice");
				ObjectArrayfinalprice = obj.getObjectArray("Preview_total");
				ObjectArraygstvalue = obj.getObjectArray("Preview_gstvalue");
				ObjectArraytotalvalue = obj.getObjectArray("Preview_grandtotal_inr");
				ObjectArrayeducashvalue = obj.getObjectArray("Preview_educash");
				ObjectArraydiscount = obj.getObjectArray("Preview_discount");
				if (!TestData.get("currency").equalsIgnoreCase("INR")) {
					ObjectArraytotalvalue = obj.getObjectArray("Preview_grandtotal");
				}
				break;

			case "OSP":
				ObjectArraycourseprice = obj.getObjectArray("Offline_originalprice");
				ObjectArrayfinalprice = obj.getObjectArray("Offline_netprice");
				ObjectArraygstvalue = obj.getObjectArray("Offline_gst");
				ObjectArraytotalvalue = obj.getObjectArray("Offline_total");
				ObjectArrayeducashvalue = obj.getObjectArray("OSP_educash");
				ObjectArraysavings = obj.getObjectArray("Offline_savings");
				ObjectArraydiscount = obj.getObjectArray("Offline_discount");
				if (!checkElementExist(ObjectArraysavings[3], ObjectArraysavings[2])) {
					refresh();
					Thread.sleep(3000);
				}
				Actualsavings = getText(ObjectArraysavings[3], ObjectArraysavings[2]);
				Actualsavings = Actualsavings.replace(",", "");

				break;
			}
			int Expectedsavings = 0, Expectedgst = 0, Expectedcalculatetotal = 0, adddiscount = 0;
			String Actualgst = null, Actualtotalvalue = null, Actualeducash = null;
			pass("ON " + pagename + " Calculation");
			int pos = 3, length, gstcounter = 3, totalcounter = 2;
			if (!addcourse[0].equalsIgnoreCase("TRUE")) {
				length = 1;
			} else {
				length = addcourse.length;
			}
			TestData.put("courseid" + 0, TestData.get("courseid"));

			for (int i = 0; i < length; i++) {
				pos = pos + 2;
				gstcounter = gstcounter + i;
				totalcounter = totalcounter + i;

				scrollIntoView(ObjectArraycourseprice[3].replace("$ID", TestData.get("courseid" + i)).replace("$POS",
						String.valueOf(pos)), ObjectArraycourseprice[2]);

				Actualcourseprice = getText(ObjectArraycourseprice[3].replace("$ID", TestData.get("courseid" + i))
						.replace("$POS", String.valueOf(pos)), ObjectArraycourseprice[2]);
				Actualcourseprice = Actualcourseprice.replaceAll("[ ₹,$]", "");

				scrollIntoView(ObjectArraydiscount[3].replace("$ID", TestData.get("courseid" + i)).replace("$POS",
						String.valueOf(pos)), ObjectArraydiscount[2]);
				String Actualdiscount = getText(ObjectArraydiscount[3].replace("$ID", TestData.get("courseid" + i))
						.replace("$POS", String.valueOf(pos)), ObjectArraydiscount[2]);
				Actualdiscount = Actualdiscount.replaceAll("[ ₹,$-]", "");
				TestData.put("discount", Actualdiscount);

				if (pagename.contains("OSP") && addcourse[0].equalsIgnoreCase("TRUE")) {
					if (!checkElementExist(ObjectArrayeducashvalue[3].replace("$ID", TestData.get("courseid" + i))
							.replace("$POS", String.valueOf(pos)), ObjectArrayeducashvalue[2])) {
						pass("edureka cash is not present on OSP in case of multiple course payment");
						Actualeducash = "0";
					}
				} else {
					if (!checkElementExist(ObjectArrayeducashvalue[3].replace("$ID", TestData.get("courseid" + i))
							.replace("$POS", String.valueOf(pos)), ObjectArrayeducashvalue[2])) {
						pass("edureka cash is not present on OSP in case of multiple course payment");
						Actualeducash = "0";
					} else {
						scrollIntoView(ObjectArrayeducashvalue[3].replace("$ID", TestData.get("courseid" + i))
								.replace("$POS", String.valueOf(pos)), ObjectArrayeducashvalue[2]);
						Actualeducash = getText(ObjectArrayeducashvalue[3].replace("$ID", TestData.get("courseid" + i))
								.replace("$POS", String.valueOf(pos)), ObjectArrayeducashvalue[2]);
						Actualeducash = Actualeducash.replaceAll("[ ₹,$-]", "");
					}
				}
				scrollIntoView(ObjectArrayfinalprice[3].replace("$ID", TestData.get("courseid" + i)).replace("$POS",
						String.valueOf(pos)), ObjectArrayfinalprice[2]);
				Actualnetprice = getText(ObjectArrayfinalprice[3].replace("$ID", TestData.get("courseid" + i))
						.replace("$POS", String.valueOf(pos)), ObjectArrayfinalprice[2]);
				Actualnetprice = Actualnetprice.replaceAll("[ ₹,$]", "");

				int calculatedvalue = Integer.valueOf(Actualcourseprice) - Integer.valueOf(Actualdiscount)
						- Integer.valueOf(Actualeducash);

				grandtotal = calculatedvalue + grandtotal;
				adddiscount = adddiscount + Integer.valueOf(Actualdiscount);

			}
			pos = pos + 3;
			TestData.put("discount", String.valueOf(adddiscount));

			if (TestData.get("currency").equalsIgnoreCase("INR")) {
				scrollIntoView(ObjectArraygstvalue[3].replace("$ID", TestData.get("courseid"))
						.replace("$POS", String.valueOf(pos)).replace("$GSTCOUNTER", String.valueOf(gstcounter)),
						ObjectArraygstvalue[2]);
				Actualgst = getText(ObjectArraygstvalue[3].replace("$POS", String.valueOf(pos)).replace("$GSTCOUNTER",
						String.valueOf(gstcounter)), ObjectArraygstvalue[2]);
				Actualgst = Actualgst.replaceAll("[ ₹,$]", "");
				totalcounter = gstcounter + 1;
			}

			scrollIntoView(
					ObjectArraytotalvalue[3].replace("$ID", TestData.get("courseid"))
							.replace("$POS", String.valueOf(pos)).replace("$VALUE", String.valueOf(totalcounter)),
					ObjectArraytotalvalue[2]);
			Actualtotalvalue = getText(ObjectArraytotalvalue[3].replace("$POS", String.valueOf(pos)).replace("$VALUE",
					String.valueOf(totalcounter)), ObjectArraytotalvalue[2]);
			Actualtotalvalue = Actualtotalvalue.replaceAll("[ ₹,$]", "");

			int Expectedcalculatenetprice = Integer.valueOf(TestData.get("originalprice"))
					- Integer.valueOf(TestData.get("discount"));
			if (TestData.get("currency").equalsIgnoreCase("INR")) {
				Expectedgst = (int) Math.ceil((grandtotal * 0.18));
				int discount = Integer.valueOf(TestData.get("discount"));
				Expectedsavings = (int) Math.ceil(discount + (discount * 0.18));
				Expectedcalculatetotal = (int) Math.ceil(((int) grandtotal + (grandtotal * 0.18)));

			} else {
				Expectedcalculatetotal = grandtotal;
				Expectedsavings = Integer.valueOf(TestData.get("discount"));

			}

//					Assert.assertEquals(Actualcourseprice, TestData.get("originalprice"));
//					pass("Expected Original value ->" + TestData.get("originalprice") + "Actual Original Value ->"
//							+ Actualcourseprice);

//					Assert.assertEquals(Actualdiscount,TestData.get("discount"));
			if (TestData.get("currency").equalsIgnoreCase("INR")) {

				Assert.assertEquals(Actualgst, String.valueOf(Expectedgst));
				pass("Expected GST value ->" + String.valueOf(Expectedgst) + "Actual GST Value ->" + Actualgst);
			}
//					Assert.assertEquals(Actualnetprice, String.valueOf(Expectedcalculatenetprice));
//					pass("Expected NET value ->" + String.valueOf(Expectedcalculatenetprice) + "Actual NET Value ->"
//							+ Actualnetprice);

			Assert.assertEquals(Actualtotalvalue, String.valueOf(Expectedcalculatetotal));
			pass("Expected Total value ->" + String.valueOf(Expectedcalculatetotal) + "Actual NET Value ->"
					+ Actualtotalvalue);

			TestData.put("finalprice", Actualtotalvalue);
			TestData.put("servicetax", Actualgst);

			if (pagename.contains("OSP")) {
				if (TestData.get("gateway").contains("EMI")) {
					String[] ObjectArrayupfront = obj.getObjectArray("RPEMI_upfront");
					String Actualupfront = getText(ObjectArrayupfront[3].replace("$POS", String.valueOf(pos))
							.replace("$GSTCOUNTER", String.valueOf(gstcounter)), ObjectArrayupfront[2]);
					Actualupfront = Actualupfront.replaceAll("[ ₹,$]", "");
					TestData.put("upfront", Actualupfront);

					if (addcourse[0].equalsIgnoreCase("TRUE")) {
						if (TestData.get("currency").contains("INR")) {
							int gst = (int) (Integer.valueOf(Actualgst) * 0.5);
							TestData.put("servicetax", String.valueOf(gst));
						}
						int discount = Integer.valueOf(TestData.get("discount")) / 2;
						TestData.put("discount", String.valueOf(discount));

						pass("Upfront value -->" + Actualupfront);
					}
					Assert.assertEquals(String.valueOf(Expectedsavings), Actualsavings);
					pass("Expected Total Savings ->" + String.valueOf(Expectedsavings) + "Actual total Savings ->"
							+ Actualsavings);
				}
			}
			if (pagename.contains("Preview")) {
				String[] ObjectArraypaynow = obj.getObjectArray("Customlink_paynow");
				click(ObjectArraypaynow[3], ObjectArraypaynow[2]);
				Thread.sleep(4000);
			}

		} catch (Exception e) {
			fail("Calculation is wrong on OSP and Preview" + e, true);
		}
	}

	/**
	 * @author sowmya
	 * @param payment_type
	 * @throws Exception
	 */
	public void clickOnPaySecurely(String payment_type) throws Exception {
		waitForSeconds(5);
		try {
			if (payment_type.equalsIgnoreCase("CCAvenue") || payment_type.equalsIgnoreCase("RazorPay")) {
				Thread.sleep(Constants.general_wait_time * 2);
				ObjectArray = obj.getObjectArray("OrderSummary_pay_securely");
				waitUntilElementPresent(ObjectArray[3], ObjectArray[2], Constants.explicit_wait_time);
				click(ObjectArray[3], ObjectArray[2]);
				Thread.sleep(4000);
			} else {
				Thread.sleep(Constants.general_wait_time * 2);
				ObjectArray = obj.getObjectArray("OrderSummary_pay_securely");
				waitUntilElementPresent(ObjectArray[3], ObjectArray[2], Constants.explicit_wait_time);
				click(ObjectArray[3], ObjectArray[2]);
				ObjectArray = obj.getObjectArray("Payment_selection_frame");
				waitUntilElementPresent(ObjectArray[3], ObjectArray[2], Constants.explicit_wait_time);
			}
		} catch (Exception e) {
			fail("Click On Payment selection failed", true);
		}
	}

	/**
	 * @author sowmya
	 * @param paymentMethod
	 * @throws Exception
	 */
	public void selectPaymentMethod(String paymentMethod) throws Exception {
		try {
			ObjectArray = obj.getObjectArray("Payment_selection_frame");
			WebElement Object = getElement(ObjectArray[3], ObjectArray[2]);
			switchToIFrame("webelement", "", Object);
			ObjectArray = obj.getObjectArray("Payment_method_selection_screen");
			waitUntilElementVisible(ObjectArray[3], ObjectArray[2], Constants.explicit_wait_time);
			ObjectArray = obj.getObjectArray("Payment_method_selection");
			String Locator = ObjectArray[3];
			if (paymentMethod.toLowerCase().equals("netbanking"))
				Locator = Locator.replace("$PAYMENT_METHOD", "netbanking");
			else if (paymentMethod.toLowerCase().equals("card"))
				Locator = Locator.replace("$PAYMENT_METHOD", "card");
			else if (paymentMethod.toLowerCase().equals("emi"))
				ObjectArray = obj.getObjectArray("emi");
			click(Locator, ObjectArray[2]);
		} catch (Exception e) {
			fail("Unable to select Payment Method", true);
		}
	}

	/**
	 * @author sowmya
	 * @throws Exception
	 */
	public void verifyPaymentConfirmation() throws Exception {
		try {
			Thread.sleep(1000);
			String[] ObjectArray = null;
			ObjectArray = obj.getObjectArray("Payment_confirmation_screen");
			if (!checkElementExist(ObjectArray[3], ObjectArray[2])) {
				Thread.sleep(1000);
//				driver.manage().window().maximize();
				refresh();
				Thread.sleep(1000);
			}
			Thread.sleep(1000);
			waitUntilElementVisible(ObjectArray[3], ObjectArray[2], Constants.explicit_wait_time);
			Thread.sleep(1000);
			pass("Congrats page is visible");
		} catch (Exception e) {
			fail("Congrats page is not visible after payment" + e, true);
		}
	}

	/**
	 * @author sowmya
	 * @throws Exception
	 */
	public void verifyPaymentConfirmationforemi() throws Exception {
		try {
			Thread.sleep(1000);

			String[] ObjectArray = null;
			ObjectArray = obj.getObjectArray("Congratspage_emi");
			String[] ObjectArrayemicase = obj.getObjectArray("Congratspage_emirp");

//			if (!checkElementExist(ObjectArray[3], ObjectArray[2])) {
//				Thread.sleep(1000);
////				driver.manage().window().maximize();
////				refresh();
//				Thread.sleep(1000);
//			}
			Thread.sleep(1000);
			if (TestData.get("courseassignment").contains("NO")) {
				waitUntilElementVisible(ObjectArrayemicase[3], ObjectArrayemicase[2], Constants.explicit_wait_time);
			} else {
				Thread.sleep(2000);
				waitUntilElementVisible(ObjectArray[3], ObjectArray[2], Constants.explicit_wait_time);
			}
			Thread.sleep(1000);
			pass("Congrats page is visible");
		} catch (Exception e) {
			fail("Congrats page is not visible after payment" + e, true);
		}
	}

	/**
	 * @author sowmya
	 * @param bank
	 * @throws Exception
	 */
	public void selectBankForPayment(String bank) throws Exception {
		try {
			waitForSeconds(3);
			ObjectArray = obj.getObjectArray("Payment_selectbank");
			click(ObjectArray[3], ObjectArray[2]);
			Thread.sleep(2000);
			ObjectArray = obj.getObjectArray("Payment_selectbank-from-list");
			List<WebElement> elements = getWebElements(ObjectArray[3], ObjectArray[2]);
			for (int i = 0; i < elements.size(); i++) {
				if (elements.get(i).getText().contains(bank)) {
					elements.get(i).click();
					Thread.sleep(1000);
					break;
				}
			}
		} catch (Exception e) {
			fail("Unabl to select Bank from dropdown" + e, true);
		}
	}

	/**
	 * @author sowmya
	 * @throws Exception
	 */
	public void makePaymentThroughRazorPay() throws Exception {
		try {
			String originalHandle = driver.getWindowHandle();
			ObjectArray = obj.getObjectArray("Payment_button");
			mouseOver(ObjectArray[3], ObjectArray[2]);
			click(ObjectArray[3], ObjectArray[2]);
			Thread.sleep(1000);
			newTab();
			Thread.sleep(1000);
			String[] successClick = obj.getObjectArray("Payment_success_button_razorpay");
			waitUntilElementClickable(successClick[3], successClick[2], 4000);
			scrollIntoView(successClick[3], successClick[2]);
			Thread.sleep(3000);
			click(successClick[3], successClick[2]);
			Thread.sleep(4000);
			driver.switchTo().window(originalHandle);
			Thread.sleep(1000);
		} catch (Exception e) {
			fail("Unable to make payment through razor pay" + e, true);
		}
	}

	/**
	 * @author sowmya
	 * @throws Exception
	 */
	public void clickOnPaypal() throws Exception {
		try {
			Thread.sleep(2000);
			ObjectArray = obj.getObjectArray("PayPal_Iframe");
			Thread.sleep(1000);
			if (!checkElementExist(ObjectArray[3], ObjectArray[2])) {
				refresh();
				Thread.sleep(1000);
			}
			waitUntilElementPresent(ObjectArray[3], ObjectArray[2], Constants.general_wait_time);
			WebElement Object = getElement(ObjectArray[3], ObjectArray[2]);
			Thread.sleep(1000);
			switchToIFrame("webelement", "", Object);
			ObjectArray = obj.getObjectArray("Paypal_click");
			WebElement object = getWebElement(ObjectArray[3], ObjectArray[2]);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", object);
			Thread.sleep(1000);
		} catch (Exception e) {
			fail("while clikcing payment button got an excpetion" + e, true);

		}

	}

	/**
	 * @author sowmya
	 * @info
	 */
	public void makePaymentThroughPayPal() throws Exception {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			String[] ObjectArrayPaypalNext = obj.getObjectArray("Paypal_Next");
			String[] ObjectArrayPaypalPaynow = obj.getObjectArray("Paypal_paynowbutton");
			String[] ObjectArrayPaypalemico = obj.getObjectArray("Paypalemi_con");
			String[] ObjectArraypaypalemiagree = obj.getObjectArray("Paypalemi_agree");

			String ParentWindow = driver.getWindowHandle();
			Set<String> WindowHandles = driver.getWindowHandles();
			Iterator<String> it = WindowHandles.iterator();
			while (it.hasNext()) {
				String ChildWindow = it.next().toString();
				if (!ChildWindow.equals(ParentWindow)) {
					driver.switchTo().window(ChildWindow);
					break;
				}
			}
			ObjectArray = obj.getObjectArray("Paypal_email");
			if (!checkElementExist(ObjectArray[3], ObjectArray[2])) {
				refresh();
				Thread.sleep(1000);

			}
			clearDefaultText(ObjectArray[3], ObjectArray[2]);
			click(ObjectArray[3], ObjectArray[2]);
			type(ObjectArray[3], ObjectArray[2], Constants.paypal_email);
			if (checkElementExist(ObjectArrayPaypalNext[3], ObjectArrayPaypalNext[2])) {
				click(ObjectArrayPaypalNext[3], ObjectArrayPaypalNext[2]);
				Thread.sleep(1000);

			}
			Thread.sleep(100);
			ObjectArray = obj.getObjectArray("Paypay_password");
			clearDefaultText(ObjectArray[3], ObjectArray[2]);
			click(ObjectArray[3], ObjectArray[2]);
			type(ObjectArray[3], ObjectArray[2], Constants.paypal_pwd);
			Thread.sleep(1000);
			ObjectArray = obj.getObjectArray("Paypal_loginbutton");
//			click(ObjectArray[3], ObjectArray[2]);
			waitUntilElementClickable(ObjectArray[3], ObjectArray[2], 3000);
			Thread.sleep(1000);
			js.executeScript("arguments[0].click();", getWebElement(ObjectArray[3], ObjectArray[2]));
			ObjectArray = obj.getObjectArray("Paypal_paynowbutton");
			String paynowlocator = ObjectArray[3];
			Thread.sleep(Constants.general_wait_time * 4);
			if (checkElementExist(ObjectArrayPaypalemico[3], ObjectArrayPaypalemico[2])) {
//				driver.manage().window().maximize();
				scrollIntoView(ObjectArrayPaypalemico[3], ObjectArrayPaypalemico[2]);
				waitUntilElementClickable(ObjectArrayPaypalemico[3], ObjectArrayPaypalemico[2], 4000);
				click(ObjectArrayPaypalemico[3], ObjectArrayPaypalemico[2]);
				Thread.sleep(1000);
			}
			Thread.sleep(Constants.general_wait_time * 2);
			if (checkElementExist(ObjectArraypaypalemiagree[3], ObjectArraypaypalemiagree[2])) {
//				driver.manage().window().maximize();
				scrollIntoView(ObjectArraypaypalemiagree[3], ObjectArraypaypalemiagree[2]);
				waitUntilElementClickable(ObjectArraypaypalemiagree[3], ObjectArraypaypalemiagree[2], 4000);
				click(ObjectArraypaypalemiagree[3], ObjectArraypaypalemiagree[2]);
				Thread.sleep(Constants.general_wait_time * 2);
			}
			if (!checkElementExist(ObjectArrayPaypalPaynow[3], ObjectArrayPaypalPaynow[2])) {
//				driver.manage().window().maximize();
				refresh();
				waitUntilElementPresent(ObjectArrayPaypalPaynow[3], ObjectArrayPaypalPaynow[2], 1000);
				Thread.sleep(1000);

			}
			waitUntilElementPresent(ObjectArrayPaypalPaynow[3], ObjectArrayPaypalPaynow[2],
					Constants.general_wait_time);
			scrollIntoView(ObjectArrayPaypalPaynow[3], ObjectArrayPaypalPaynow[2]);
			Thread.sleep(1000);
			WebElement object = getWebElement(ObjectArrayPaypalPaynow[3], ObjectArrayPaypalPaynow[2]);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", object);
			Thread.sleep(2000);
			if (checkElementExist(ObjectArrayPaypalPaynow[3], ObjectArrayPaypalPaynow[2])) {
//				driver.manage().window().maximize();
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", object);
				Thread.sleep(1000);

			}

			driver.switchTo().window(ParentWindow);
			driver.switchTo().defaultContent();

			Thread.sleep(1000);
		} catch (Exception e) {
			fail("while making payment got an excpetion" + e, true);
		}

	}

	/**
	 * @author aishwarya for cc avenue payment
	 */
	public void ccavenueDetails() throws Exception {
		try {
			Thread.sleep(Constants.general_wait_time);
			String ObjectArrayccAddress[] = obj.getObjectArray("CCAvenue_Address");
			String ObjectArrayccZip[] = obj.getObjectArray("CCAvenue_Zip");
			String ObjectArrayccState[] = obj.getObjectArray("CCAvenue_State");
			String ObjectArrayccCity[] = obj.getObjectArray("CCAvenue_City");
			String ObjectArrayccNetbanking[] = obj.getObjectArray("CCAvenue_Netbanking");
			String ObjectArrayccBankSelect[] = obj.getObjectArray("CCAvenue_Selectbank");
			String ObjectArrayccPayment[] = obj.getObjectArray("CCAvenue_MakePayment");
			String ObjectArrayccMerchant[] = obj.getObjectArray("CCAvenue_Merchant");
			// String ObjectArrayccCongrats[] = obj.getObjectArray("CCAvenue_Congrats");
			click(ObjectArrayccAddress[3], ObjectArrayccAddress[2]);
			type(ObjectArrayccAddress[3], ObjectArrayccAddress[2], "test address for cc avenue payment");
			click(ObjectArrayccZip[3], ObjectArrayccZip[2]);
			type(ObjectArrayccZip[3], ObjectArrayccZip[2], "560003");
			click(ObjectArrayccState[3], ObjectArrayccState[2]);
			type(ObjectArrayccState[3], ObjectArrayccState[2], "Karnataka");
			click(ObjectArrayccCity[3], ObjectArrayccCity[2]);
			type(ObjectArrayccCity[3], ObjectArrayccCity[2], "Bengaluru");
			WebElement drpDwnList1 = driver.findElement(By.id("orderBillCountry"));
			Select objSel1 = new Select(drpDwnList1);
			objSel1.selectByVisibleText("India");
			scrollToEnd();
			click(ObjectArrayccNetbanking[3], ObjectArrayccNetbanking[2]);
			click(ObjectArrayccBankSelect[3], ObjectArrayccBankSelect[2]);
			WebElement drpDwnList = driver.findElement(By.id("netBankingBank"));
			Select objSel = new Select(drpDwnList);
			objSel.selectByVisibleText("AvenuesTest");
			click(ObjectArrayccPayment[3], ObjectArrayccPayment[2]);
			Thread.sleep(Constants.general_wait_time);
			Thread.sleep(Constants.general_wait_time);
			waitUntilElementVisible(ObjectArrayccMerchant[3], ObjectArrayccMerchant[2], Constants.explicit_wait_time);
			click(ObjectArrayccMerchant[3], ObjectArrayccBankSelect[2]);
			Thread.sleep(Constants.general_wait_time * 5);
			// waitUntilElementVisible(ObjectArrayccCongrats[3], ObjectArrayccCongrats[2],
			// Constants.explicit_wait_time);
			// click(ObjectArrayccCongrats[3], ObjectArrayccCongrats[2]);
		} catch (Exception e) {
			fail("CCAvenue payment not working", true);
		}
	}

	/**
	 * @author aishwarya
	 * @param email
	 * @param courseid
	 * @throws Exception
	 */
	public void pgpuploadslot(String email, String courseid) throws Exception {
		try {
			String[] ObjectArraychoosefile = obj.getObjectArray("Upload_faculty_slot");
			String[] ObjectArraysubmit = obj.getObjectArray("SubmitFacultySlot");
			File inputFile = new File(workingDir + "/driver/pgp_slot_upload.csv");
			Thread.sleep(2000);

			WebElement element = getWebElement(ObjectArraychoosefile[3], ObjectArraychoosefile[2]);

			csvreaderpgpslot(email, courseid, inputFile);
			Thread.sleep(2000);

			element.sendKeys(workingDir + "/driver/pgp_slot_upload.csv");

			Thread.sleep(2000);

			click(ObjectArraysubmit[3], ObjectArraysubmit[2]);

			Thread.sleep(2000);

		} catch (Exception e) {
			fail("pgp bulk upload upload got failed" + e, true);
		}
	}

	/**
	 * @author aishwarya
	 * @param email
	 * @param courseid
	 * @param inputfile
	 * @throws Exception
	 */
	public void csvreaderpgpslot(String email, String courseid, File inputfile) throws Exception {
		try {
			// Read existing file
			CSVReader reader = new CSVReader(new FileReader(inputfile), ',');
			List<String[]> csvBody = reader.readAll();
			Thread.sleep(2000);
			csvBody.get(1)[1] = courseid;
			csvBody.get(1)[0] = email;
			Date dt = new Date();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Calendar c = Calendar.getInstance();
			c.setTime(dt);
			c.add(Calendar.DATE, 1);
			dt = c.getTime();
			csvBody.get(1)[2] = dateFormat.format(dt);
			c.add(Calendar.HOUR, 1);
			dt = c.getTime();
			csvBody.get(1)[3] = dateFormat.format(dt);
			TestData.put("email", csvBody.get(1)[0]);
			TestData.put("courseid", csvBody.get(1)[1]);
			reader.close();
			// Write to CSV file which is open
			CSVWriter writer = new CSVWriter(new FileWriter(inputfile), ',');
			writer.writeAll(csvBody);
			Thread.sleep(1000);
			writer.flush();
			writer.close();
		} catch (Exception e) {
			fail("csv reader for pgp slot book func got failed" + e, true);
		}
	}

	/**
	 * author aishwarya params email
	 * 
	 * @throws Exception
	 */
	public void pgp_book_slot(String email, String course) throws Exception {
		try {
			String SelectSlot[] = obj.getObjectArray("SelectSlot");
			String SelectSlotSubmit[] = obj.getObjectArray("SelectSlotSubmit");
			String SelectSlotSuccess[] = obj.getObjectArray("SelectSlotSuccess");
			if (Constants.base_url.contains("stage")) {
				loadUrl(Constants.base_url.replace("admin", "ecom") + "/post-graduate/admission-process?courseId="
						+ course + "&email=" + email);
			} else {
				loadUrl(Constants.base_url.replace("admin", "com") + "/post-graduate/admission-process?courseId="
						+ course + "&email=" + email);
			}
			Thread.sleep(Constants.general_wait_time);
			click(SelectSlot[3], SelectSlot[2]);
			Thread.sleep(2000);
			click(SelectSlotSubmit[3], SelectSlotSubmit[2]);
			Thread.sleep(Constants.general_wait_time);
			waitUntilElementVisible(SelectSlotSuccess[3], SelectSlotSuccess[2], Constants.general_wait_time);
		} catch (Exception e) {
			fail("Failed to book pgp slot   " + e, true);
		}
	}

	/**
	 * @author sowmya
	 * @throws Exception
	 */
	public void makePaymentThroughRazorPayEMI() throws Exception {
		try {
			String[] ObjectArraypayctafooter = obj.getObjectArray("RPEMI_paycard");
			String[] ObjectArraypayskipcard = obj.getObjectArray("RPEMI_skipcard");
			String[] ObjectArraypaycardno = obj.getObjectArray("RPEMI_cardno");
			String[] ObjectArrayexpiry = obj.getObjectArray("RPEMI_expiry");
			String[] ObjectArraycvv = obj.getObjectArray("RPEMI_cvv");
			String[] ObjectArrayinrpay = obj.getObjectArray("RPEMI_inr");
			String[] ObjectArraycard = obj.getObjectArray("RPEMI_card");

			Thread.sleep(2000);
			ObjectArray = obj.getObjectArray("Payment_selection_frame");
			WebElement Object = getElement(ObjectArray[3], ObjectArray[2]);
			switchToIFrame("webelement", "", Object);
//			ObjectArray = obj.getObjectArray("Payment_method_selection_screen");
//			waitUntilElementVisible(ObjectArray[3], ObjectArray[2], Constants.explicit_wait_time);

			String originalHandle = driver.getWindowHandle();
			Thread.sleep(3000);
			if (checkElementExist(ObjectArraycard[3], ObjectArraycard[2])) {
				click(ObjectArraycard[3], ObjectArraycard[2]);
				Thread.sleep(1000);
			}
			if (checkElementExist(ObjectArraypayctafooter[3], ObjectArraypayctafooter[2])) {
				click(ObjectArraypayctafooter[3], ObjectArraypayctafooter[2]);
				Thread.sleep(1000);
			}
			Thread.sleep(1000);
			if (checkElementExist(ObjectArraypayskipcard[3], ObjectArraypayskipcard[2])) {
				click(ObjectArraypayskipcard[3], ObjectArraypayskipcard[2]);
				Thread.sleep(1000);
			}
			click(ObjectArraypaycardno[3], ObjectArraypaycardno[2]);
			type(ObjectArraypaycardno[3], ObjectArraypaycardno[2], "5104015555555558");
			type(ObjectArrayexpiry[3], ObjectArrayexpiry[2], "1223");

			type(ObjectArraycvv[3], ObjectArraycvv[2], "223");

			click(ObjectArrayinrpay[3], ObjectArrayinrpay[2]);

			click(ObjectArraypayctafooter[3], ObjectArraypayctafooter[2]);

			click(ObjectArraypayskipcard[3], ObjectArraypayskipcard[2]);

//			ObjectArray = obj.getObjectArray("Payment_button");
//			mouseOver(ObjectArray[3], ObjectArray[2]);
//			click(ObjectArray[3], ObjectArray[2]);
			Thread.sleep(1000);
			newTab();
			Thread.sleep(1000);
			String[] successClick = obj.getObjectArray("Payment_success_button_razorpay");
			waitUntilElementClickable(successClick[3], successClick[2], 4000);
			scrollIntoView(successClick[3], successClick[2]);
			Thread.sleep(3000);
			click(successClick[3], successClick[2]);
			Thread.sleep(4000);
			driver.switchTo().window(originalHandle);
			Thread.sleep(1000);
		} catch (Exception e) {
			fail("Unable to make payment through razor pay" + e, true);
		}
	}

	/**
	 * @author sowmya
	 * @info
	 */
	public void makePaymentThroughPayPalemi() throws Exception {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			String[] ObjectArrayPaypalNext = obj.getObjectArray("Paypal_Next");
			String[] ObjectArrayPaypalPaynow = obj.getObjectArray("Paypal_paynowbutton");
			String[] ObjectArrayPaypalemico = obj.getObjectArray("Paypalemi_con");
			String[] ObjectArraypaypalemiagree = obj.getObjectArray("Paypalemi_agree");

			String ParentWindow = driver.getWindowHandle();
			Set<String> WindowHandles = driver.getWindowHandles();
			Iterator<String> it = WindowHandles.iterator();
			while (it.hasNext()) {
				String ChildWindow = it.next().toString();
				if (!ChildWindow.equals(ParentWindow)) {
					driver.switchTo().window(ChildWindow);
					break;
				}
			}
			ObjectArray = obj.getObjectArray("Paypal_email");
			if (!checkElementExist(ObjectArray[3], ObjectArray[2])) {
				refresh();
				Thread.sleep(1000);

			}
			clearDefaultText(ObjectArray[3], ObjectArray[2]);
			click(ObjectArray[3], ObjectArray[2]);
			type(ObjectArray[3], ObjectArray[2], Constants.paypal_email);
			if (checkElementExist(ObjectArrayPaypalNext[3], ObjectArrayPaypalNext[2])) {
				click(ObjectArrayPaypalNext[3], ObjectArrayPaypalNext[2]);
				Thread.sleep(1000);

			}
			Thread.sleep(100);
			ObjectArray = obj.getObjectArray("Paypay_password");
			clearDefaultText(ObjectArray[3], ObjectArray[2]);
			click(ObjectArray[3], ObjectArray[2]);
			type(ObjectArray[3], ObjectArray[2], Constants.paypal_pwd);
			Thread.sleep(1000);
			ObjectArray = obj.getObjectArray("Paypal_loginbutton");
//			click(ObjectArray[3], ObjectArray[2]);
			waitUntilElementClickable(ObjectArray[3], ObjectArray[2], 3000);
			Thread.sleep(1000);
			js.executeScript("arguments[0].click();", getWebElement(ObjectArray[3], ObjectArray[2]));
			ObjectArray = obj.getObjectArray("Paypal_paynowbutton");
			String paynowlocator = ObjectArray[3];
			Thread.sleep(Constants.general_wait_time * 4);
			if (checkElementExist(ObjectArrayPaypalemico[3], ObjectArrayPaypalemico[2])) {
//				driver.manage().window().maximize();
				scrollIntoView(ObjectArrayPaypalemico[3], ObjectArrayPaypalemico[2]);
				waitUntilElementClickable(ObjectArrayPaypalemico[3], ObjectArrayPaypalemico[2], 4000);
				click(ObjectArrayPaypalemico[3], ObjectArrayPaypalemico[2]);
				Thread.sleep(1000);
			}
			Thread.sleep(Constants.general_wait_time * 2);
			if (checkElementExist(ObjectArraypaypalemiagree[3], ObjectArraypaypalemiagree[2])) {
//				driver.manage().window().maximize();
				scrollIntoView(ObjectArraypaypalemiagree[3], ObjectArraypaypalemiagree[2]);
				waitUntilElementClickable(ObjectArraypaypalemiagree[3], ObjectArraypaypalemiagree[2], 4000);
				click(ObjectArraypaypalemiagree[3], ObjectArraypaypalemiagree[2]);
				Thread.sleep(Constants.general_wait_time * 2);
			}

			driver.switchTo().window(ParentWindow);
			driver.switchTo().defaultContent();

			Thread.sleep(1000);
		} catch (Exception e) {
			fail("while making payment got an excpetion" + e, true);
		}

	}

	/**
	 * @author sowmya
	 * @param coursetype
	 * @param course
	 * @param currency
	 * @param batch
	 * @param amount
	 * @param gateway
	 * @param message
	 * @throws Exception
	 */
	public void fillsalesalertform(String coursetype, String course, String currency, String batch, String amount,
			String gateway, String message) throws Exception {
		// TODO Auto-generated method stub
		try {
			String[] ObjectArrayemail = obj.getObjectArray("SalesAlert_email");
			String[] ObjectArraycoursetype = obj.getObjectArray("SalesAlert_coursetype");
			String[] ObjectArraycoursetypeselect = obj.getObjectArray("SalesAlert_coursetype_select");
			String[] ObjectArraycourse = obj.getObjectArray("SalesAlert_course");
			String[] ObjectArraycourseselect = obj.getObjectArray("SalesAlert_course_select");
			String[] ObjectArraybatches = obj.getObjectArray("SalesAlert_batches");
			String[] ObjectArraybatcheselect = obj.getObjectArray("SalesAlert_batches_select");
			String[] ObjectArraycurrency = obj.getObjectArray("SalesAlert_currency");
			String[] ObjectArraycurrencyselect = obj.getObjectArray("SalesAlert_currency_Select");
			String[] ObjectArraystate = obj.getObjectArray("SalesAlert_state");
			String[] ObjectArraystateinput = obj.getObjectArray("SalesAlert_state_input");
			String[] ObjectArraygateway = obj.getObjectArray("SalesAlert_gateway");
			String[] ObjectArraygatewayselect = obj.getObjectArray("SalesAlert_gateway_Select");
			String[] ObjectArrayaddmore = obj.getObjectArray("SalesAlert_addmore");
			String[] ObjectArrayamount = obj.getObjectArray("SalesAlert_amount");
			String[] ObjectArraytrid = obj.getObjectArray("SalesAlert_trid");
			String[] ObjectArraysubmit = obj.getObjectArray("SalesAlert_submit");
			String[] ObjectArraycid = obj.getObjectArray("SalesAlert_cidtext");
			String[] ObjectArraytotalamount = obj.getObjectArray("SalesAlert_totalamount");

			String[] ObjectArrayemiinst = obj.getObjectArray("SalesAlert_emiinst");
			String[] ObjectArrayemistartdate = obj.getObjectArray("SalesAlert_emidate");
			JavascriptExecutor js = (JavascriptExecutor) driver;

			String[] ObjectArrayofflinecity = obj.getObjectArray("SalesAlert_offline_city");
			String[] ObjectArrayofflinecityselect = obj.getObjectArray("SalesAlert_offline_cityselect");
			String[] ObjectArrayofflinebatch = obj.getObjectArray("SalesAlert_offline_batch");
			String[] ObjectArrayofflinebatchselect = obj.getObjectArray("SalesAlert_offline_batch");

			String[] ObjectArraynobatch = obj.getObjectArray("SalesAlert_batch_exist");

			int amtcounter = 1;
			String[] gatewayarray = gateway.split(",");
			TestData.put("currency", currency);

			type(ObjectArrayemail[3], ObjectArrayemail[2], TestData.get("email"));
			Thread.sleep(3000);

			if (isAlertPresent()) {
				refresh();
				Thread.sleep(3000);
				fillsalesalertform(coursetype, course, currency, batch, amount, gateway, message);
			}
			click(ObjectArraytotalamount[3], ObjectArraytotalamount[2]);

			Thread.sleep(3000);

			click(ObjectArraycoursetype[3], ObjectArraycoursetype[2]);

			click(ObjectArraycoursetypeselect[3].replace("$TYPE", coursetype), ObjectArraycoursetypeselect[2]);
			Thread.sleep(2000);
			click(ObjectArraycoursetype[3], ObjectArraycoursetype[2]);

			Thread.sleep(3000);

			click(ObjectArraycourse[3], ObjectArraycourse[2]);

			fetchcontentandselect(course, ObjectArraycourseselect);

			Thread.sleep(2000);

			if (coursetype.contains("Offline")) {
//				click(ObjectArrayofflinecity[3],ObjectArrayofflinecity[2]);
				driver.findElement(By.cssSelector("#CourseAssignmentCityId")).click();
				Thread.sleep(1000);
				click(ObjectArrayofflinecityselect[3], ObjectArrayofflinecityselect[2]);
				Thread.sleep(3000);
				driver.findElement(By.xpath("//*[@id=\"paidBatches\"]")).click();
				js.executeScript("arguments[0].click();", driver.findElement(By.xpath("//*[@id=\"paidBatches\"]")));

				click(ObjectArrayofflinebatchselect[3], ObjectArrayofflinebatchselect[2]);
				Thread.sleep(2000);
			} else {
				click(ObjectArraybatches[3], ObjectArraybatches[2]);
				if (checkElementExist(ObjectArraybatcheselect[3].replace("$BATCH", batch),
						ObjectArraybatcheselect[2])) {
					click(ObjectArraybatcheselect[3].replace("$BATCH", batch), ObjectArraybatcheselect[2]);
				} else if (checkElementExist(ObjectArraynobatch[3], ObjectArraynobatch[2])) {
					click(ObjectArraynobatch[3], ObjectArraynobatch[2]);
				}
			}
			if (isAlertPresent()) {
				refresh();
				Thread.sleep(3000);
				fillsalesalertform(coursetype, course, currency, batch, amount, gateway, message);
			}
			Thread.sleep(2000);

			click(ObjectArraycurrency[3], ObjectArraycurrency[2]);

			fetchcontentandselect(currency, ObjectArraycurrencyselect);
			Thread.sleep(2000);

			if (currency.contains("INR")) {
				click(ObjectArraystate[3], ObjectArraystate[2]);
				Thread.sleep(1000);
				type(ObjectArraystateinput[3], ObjectArraystateinput[2], "Karnataka");
				WebElement elem = getWebElement(ObjectArraystateinput[3], ObjectArraystateinput[2]);
				elem.sendKeys(Keys.RETURN);
				Thread.sleep(1000);

			}
			type(ObjectArraytotalamount[3], ObjectArraytotalamount[2], amount);
			Thread.sleep(1000);

			for (int counter = 0; counter < gatewayarray.length; counter++) {
				if (counter > 0) {
					click(ObjectArrayaddmore[3], ObjectArrayaddmore[2]);
					Thread.sleep(2000);
				}
				scrollIntoView(ObjectArraysubmit[3], ObjectArraysubmit[2]);

				Thread.sleep(2000);
				List<WebElement> elementgateway = driver.findElements(
						By.xpath("//tbody[@class=\"gateway-table-body\"]/tr/td/select[@name=\"paymentModes\"]"));

				elementgateway.get(counter).click();
				int amountcal = Integer.valueOf(amount) / gatewayarray.length;
				TestData.put("finalprice", String.valueOf(amountcal));

				type(ObjectArrayamount[3].replace("$AMTCTR", String.valueOf(amtcounter)).replace("$TRCTR", "2"),
						ObjectArrayamount[2], String.valueOf(amountcal));

				if (gateway.contains("emi")) {
					selectByVisibleText(ObjectArrayemiinst[3], ObjectArrayemiinst[2], "visibletext", "2");
					if (!gateway.contains("Paypal")) {
						selectByVisibleText(ObjectArrayemistartdate[3], ObjectArrayemistartdate[2], "index", "2");
					}

				} else {
					type(ObjectArrayamount[3].replace("$AMTCTR", String.valueOf(amtcounter)).replace("$TRCTR", "3"),
							ObjectArrayamount[2], getTridforGateways(gatewayarray[counter]));
				}
				elementgateway = driver.findElements(
						By.xpath("//tbody[@class=\"gateway-table-body\"]/tr/td/select[@name=\"paymentModes\"]"));
				mouseOver(elementgateway.get(counter));
				elementgateway.get(counter).click();
				Thread.sleep(3000);
				List<WebElement> elementgatewayselect = getWebElements(
						ObjectArraygatewayselect[3].replace("$GATEWAY", gatewayarray[counter]),
						ObjectArraygatewayselect[2]);
				elementgatewayselect.get(counter).click();
				Thread.sleep(2000);
				elementgateway.get(counter).click();

				amtcounter = amtcounter + 2;
			}
			WebElement submit = getWebElement(ObjectArraysubmit[3], ObjectArraysubmit[2]);

			if (coursetype.contains("Offline")) {
				driver.findElement(By.xpath("//*[@id=\"paidBatches\"]")).click();
				js.executeScript("arguments[0].click();", driver.findElement(By.xpath("//*[@id=\"paidBatches\"]")));
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id=\"paidBatches\"]/option[2]")).click();
				js.executeScript("arguments[0].click();",
						driver.findElement(By.xpath("//*[@id=\"paidBatches\"]/option[2]")));

				click(ObjectArrayofflinebatchselect[3], ObjectArrayofflinebatchselect[2]);
				Thread.sleep(3000);
			}
			scrollIntoView(ObjectArraysubmit[3], ObjectArraysubmit[2]);
			driver.findElement(By.cssSelector("#submit")).click();

			Thread.sleep(12000);
			if (driver.switchTo().alert().getText().contains(message)) {
				driver.switchTo().alert().accept();
			} else {

				driver.switchTo().alert().accept();
				fail("failed to fill sales alert form", true);
			}

		} catch (Exception e) {
			fail("failed to fill sales alert form" + e, true);
		}

	}

	/**
	 * @author sowmya
	 * @param gateway
	 * @return
	 * @throws Exception
	 */
	public String getTridforGateways(String gateway) throws Exception {
		String trid = null;
		try {
			switch (gateway) {
			case "Razorpay":
				trid = "pay_" + Constants.prefix1;
				break;

			case "Liquiloan":
				trid = "CL000" + Constants.prefix1;
				break;
			default:
				trid = Constants.prefix1 + Constants.prefix2;
				break;
			}
		} catch (Exception e) {
			fail("failed to get trid" + e, true);
		}
		return trid;
	}

	/**
	 * @author sowmya
	 * @param email
	 * @param course
	 * @param batch
	 * @param reason
	 * @throws Exception
	 */
	public void courseassignmentform(String email, String course, String batch, String reason) throws Exception {
		// TODO Auto-generated method stub
		try {
			String[] ObjectArrayemail = obj.getObjectArray("CA_email");
			String[] ObjectArraycourse = obj.getObjectArray("CA_course");
			String[] ObjectArraycourseselect = obj.getObjectArray("CA_courseselect");
			String[] ObjectArraycoursegroup = obj.getObjectArray("CA_coursegroup");
			String[] ObjectArraycoursebatch = obj.getObjectArray("CA_batch");
			String[] ObjectArraycoursereason = obj.getObjectArray("CA_reason");
			String[] ObjectArraycoursereasonselect = obj.getObjectArray("CA_reasonselect");
			String[] ObjectArraysubmit = obj.getObjectArray("CA_submit");
			String[] ObjectArraymessage = obj.getObjectArray("CA_message");

			type(ObjectArrayemail[3], ObjectArrayemail[2], email);
			Thread.sleep(2000);
			click(ObjectArraycourse[3], ObjectArraycourse[2]);
			Thread.sleep(2000);
			fetchcontentandselect(course, ObjectArraycourseselect);
			Thread.sleep(2000);
			selectByVisibleText(ObjectArraycoursebatch[3], ObjectArraycoursebatch[2], batch, "visibletext");
			Thread.sleep(1000);
//			click(ObjectArraycoursereason[3],ObjectArraycoursereason[2]);
//			((JavascriptExecutor) driver).executeScript("arguments[0].click();",
//					getWebElement(ObjectArraycoursereasonselect[3], ObjectArraycoursereasonselect[2]));
			selectByVisibleText(ObjectArraycoursereason[3], ObjectArraycoursereason[2], reason, "visibletext");
			Thread.sleep(1000);
			click(ObjectArraysubmit[3], ObjectArraysubmit[2]);
			Thread.sleep(5000);
			Assert.assertEquals(getText(ObjectArraymessage[3], ObjectArraymessage[2]), "Course Assigned To User.");
			Thread.sleep(1000);
		} catch (Exception e) {
			fail("course assignment form got failed" + e, true);
		}
	}

	/**
	 * @author sowmya
	 * @param value
	 * @param cases
	 * @param coursetype
	 * @throws Exception
	 */
	public void customdiscountadminform(String value, String cases, String coursetype) throws Exception {
		// TODO Auto-generated method stub
		try {
			String[] ObjectArraydefault = obj.getObjectArray("course");
			String[] ObjectArraycoursecopy = obj.getObjectArray("course_copy");
			String[] ObjectArrayexIN = obj.getObjectArray("course_ex_IN");
			String[] ObjectArrayexUS = obj.getObjectArray("course_ex_US");
			String[] ObjectArrayexROW = obj.getObjectArray("course_ex_ROW");
			String[] ObjectArraynewIN = obj.getObjectArray("course_new_IN");
			String[] ObjectArraynewUS = obj.getObjectArray("course_new_US");
			String[] ObjectArraynewROW = obj.getObjectArray("course_new_ROW");
			String[] ObjectArraysubmit = obj.getObjectArray("submit");
			String expectedvalue = null;

			String exvalue = driver.findElement(By.cssSelector(ObjectArrayexIN[3].replace("$TYPE", coursetype)))
					.getAttribute("value");

			String newvalue = driver.findElement(By.cssSelector(ObjectArraynewIN[3].replace("$TYPE", coursetype)))
					.getAttribute("value");

			String defaultvalue = driver.findElement(By.cssSelector(ObjectArraydefault[3].replace("$TYPE", coursetype)))
					.getAttribute("value");

			switch (cases) {
			case "check Copy All button":
				expectedvalue = "{\"default\":\"" + value + "\",\"existing\":{\"ex_india\":\"" + value
						+ "\",\"ex_usa\":\"" + value + "\",\"ex_row\":\"" + value + "\"},\"new\":{\"new_india\":\""
						+ value + "\",\"new_usa\":\"" + value + "\",\"new_row\":\"" + value + "\"}}";
				clearDefaultText(ObjectArraydefault[3].replace("$TYPE", coursetype), ObjectArraydefault[2]);
				type(ObjectArraydefault[3].replace("$TYPE", coursetype), ObjectArraydefault[2], value);
				click(ObjectArraycoursecopy[3].replace("$TYPE", coursetype), ObjectArraycoursecopy[2]);
				TestData.put("value", value);
				break;

			case "check Discount update for Existing User":
				expectedvalue = "{\"default\":\"" + defaultvalue + "\",\"existing\":{\"ex_india\":\"" + value
						+ "\",\"ex_usa\":\"" + value + "\",\"ex_row\":\"" + value + "\"},\"new\":{\"new_india\":\""
						+ newvalue + "\",\"new_usa\":\"" + newvalue + "\",\"new_row\":\"" + newvalue + "\"}}";
				clearDefaultText(ObjectArrayexIN[3].replace("$TYPE", coursetype), ObjectArrayexIN[2]);
				type(ObjectArrayexIN[3].replace("$TYPE", coursetype), ObjectArrayexIN[2], value);
				clearDefaultText(ObjectArrayexUS[3].replace("$TYPE", coursetype), ObjectArrayexUS[2]);
				type(ObjectArrayexUS[3].replace("$TYPE", coursetype), ObjectArrayexUS[2], value);
				clearDefaultText(ObjectArrayexROW[3].replace("$TYPE", coursetype), ObjectArrayexROW[2]);
				type(ObjectArrayexROW[3].replace("$TYPE", coursetype), ObjectArrayexROW[2], value);
				break;

			case "check Discount update for New User":
				expectedvalue = "{\"default\":\"" + defaultvalue + "\",\"existing\":{\"ex_india\":\"" + exvalue
						+ "\",\"ex_usa\":\"" + exvalue + "\",\"ex_row\":\"" + exvalue + "\"},\"new\":{\"new_india\":\""
						+ value + "\",\"new_usa\":\"" + value + "\",\"new_row\":\"" + value + "\"}}";
				clearDefaultText(ObjectArraynewIN[3].replace("$TYPE", coursetype), ObjectArraynewIN[2]);
				type(ObjectArraynewIN[3].replace("$TYPE", coursetype), ObjectArraynewIN[2], value);
				clearDefaultText(ObjectArraynewUS[3].replace("$TYPE", coursetype), ObjectArraynewUS[2]);
				type(ObjectArraynewUS[3].replace("$TYPE", coursetype), ObjectArraynewUS[2], value);
				clearDefaultText(ObjectArraynewROW[3].replace("$TYPE", coursetype), ObjectArraynewROW[2]);
				type(ObjectArraynewROW[3].replace("$TYPE", coursetype), ObjectArraynewROW[2], value);
				break;

			default:
				fail("not valid case", true);
			}

			Thread.sleep(1000);
			click(ObjectArraysubmit[3].replace("$TYPE", coursetype), ObjectArraysubmit[2]);
			Thread.sleep(3000);

			String actualvalue;
			actualvalue = DBUtils.fetchsettingtable(coursetype);

			Thread.sleep(3000);
			driver.switchTo().alert().accept();
			Thread.sleep(1000);
			Assert.assertEquals(expectedvalue, actualvalue);

		} catch (Exception e) {
			fail("course assignment form got failed" + e, true);
		}

	}

	public void checkExtendValidityFunction(String validity, String message) throws Exception {
		// TODO Auto-generated method stub
		try {
			String[] ObjectArrayurlparm = obj.getObjectArray("ExtendCL_urlparam");
			String[] ObjectArrayvalidity = obj.getObjectArray("ExtendCL_validity");
			String[] ObjectArrayvalidityselect = obj.getObjectArray("ExtendCL_validityselect");
			String[] ObjectArraysubmit = obj.getObjectArray("ExtendCL_submit");
			String[] ObjectArraymessage = obj.getObjectArray("ExtendCL_message");

			TestData.put("url_param", DBUtils.getUrlParam(TestData.get("email")));
			type(ObjectArrayurlparm[3], ObjectArrayurlparm[2], TestData.get("url_param"));
			Thread.sleep(2000);
			click(ObjectArrayvalidity[3], ObjectArrayvalidity[2]);
			Thread.sleep(2000);
			click(ObjectArrayvalidityselect[3].replace("$TIME", validity), ObjectArrayvalidityselect[2]);
			TestData.put("time", validity);
			Thread.sleep(2000);
			click(ObjectArraysubmit[3], ObjectArraysubmit[2]);
			Thread.sleep(2000);
			if (getWebElement(ObjectArraymessage[3], ObjectArraymessage[2]).getAttribute("innerText")
					.contains(message)) {
				pass("Link Validity Funcion is working fine");
			}

		} catch (Exception e) {
			fail("Extend Custom link validity function got failed" + e, true);
		}
	}

	/**
	 * author aishwarya params name params contact number India params contact
	 * number US params email
	 * 
	 * @throws Exception
	 */
	public void addbu(String name, String contactin, String contactus, String email) throws Exception {
		try {
			String AddBU[] = obj.getObjectArray("AddBU");
			String AddBUName[] = obj.getObjectArray("AddBUName");
			String AddBUPhIndia[] = obj.getObjectArray("AddBUPhIndia");
			String AddBUPhUS[] = obj.getObjectArray("AddBUPhUS");
			String AddBUEmail[] = obj.getObjectArray("AddBUEmail");
			String AddBUSubmit[] = obj.getObjectArray("AddBUSubmit");
			Thread.sleep(Constants.general_wait_time);
			scrollIntoViewAndClick(AddBU[3], AddBU[2]);
			Thread.sleep(2000);
			click(AddBUName[3], AddBUName[2]);
			type(AddBUName[3], AddBUName[2], "BU New");
			click(AddBUPhIndia[3], AddBUPhIndia[2]);
			type(AddBUPhIndia[3], AddBUPhIndia[2], contactin);
			click(AddBUPhUS[3], AddBUPhUS[2]);
			type(AddBUPhUS[3], AddBUPhUS[2], contactus);
			click(AddBUEmail[3], AddBUEmail[2]);
			type(AddBUEmail[3], AddBUEmail[2], email);
			click(AddBUSubmit[3], AddBUSubmit[2]);
			Thread.sleep(Constants.general_wait_time);
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			fail("Failed to add bu   " + e, true);
		}
	}

	/**
	 * author aishwarya
	 * 
	 * @throws Exception
	 */
	public void editbu() throws Exception {
		try {
			Thread.sleep(Constants.general_wait_time);
			String BUNewExistence[] = obj.getObjectArray("BUNewExistence");
			String BUNewEdit[] = obj.getObjectArray("BUNewEdit");
			String BUNewEditPhIn[] = obj.getObjectArray("BUNewEditPhIn");
			String BUNewEditPhUs[] = obj.getObjectArray("BUNewEditPhUs");
			String BUNewEditPhEmail[] = obj.getObjectArray("BUNewEditPhEmail");
			String BuNewEditSave[] = obj.getObjectArray("BuNewEditSave");
			Thread.sleep(Constants.general_wait_time);
			waitUntilElementVisible(BUNewExistence[3], BUNewExistence[2], Constants.general_wait_time);
			click(BUNewEdit[3], BUNewEdit[2]);
			click(BUNewEditPhIn[3], BUNewEditPhIn[2]);
			clearDefaultText(BUNewEditPhIn[3], BUNewEditPhIn[2]);
			type(BUNewEditPhIn[3], BUNewEditPhIn[2], "+91-0987654321");
			click(BUNewEditPhUs[3], BUNewEditPhUs[2]);
			clearDefaultText(BUNewEditPhUs[3], BUNewEditPhUs[2]);
			type(BUNewEditPhUs[3], BUNewEditPhUs[2], "+1-9999999999");
			click(BUNewEditPhEmail[3], BUNewEditPhEmail[2]);
			clearDefaultText(BUNewEditPhEmail[3], BUNewEditPhEmail[2]);
			type(BUNewEditPhEmail[3], BUNewEditPhEmail[2], "checkbuedit@tech.edureka.in");
			click(BuNewEditSave[3], BuNewEditSave[2]);
			Thread.sleep(3000);
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			fail("Failed to edit bu   " + e, true);
		}
	}

	/**
	 * @author aishwarya
	 * @param email
	 * @param course
	 * @param currency
	 * @param gateway
	 * @throws Exception
	 */
	public void pgp_interview_payment(String email, String course, String currency, String gateway) throws Exception {
		try {
			String[] PgpIntPay = obj.getObjectArray("PgpIntPay");
			String[] IntPayEmail = obj.getObjectArray("IntPayEmail");
			String[] InPayCourse = obj.getObjectArray("InPayCourse");
			String[] InPayCurrency = obj.getObjectArray("InPayCurrency");
			String[] InPayGateway = obj.getObjectArray("InPayGateway");
			String[] InPaySubmit = obj.getObjectArray("InPaySubmit");
			Thread.sleep(Constants.general_wait_time);
			click(PgpIntPay[3], PgpIntPay[2]);
			click(IntPayEmail[3], IntPayEmail[2]);
			type(IntPayEmail[3], IntPayEmail[2], email);
			Thread.sleep(5000);
			click(InPayCourse[3], InPayCourse[2]);
			Select drpCourse = new Select(driver.findElement(By.xpath(InPayCourse[3])));
			drpCourse.selectByVisibleText(course);
			Thread.sleep(2000);
			click(InPayCurrency[3], InPayCurrency[2]);
			Select drpCourse1 = new Select(driver.findElement(By.cssSelector(InPayCurrency[3])));
			drpCourse1.selectByVisibleText(currency);
			if (currency.equalsIgnoreCase("INR")) {
				Thread.sleep(2000);
				click(InPayGateway[3], InPayGateway[2]);
				Select drpCourse2 = new Select(driver.findElement(By.cssSelector(InPayGateway[3])));
				drpCourse2.selectByVisibleText(gateway);
				Thread.sleep(2000);
			}
				click(InPaySubmit[3], InPaySubmit[2]);
			Thread.sleep(9000);
			driver.switchTo().alert().accept();
			WebElement element = driver.findElement(By.xpath("//input[@id='paymentLink']"));
			String elementval = element.getAttribute("value");
			loadUrl(elementval);
		} catch (Exception e) {
			fail("Pgp Interview fee payment failed" + e, true);
		}
	}

	/**
	 * @author sowmya
	 * @param email
	 * @param course
	 * @param currency
	 * @param gateway
	 * @param paidintfee
	 * @param alertmessage
	 * @param amount
	 * @throws Exception
	 */
	public void fillPgpUpfrontSalesAlertForm(String email, String course, String currency, String gateway,
			boolean paidintfee, String alertmessage, String amount) throws Exception {
		// TODO Auto-generated method stub
		try {
			String[] ObjectArrayupfrontbutton = obj.getObjectArray("pgpUpfront_button");
			String[] ObjectArrayemail = obj.getObjectArray("pgpUpfront_email");
			String[] ObjectArraycourse = obj.getObjectArray("pgpUpfront_course");
			String[] ObjectArraycourseselect = obj.getObjectArray("pgpUpfront_course_select");
			String[] ObjectArraysale = obj.getObjectArray("pgpUpfront_sale");
			String[] ObjectArraysaleselect = obj.getObjectArray("pgpUpfront_sale_select");
			String[] ObjectArraycurrency = obj.getObjectArray("pgpUpfront_currency");
			String[] ObjectArraycurrencyselect = obj.getObjectArray("pgpUpfront_currencyselect");
			String[] ObjectArrayamount = obj.getObjectArray("pgpUpfront_amount");
			String[] ObjectArraypaymentmode = obj.getObjectArray("pgpUpfront_paymentmode");
			String[] ObjectArraypaymentmodeselect = obj.getObjectArray("pgpUpfront_paymentmode_select");
			String[] ObjectArraytrid = obj.getObjectArray("pgpUpfront_trid");
			String[] ObjectArraycountry = obj.getObjectArray("pgpUpfront_country");
			String[] ObjectArraycountrysearch = obj.getObjectArray("pgpUpfront_country_search");

			String[] ObjectArraybatch = obj.getObjectArray("pgpUpfront_batch");
			String[] ObjectArraybatchselect = obj.getObjectArray("pgpUpfront_batchselect");
			String[] ObjectArraydownpayment = obj.getObjectArray("pgpUpfront_downpayment");
			String[] ObjectArrayintpay = obj.getObjectArray("pgpUpfront_intpayment");
			String[] ObjectArraystate = obj.getObjectArray("pgpUpfront_state");
			String[] ObjectArraycode = obj.getObjectArray("pgpUpfront_postalcode");
			String[] ObjectArraysubmit = obj.getObjectArray("pgpUpfront_submit");

			Thread.sleep(2000);
			click(ObjectArrayupfrontbutton[3], ObjectArrayupfrontbutton[2]);
			Thread.sleep(Constants.explicit_wait_time);

			type(ObjectArrayemail[3], ObjectArrayemail[2], email);

			click(ObjectArraycourse[3], ObjectArraycourse[2]);
			fetchcontentandselect(course, ObjectArraycourseselect);
			Thread.sleep(3000);
			click(ObjectArraysale[3], ObjectArraysale[2]);
			click(ObjectArraysaleselect[3], ObjectArraysaleselect[2]);
			click(ObjectArraycurrency[3], ObjectArraycurrency[2]);
			click(ObjectArraycurrencyselect[3].replace("$CURRENCY", currency), ObjectArraycurrencyselect[2]);

//			fetchcontentandselect(currency,ObjectArraycurrencyselect);
			Thread.sleep(2000);
			if (paidintfee) {
				if (getText(ObjectArrayintpay[3], ObjectArrayintpay[2]) != null) {
					pass("Interview fee is fetching properly");
				}
			} else {
				driver.switchTo().alert().accept();
				Thread.sleep(5000);
			}

			click(ObjectArraybatch[3], ObjectArraybatch[2]);
			Thread.sleep(3000);

			click(ObjectArraybatchselect[3], ObjectArraybatchselect[2]);

			type(ObjectArrayamount[3], ObjectArrayamount[2], amount);

			Thread.sleep(3000);
			int upfront = Integer.valueOf(amount) / 2;
			float originalprice = 1;
			TestData.put("finalprice", String.valueOf(upfront));
			if (currency.equalsIgnoreCase("INR")) {
				originalprice = (float) (upfront * 0.8475);
			} else {
				originalprice = upfront;
			}
			TestData.put("originalprice", String.valueOf((int) Math.floor(originalprice)));

			type(ObjectArraydownpayment[3], ObjectArraydownpayment[2], String.valueOf(upfront));

			click(ObjectArraypaymentmode[3], ObjectArraypaymentmode[2]);
			click(ObjectArraypaymentmodeselect[3].replace("$MODE", gateway), ObjectArraypaymentmodeselect[2]);

			type(ObjectArraytrid[3], ObjectArraytrid[2], getTridforGateways(gateway));

			click(ObjectArraycountry[3], ObjectArraycountry[2]);

			type(ObjectArraycountrysearch[3], ObjectArraycountrysearch[2], "india");
			WebElement elem = getWebElement(ObjectArraycountrysearch[3], ObjectArraycountrysearch[2]);
			elem.sendKeys(Keys.ENTER);

			if (currency.equalsIgnoreCase("INR")) {
				type(ObjectArraystate[3], ObjectArraystate[2], "test state");
				type(ObjectArraycode[3], ObjectArraycode[2], "test code");

			}
			click(ObjectArraysubmit[3], ObjectArraysubmit[2]);

			Thread.sleep(3000);
			if (driver.switchTo().alert().getText().contains(alertmessage)) {
				driver.switchTo().alert().accept();
				Thread.sleep(3000);
			} else {
				fail("pgp upfront sales alert has some issue " + driver.switchTo().alert().getText(), true);
			}
		} catch (Exception e) {
			fail("PGP upfront sales alert functionality is not working " + e, true);
		}
	}

	/**
	 * @author sowmya
	 * @param email
	 * @param course
	 * @param amount
	 * @param validity
	 * @param alertmessage
	 * @throws Exception
	 */
	public void fillPgpRPsubForm(String email, String course, String amount, String validity, String alertmessage)
			throws Exception {
		// TODO Auto-generated method stub
		try {
			String[] ObjectArrayemail = obj.getObjectArray("rpsub_email");
			String[] ObjectArraycourse = obj.getObjectArray("rpsub_course");
			String[] ObjectArraycourseselect = obj.getObjectArray("rpsub_courseselect");
			String[] ObjectArrayamount = obj.getObjectArray("rpsub_amount");
			String[] ObjectArrayinitializeamount = obj.getObjectArray("rpsub_initialize_amount");
			String[] ObjectArrayemi = obj.getObjectArray("rpsub_emi");
			String[] ObjectArrayemivalue = obj.getObjectArray("rpsub_per_month_amt");
			String[] ObjectArrayemidate = obj.getObjectArray("rpsub_emidate");
			String[] ObjectArrayvalidity = obj.getObjectArray("rpsub_validity");
			String[] ObjectArraysubmit = obj.getObjectArray("rpsub_submit");

			TestData.put("courseassignment", "NO");
			type(ObjectArrayemail[3], ObjectArrayemail[2], email);
			click(ObjectArraycourse[3], ObjectArraycourse[2]);
			Thread.sleep(3000);
			click(ObjectArraycourseselect[3].replace("$COURSE", course), ObjectArraycourseselect[2]);
//			fetchcontentandselect(course,ObjectArraycourseselect);
			Thread.sleep(3000);
			click(ObjectArraycourse[3], ObjectArraycourse[2]);
			Thread.sleep(3000);
			type(ObjectArrayamount[3], ObjectArrayamount[2], amount);
			int amt = Integer.valueOf(amount) / 2;
			float originalvalue = Math.round((float) (amt * (0.847)));
			float gstvalue = Math.round(amt - (amt * (0.847)));
			BigDecimal b1 = new BigDecimal(originalvalue);
			BigDecimal b2 = new BigDecimal(gstvalue);

			TestData.put("upfront", String.valueOf(amt));
			TestData.put("finalprice", String.valueOf(amt));
			TestData.put("servicetax", String.valueOf(b2.stripTrailingZeros()));
			TestData.put("originalprice", String.valueOf(b1.stripTrailingZeros()));

			type(ObjectArrayinitializeamount[3], ObjectArrayinitializeamount[2], String.valueOf(amt));
			Thread.sleep(1000);
			selectByVisibleText(ObjectArrayemi[3], ObjectArrayemi[2], "2", "index");
			Thread.sleep(1000);

			selectByVisibleText(ObjectArrayemidate[3], ObjectArrayemidate[2], "2", "index");
			Thread.sleep(1000);

			selectByVisibleText(ObjectArrayvalidity[3], ObjectArrayvalidity[2], "45", "value");
			Thread.sleep(1000);

			TestData.put("emiamt", getWebElement(ObjectArrayemivalue[3], ObjectArrayemivalue[2])
					.getAttribute("innerText").replace("Rs.", "").replace(" / Month", ""));

			click(ObjectArraysubmit[3], ObjectArraysubmit[2]);
			Thread.sleep(6000);

			if (driver.switchTo().alert().getText().contains(alertmessage)) {
				driver.switchTo().alert().accept();
				Thread.sleep(4000);
			} else if (driver.switchTo().alert().getText().contains("Alert text : Error!!! No Data!!")) {
				refresh();
				Thread.sleep(4000);
				fillPgpRPsubForm(email, course, amount, validity, alertmessage);
			}
			verifypreviewandsendmailforrpemi();

		} catch (Exception e) {
			fail("PGP RP subscription form functionality is not working " + e, true);
		}
	}

	/**
	 * @author sowmya
	 * @throws Exception
	 */
	public void verifypreviewandsendmailforrpemi() throws Exception {
		try {
			String[] ObjectArraypreview = obj.getObjectArray("rpsub_preview");
			String[] ObjectArraysendmail = obj.getObjectArray("rpsub_sendmail");
			String windowhandle = null;
			String[] addcourse = TestData.get("multiplepaymentvalue").split(",");

//				String originalHandle = driver.getWindowHandle();
			scrollIntoView(ObjectArraypreview[3], ObjectArraypreview[2]);
			Thread.sleep(2000);
			click(ObjectArraysendmail[3], ObjectArraysendmail[2]);
			WebDriverWait wait = new WebDriverWait(driver, 15);
			wait.until(ExpectedConditions.alertIsPresent());
			Alert simpleAlert = driver.switchTo().alert();
			String actualalertText = simpleAlert.getText();

			pass("Send mail alert Text->" + actualalertText);
			Thread.sleep(2000);
			driver.switchTo().alert().accept();
			Thread.sleep(2000);
			scrollIntoView(ObjectArraypreview[3], ObjectArraypreview[2]);
			click(ObjectArraypreview[3], ObjectArraypreview[2]);
			Thread.sleep(2000);

//				if (addcourse[0].contains("FALSE")) {
			windowhandle = getWindowHandlesthroughURL("preview");
			driver.switchTo().window(windowhandle);
			calculatediscountandfinalpriceonpreviewforrpemi("Preview");
//				driver.close();

			windowhandle = getWindowHandlesthroughURL("pgp_payments");
			driver.switchTo().window(windowhandle);
			driver.close();

			windowhandle = getWindowHandlesthroughURL("custom_payment");
			driver.switchTo().window(windowhandle);
			Thread.sleep(2000);
			calculatediscountandfinalpriceonpreviewforrpemi("OSP");

//				} else {
//					windowhandle = getWindowHandlesthroughURL("preview");
//					driver.switchTo().window(windowhandle);
//					calculatediscountandfinalpriceonpreview("Preview");
//					String[] ObjectArraypaynow = obj.getObjectArray("Customlink_paynow");
//					click(ObjectArraypaynow[3], ObjectArraypaynow[2]);
//					Thread.sleep(4000);
//					driver.close();
//					Thread.sleep(1000);
//					windowhandle = getWindowHandlesthroughURL("admin_zoho");
//					driver.switchTo().window(windowhandle);
//					driver.close();
//					Thread.sleep(2000);
//					windowhandle = getWindowHandlesthroughURL("custom_payment");
//					driver.switchTo().window(windowhandle);
//					calculatediscountandfinalpriceonpreview("OSP");
//					refresh();
//					Thread.sleep(2000);
//				}

		} catch (Exception e) {
			fail("preview functionality is not working  " + e, true);
		}
	}

	/**
	 * @author sowmya
	 * @param pagename
	 * @throws Exception
	 */
	public void calculatediscountandfinalpriceonpreviewforrpemi(String pagename) throws Exception {
		try {
			String[] ObjectArraycourseprice = null;
			String[] ObjectArrayemiprice = null;
			String[] ObjectArraycoursename = null;
			String[] ObjectArrayupfront = null;
			String[] ObjectArrayeducashvalue = null;
			String[] ObjectArraydiscount = null;
			String[] ObjectArraysavings = null;
			String[] ObjectArrayemisize = null;
			String[] ObjectArraypaynow = null;
			String[] ObjectArrayemiclose = null;
			String Actualsavings = null;
			String Actualcourseprice = null, Actualnetprice = null;
			int grandtotal = 0;
			DBUtils.getCoursegroup(TestData.get("courseid"));
			switch (pagename) {
			case "Preview":
				ObjectArraycourseprice = obj.getObjectArray("rpsub_courseprice");
				ObjectArrayemiprice = obj.getObjectArray("rpsub_emiamount");
				ObjectArraycoursename = obj.getObjectArray("rpsub_coursename");
				ObjectArrayupfront = obj.getObjectArray("rpsub_upfront");
				ObjectArrayeducashvalue = obj.getObjectArray("rpsub_upfront");
				ObjectArraydiscount = obj.getObjectArray("Preview_discount");
				ObjectArrayemisize = obj.getObjectArray("rpsub_emiamount_size");
				ObjectArraypaynow = obj.getObjectArray("rpsub_paynow");
				break;

			case "OSP":
				String locator;
				ObjectArraycourseprice = obj.getObjectArray("rpsub_osp_amount");
				ObjectArraycourseprice[3] = ObjectArraycourseprice[3].replace("$COURSE_GROUP",
						DBUtils.DbutilsTestData.get("coursegroup"));
				ObjectArrayupfront = obj.getObjectArray("rpsub_osp_upfront");
				String[] ObjectArrayemicta = obj.getObjectArray("rpsub_osp_emi_cta");
				ObjectArrayemisize = obj.getObjectArray("rpsub_osp_emi_size");
				ObjectArrayemiprice = obj.getObjectArray("rpsub_osp_emiprize");
				ObjectArrayemiclose = obj.getObjectArray("rpsub_osp_emi_close");
				click(ObjectArrayemicta[3], ObjectArrayemicta[2]);

				break;
			}
			int Expectedsavings = 0, Expectedgst = 0, Expectedcalculatetotal = 0, adddiscount = 0;
			String Actualemi = null, Actualupfrontvalue = null, Actualeducash = null;
			pass("ON " + pagename + " Calculation");
			int pos = 3, length, gstcounter = 3, totalcounter = 2;
			length = getWebElements(ObjectArrayemisize[3], ObjectArrayemisize[2]).size();

//				if (!addcourse[0].equalsIgnoreCase("TRUE")) {
//					length = 1;
//				} else {
//					length = addcourse.length;
//				}
			Actualcourseprice = getText(ObjectArraycourseprice[3], ObjectArraycourseprice[2]);
			TestData.put("courseid" + 0, TestData.get("courseid"));
			scrollIntoView(ObjectArraycourseprice[3].replace("$POS", String.valueOf(pos)), ObjectArraycourseprice[2]);
			for (int i = 1; i <= length; i++) {
				pos = pos + 2;
				gstcounter = gstcounter + i;
				totalcounter = totalcounter + i;

				if (pagename.equalsIgnoreCase("OSP")) {
					i = i + 1;
					if (i >= length) {
						break;
					}
				}

				Actualemi = getWebElement(ObjectArrayemiprice[3].replace("$J", String.valueOf(i)),
						ObjectArrayemiprice[2]).getAttribute("innerText").replaceAll("₹", "").replaceAll(" ", "");
				Assert.assertEquals(TestData.get("emiamt"), Actualemi);
			}
			if (pagename.equalsIgnoreCase("OSP")) {
				scrollIntoView(ObjectArrayemiclose[3], ObjectArrayemiclose[2]);
				Thread.sleep(1000);
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].scrollIntoView(true);",
						getWebElement(ObjectArrayemiclose[3], ObjectArrayemiclose[2]));
				getWebElement(ObjectArrayemiclose[3], ObjectArrayemiclose[2]).click();
			}
			Actualupfrontvalue = getText(ObjectArrayupfront[3], ObjectArrayupfront[2]).replaceAll("₹", "")
					.replaceAll(",", "");
			Assert.assertEquals(TestData.get("upfront"), Actualupfrontvalue);
			if (pagename.contains("Preview")) {
				click(ObjectArraypaynow[3], ObjectArraypaynow[2]);
				Thread.sleep(2000);
			}
		} catch (Exception e) {
			fail("Calculation is wrong on OSP and Preview" + e, true);
		}
	}

	/**
	 * @author sowmya
	 * @param course
	 * @param currency
	 * @param type
	 * @param scholarshipamt
	 * @param discount
	 * @param downpayment
	 * @param gateway
	 * @throws Exception
	 */
	public void fillPgpUpfrontpqymentForm(String course, String currency, String type, String scholarshipamt,
			String discount, String downpayment, String gateway) throws Exception {
		// TODO Auto-generated method stub
		try {
			String[] ObjectArraypgpupfront = obj.getObjectArray("uplink_button");
			String[] ObjectArrayemail = obj.getObjectArray("uplink_email");
			String[] ObjectArraycourse = obj.getObjectArray("uplink_course");
			String[] ObjectArraycourseselect = obj.getObjectArray("uplink_courseselect");
			String[] ObjectArraybatch = obj.getObjectArray("uplink_batch");
			String[] ObjectArraycurrency = obj.getObjectArray("uplink_currency");
			String[] ObjectArraycourseprice = obj.getObjectArray("uplink_courseprice");
			String[] ObjectArrayeducash = obj.getObjectArray("uplink_educash");
			String[] ObjectArrayintfee = obj.getObjectArray("uplink_intfee");
			String[] ObjectArrayshipamt = obj.getObjectArray("uplink_scholarshipamount");
			String[] ObjectArraydiscount = obj.getObjectArray("uplink_discount");
			String[] ObjectArraytotalprice = obj.getObjectArray("uplink_totalprice");
			String[] ObjectArrayfinalprice = obj.getObjectArray("uplink_finalprice");
			String[] ObjectArrayupfront = obj.getObjectArray("uplink_upfront");
			String[] ObjectArrayremaining = obj.getObjectArray("uplink_remaining");
			String[] ObjectArraygateway = obj.getObjectArray("Uplink_gateway");
			String[] ObjectArraygenerate = obj.getObjectArray("Uplink_generate");
			String[] ObjectArraypaymenttype = obj.getObjectArray("Uplink_paymentype");

			String[] ObjectArraypreview = obj.getObjectArray("Uplink_preview");
			String[] ObjectArraypaynow = obj.getObjectArray("Uplink_paynow");
			String windowhandle = null;

			click(ObjectArraypgpupfront[3], ObjectArraypgpupfront[2]);
			Thread.sleep(2000);
			type(ObjectArrayemail[3], ObjectArrayemail[2], replace("$EMAIL_ID"));

			click(ObjectArraycourse[3], ObjectArraycourse[2]);
			click(ObjectArraycourseselect[3].replace("$COURSE", course), ObjectArraycourseselect[2]);
			selectByVisibleText(ObjectArraybatch[3], ObjectArraybatch[2], "1", "index");
			Thread.sleep(2000);
			selectByVisibleText(ObjectArraycurrency[3], ObjectArraycurrency[2], currency, "visibletext");
			Thread.sleep(2000);
			if (isAlertPresent()) {
				if (driver.switchTo().alert().getText().contains("Error occurred while getting the Interview Fees!")) {
					driver.switchTo().alert().accept();
					Thread.sleep(4000);
				}
			}
			click(ObjectArraypaymenttype[3].replace("$TYPE", type), ObjectArraypaymenttype[2]);

			scrollIntoView(ObjectArrayshipamt[3], ObjectArrayshipamt[2]);
			type(ObjectArrayshipamt[3], ObjectArrayshipamt[2], scholarshipamt);
			type(ObjectArraydiscount[3], ObjectArraydiscount[2], discount);
			click(ObjectArraytotalprice[3], ObjectArraytotalprice[2]);
			Thread.sleep(2000);
			scrollIntoView(ObjectArrayupfront[3], ObjectArrayupfront[2]);

			type(ObjectArrayupfront[3], ObjectArrayupfront[2], downpayment);
			click(ObjectArraytotalprice[3], ObjectArraytotalprice[2]);
			scrollIntoView(ObjectArraygateway[3], ObjectArraygateway[2]);

			TestData.put("finalprice", downpayment);

			selectByVisibleText(ObjectArraygateway[3], ObjectArraygateway[2], gateway, "visibletext");
			Thread.sleep(2000);

			click(ObjectArraygenerate[3], ObjectArraygenerate[2]);
			Thread.sleep(3000);

			if (driver.switchTo().alert().getText().contains("Successfully Added !!")) {
				driver.switchTo().alert().accept();
				Thread.sleep(4000);
			} else if (driver.switchTo().alert().getText().contains("Alert text : Error!!! No Data!!")) {
				refresh();
				Thread.sleep(4000);
				fillPgpUpfrontpqymentForm(course, currency, type, scholarshipamt, discount, downpayment, gateway);
			}
			scrollIntoView(ObjectArraypreview[3], ObjectArraypreview[2]);
			click(ObjectArraypreview[3], ObjectArraypreview[2]);

			Thread.sleep(2000);

			windowhandle = getWindowHandlesthroughURL("preview");
			driver.switchTo().window(windowhandle);
			scrollIntoView(ObjectArraypaynow[3], ObjectArraypaynow[2]);
			click(ObjectArraypaynow[3], ObjectArraypaynow[2]);
//				driver.close();

			windowhandle = getWindowHandlesthroughURL("pgp_payments");
			driver.switchTo().window(windowhandle);
			driver.close();

			windowhandle = getWindowHandlesthroughURL("custom_payment");
			driver.switchTo().window(windowhandle);
			Thread.sleep(2000);
		} catch (Exception e) {
			fail("pgp upfront payment link is not working" + e, true);
		}

	}
/**
 * @author sowmya
 * @param course
 * @param alertmessage
 * @throws Exception
 */
public void verifyconvertleadtopotentialform(String course, String alertmessage) throws Exception {
	// TODO Auto-generated method stub
	try
	{
		String[] ObjectArrayemail = obj.getObjectArray("CLP_email");
		String[] ObjectArraygetclist = obj.getObjectArray("CLP_getclist");
		String[] ObjectArraycourselist = obj.getObjectArray("CLP_courselist");
		String[] ObjectArraycourselistselect = obj.getObjectArray("CLP_courselistselect");
		String[] ObjectArraycomment = obj.getObjectArray("CLP_comment");
		String[] ObjectArraysubmit = obj.getObjectArray("CLP_submit");
		String[] ObjectArrayarraymessage = obj.getObjectArray("CLP_message");

		
		type(ObjectArrayemail[3],ObjectArrayemail[2],TestData.get("email"));
		click(ObjectArraygetclist[3],ObjectArraygetclist[2]);
		Thread.sleep(1000);
		if(!checkElementExist(ObjectArrayarraymessage[3],ObjectArrayarraymessage[2]))
		{
			DBUtils.fetchanalyticsdisplaytitle(TestData.get("courseid"));
			click(ObjectArraycourselist[3],ObjectArraycourselist[2]);
			click(ObjectArraycourselistselect[3].replace("$COURSE", DBUtils.fetchanalyticsdisplaytitle(TestData.get("courseid"))),ObjectArraycourselistselect[2]);

			Thread.sleep(1000);
			type(ObjectArraycomment[3],ObjectArraycomment[2],"test test test");
			
			click(ObjectArraysubmit[3],ObjectArraysubmit[2]);
			if(getWebElement(ObjectArrayarraymessage[3],ObjectArrayarraymessage[2]).getAttribute("innerText").contains(alertmessage))
			{
				pass("convert lead to potential form is working fine");
			}
		}
		else if(getWebElement(ObjectArrayarraymessage[3],ObjectArrayarraymessage[2]).getAttribute("innerText").contains(alertmessage))
		{
			pass("convert lead to potential form is working fine");
		}
	}
	catch(Exception e)
	{
		fail("pgp upfront payment link is not working" + e, true);	
	}
}
/**
 * @author sowmya
 * @param action
 * @param course
 * @param gateway
 * @param currency
 * @param amount
 * @param message
 * @throws Exception
 */
public void verifymodifysalesdata(String action, String course, String gateway, String currency, String amount,
		String message) throws Exception {
	// TODO Auto-generated method stub
	try {
		String[] ObjectArrayemail = obj.getObjectArray("Modify_email");
		String[] ObjectArraysearch = obj.getObjectArray("Modify_search");
		String[] ObjectArraycourse = obj.getObjectArray("Modify_course");
		String[] ObjectArraycourseselect = obj.getObjectArray("Modify_courseselect");
		String[] ObjectArrayedit = obj.getObjectArray("Modify_edit");
		String[] ObjectArraygateway = obj.getObjectArray("Modify_gateway");
		String[] ObjectArraycurrency = obj.getObjectArray("Modify_currency");
		String[] ObjectArrayamount = obj.getObjectArray("Modify_amount");
		String[] ObjectArrayeditreason = obj.getObjectArray("Modify_editreason");
		String[] ObjectArraysave = obj.getObjectArray("Modify_save");
		String[] ObjectArraydisable = obj.getObjectArray("Modify_disable");
		String[] ObjectArraydisablereason = obj.getObjectArray("Modify_disablereason");
		String[] ObjectArrayconfirm = obj.getObjectArray("Modify_confirm");
		
		type(ObjectArrayemail[3],ObjectArrayemail[2],TestData.get("email"));
		click(ObjectArraysearch[3],ObjectArraysearch[2]);
		Thread.sleep(2000);
		click(ObjectArraycourse[3],ObjectArraycourse[2]);
		click(ObjectArraycourseselect[3].replace("$COURSE", course),ObjectArraycourseselect[2]);
		Thread.sleep(2000);

		if(action.equalsIgnoreCase("Edit"))
		{
			click(ObjectArrayedit[3],ObjectArrayedit[2]);
			selectByVisibleText(ObjectArraygateway[3], ObjectArraygateway[2], gateway, "visibletext");
			Thread.sleep(1000);
			selectByVisibleText(ObjectArraycurrency[3], ObjectArraycurrency[2], currency, "visibletext");
			selectByVisibleText(ObjectArraycurrency[3], ObjectArraycurrency[2], currency, "visibletext");
			clearDefaultText(ObjectArrayamount[3],ObjectArrayamount[2]);
			type(ObjectArrayamount[3],ObjectArrayamount[2],amount);
			TestData.put("finalprice", amount);
			selectByVisibleText(ObjectArrayeditreason[3], ObjectArrayeditreason[2], "1", "index");
			click(ObjectArraysave[3],ObjectArraysave[2]);
			Thread.sleep(Constants.general_wait_time);
			if (driver.switchTo().alert().getText().contains(message)) {
				driver.switchTo().alert().accept();
				Thread.sleep(4000);
			} 
		}
		else if(action.equalsIgnoreCase("Disable"))
		{
			click(ObjectArraydisable[3],ObjectArraydisable[2]);
			selectByVisibleText(ObjectArraydisablereason[3], ObjectArraydisablereason[2], "1", "index");
			click(ObjectArrayconfirm[3],ObjectArrayconfirm[2]);
			Thread.sleep(Constants.general_wait_time);
			if (driver.switchTo().alert().getText().contains(message)) {
				driver.switchTo().alert().accept();
				Thread.sleep(4000);
			} 

		}

	}
	catch(Exception e)
	{
		fail("modify sales data functionality is not working"+e,true);
	}
}
/**
 * @author sowmya
 * @param coursename
 * @throws Exception
 */
public void getcoursedetails(String coursename) throws Exception {
	// TODO Auto-generated method stub
	try {
		String[] ObjectArraycourse = obj.getObjectArray("CM_cname");
		String[] ObjectArraycourseid = obj.getObjectArray("CM_cid");
		String[] ObjectArraysearch = obj.getObjectArray("CM_cid_search");
		String[] ObjectArraysearchresult = obj.getObjectArray("CM_search_result");

		
		type(ObjectArraycourse[3],ObjectArraycourse[2],coursename);
		type(ObjectArraycourseid[3],ObjectArraycourseid[2],TestData.get("courseid"));
		click(ObjectArraysearch[3],ObjectArraysearch[2]);
		Thread.sleep(3000);
		
		if(checkElementExist(ObjectArraysearchresult[3],ObjectArraysearchresult[2]))
		{
			pass("reach functionality is working fine");
		}
		else
		{
			fail("search functionality is not working fine");
		}
	}
	catch(Exception e)
	{
		fail("Search functionality in course management module got failed "+e,true);
	}
}
/**
 * @author sowmya
 * @param modulename
 * @throws Exception
 */
public void verifycoursemodules(String modulename) throws Exception {
	// TODO Auto-generated method stub
	try {
		String[] ObjectArraymodulename = null;
		switch(modulename)
		{
		case "Course Config": 
			ObjectArraymodulename = obj.getObjectArray("CM_config");
			click(ObjectArraymodulename[3],ObjectArraymodulename[2]);
			Thread.sleep(2000);
			verifycourseconfigmodule();
			break;
			
		default :
			System.out.println("invalid module name");
			break;
			
		}
		
		
	}
	catch(Exception e)
	{
		fail("course modules is not working please check "+e,true);
	}
	
}
/**
 * @author sowmya
 * @throws Exception
 */
public void verifycourseconfigmodule() throws Exception
{
	try {
		String[] ObjectArrayshortcode = obj.getObjectArray("CM_shortcode");
		String[] ObjectArrayshortname = obj.getObjectArray("CM_shortname");
		String[] ObjectArraywelcomemail = obj.getObjectArray("CM_welocomemail");
		String[] ObjectArrayenrollmail = obj.getObjectArray("CM_enrollemail");
		String[] ObjectArrayspwelcommail = obj.getObjectArray("CM_spwelcomemail");
		String[] ObjectArrayspenrollmail = obj.getObjectArray("CM_spenrollmail");
		String[] ObjectArrayTRcount = obj.getObjectArray("CM_Trcount");
		String[] ObjectArrayENcount = obj.getObjectArray("CM_Encount");

		String[] ObjectArraysuggestion = obj.getObjectArray("CM_Suggestion");
		String[] ObjectArraysuggestionlist = obj.getObjectArray("CM_Suggestion_list");

		String[] ObjectArraysubmit = obj.getObjectArray("CM_Submit");
		
		String ran = generatePhoneNumber();
		clearDefaultText(ObjectArrayshortcode[3],ObjectArrayshortcode[2]);
		type(ObjectArrayshortcode[3],ObjectArrayshortcode[2],ran+"shortcode");
		TestData.put("mailchimp_shortCode",ran+"shortcode");
		clearDefaultText(ObjectArrayshortname[3],ObjectArrayshortname[2]);
		type(ObjectArrayshortname[3],ObjectArrayshortname[2],ran+"shortname");
		TestData.put("short_name",ran+"shortname");


		clearDefaultText(ObjectArraywelcomemail[3],ObjectArraywelcomemail[2]);
		type(ObjectArraywelcomemail[3],ObjectArraywelcomemail[2],ran+"welcome mail");
		TestData.put("mailchimp_welcomeMail",ran+"welcome mail");

		
		clearDefaultText(ObjectArrayenrollmail[3],ObjectArrayenrollmail[2]);
		type(ObjectArrayenrollmail[3],ObjectArrayenrollmail[2],ran+"enroll mail");
		TestData.put("mailchimp_enrollMail",ran+"enroll mail");

		
		clearDefaultText(ObjectArrayspwelcommail[3],ObjectArrayspwelcommail[2]);
		type(ObjectArrayspwelcommail[3],ObjectArrayspwelcommail[2],ran+"sp welcome mail");
		TestData.put("sparkpost_welcomeMail",ran+"sp welcome mail");

		clearDefaultText(ObjectArrayspenrollmail[3],ObjectArrayspenrollmail[2]);
		type(ObjectArrayspenrollmail[3],ObjectArrayspenrollmail[2],ran+"sp enroll mail");
		TestData.put("sparkpost_enrollMail",ran+"sp enroll mail");

		clearDefaultText(ObjectArrayTRcount[3],ObjectArrayTRcount[2]);
		type(ObjectArrayTRcount[3],ObjectArrayTRcount[2],"2");
		
		clearDefaultText(ObjectArrayENcount[3],ObjectArrayENcount[2]);
		type(ObjectArrayENcount[3],ObjectArrayENcount[2],"2");
		
//		for(int i=0;i<=10;i++)
//		{
//			
//		}
//		clearDefaultText(ObjectArraysuggestion[3],ObjectArraysuggestion[2]);
//		Thread.sleep(3000);
//		type(ObjectArraysuggestion[3],ObjectArraysuggestion[2],"selenium certification");
//		Thread.sleep(7000);
//		click(ObjectArraysuggestionlist[3],ObjectArraysuggestionlist[2]);
//		Thread.sleep(2000);
		click(ObjectArraysubmit[3],ObjectArraysubmit[2]);
		
	}
	catch(Exception e)
	{
		fail("course config module is not woring please check "+e,true);
	}
}

	/**
	 * @author aishwarya
	 * @param email
	 * @param course
	 * @param currency
	 * @param gateway
	 * @throws Exception
	 */
	public void pgp_interview_sales_alert(String email, String course, String currency, String gateway, String fee,
			String agent) throws Exception {
		try {
			String[] PgpIntPaySA = obj.getObjectArray("PgpIntPaySA");
			String[] IntPayEmailSA = obj.getObjectArray("IntPayEmailSA");
			String[] InPayCourseSA = obj.getObjectArray("InPayCourseSA");
			String[] InPayCourseSASelect = obj.getObjectArray("InPayCourseSASelect");
			String[] InPayCurrencySA = obj.getObjectArray("InPayCurrencySA");
			String[] InPayGatewaySA = obj.getObjectArray("InPayGatewaySA");
			String[] InPaySubmitSA = obj.getObjectArray("InPaySubmitSA");
			String[] InPayNameSA = obj.getObjectArray("InPayNameSA");
			String[] InPaySalesPerson = obj.getObjectArray("InPaySalesPerson");
			String[] InPaySalesPersonSelect = obj.getObjectArray("InPaySalesPersonSelect");
			String[] InPayIntFeeSA = obj.getObjectArray("InPayIntFeeSA");
			String[] InPaySACountry = obj.getObjectArray("InPaySACountry");
			String[] InPaySACountrySelect = obj.getObjectArray("InPaySACountrySelect");
			String[] InPaySAState = obj.getObjectArray("InPaySAState");
			String[] InPaySACode = obj.getObjectArray("InPaySACode");
			String[] InPaySACity = obj.getObjectArray("InPaySACity");
			String[] InPaySATransID = obj.getObjectArray("InPaySATransID");
			Thread.sleep(Constants.general_wait_time);
			click(PgpIntPaySA[3], PgpIntPaySA[2]);
			click(IntPayEmailSA[3], IntPayEmailSA[2]);
			type(IntPayEmailSA[3], IntPayEmailSA[2], email);
			Thread.sleep(2000);
			click(InPayCourseSA[3], InPayCourseSA[2]);
			click(InPayCourseSASelect[3].replace("$COURSE", course), InPayCourseSASelect[2]);
			Thread.sleep(5000);
			click(InPayNameSA[3], InPayNameSA[2]);
			type(InPayNameSA[3], InPayNameSA[2], "Test user");
			click(InPaySalesPerson[3], InPaySalesPerson[2]);
			click(InPaySalesPersonSelect[3].replace("$AGENT", agent), InPaySalesPersonSelect[2]);
			click(InPayCurrencySA[3], InPayCurrencySA[2]);
			Select drpCourse1 = new Select(driver.findElement(By.xpath(InPayCurrencySA[3])));
			drpCourse1.selectByVisibleText(currency);
			Thread.sleep(3000);
			click(InPayIntFeeSA[3], InPayIntFeeSA[2]);
			type(InPayIntFeeSA[3], InPayIntFeeSA[2], fee);
			click(InPayGatewaySA[3], InPayGatewaySA[2]);
			Select drpCourse2 = new Select(driver.findElement(By.xpath(InPayGatewaySA[3])));
			drpCourse2.selectByVisibleText(gateway);
			Thread.sleep(2000);
			click(InPaySATransID[3], InPaySATransID[2]);
			if (currency.equalsIgnoreCase("INR")) {
				type(InPaySATransID[3], InPaySATransID[2], "pay_TestTransactionID");
			} else
				type(InPaySATransID[3], InPaySATransID[2], "96E56956L6837670A");
			click(InPaySACountry[3], InPaySACountry[2]);
			click(InPaySACountrySelect[3], InPaySACountrySelect[2]);
			if (currency.equalsIgnoreCase("INR")) {
				click(InPaySAState[3], InPaySAState[2]);
				type(InPaySAState[3], InPaySAState[2], "Karnataka");
				click(InPaySACode[3], InPaySACode[2]);
				type(InPaySACode[3], InPaySACode[2], "560003");
				click(InPaySACity[3], InPaySACity[2]);
				type(InPaySACity[3], InPaySACity[2], "Bengaluru");
				Thread.sleep(1000);
			}
			click(InPaySubmitSA[3], InPaySubmitSA[2]);
			Thread.sleep(3000);
			driver.switchTo().alert().accept();
			Thread.sleep(1000);
		} catch (Exception e) {
			fail("Pgp Interview fee Sales alert failed" + e, true);
		}
	}

}