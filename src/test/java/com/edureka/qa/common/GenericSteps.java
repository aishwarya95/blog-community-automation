package com.edureka.qa.common;

import java.util.List;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import junit.framework.Assert;
import net.lightbody.bmp.core.har.HarEntry;
import net.lightbody.bmp.core.har.HarNameValuePair;

import com.edureka.qa.common.ApplicationLibrary;

public class GenericSteps {

	ApplicationLibrary app;

	public GenericSteps(ApplicationLibrary app) {
		this.app = app;
	}

/**	@And("^login with global credentials$")
	public void login_with_global_credentials() {
		try {
			// app.login(Constants.global_email, Constants.global_password);
			app.login("test_100b2001@tech.edureka.in", "Ramu@123#");
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	} **/

@Given("^user navigates to edureka admin portal$")
	public void user_navigates_to_edureka_website() {
		try {
			app.launchBrowser();
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	@And("^login to admin portal$")
	public void logintoadmin() {
		try {
			app.logintoadmin();
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	@And("^set currency as \"([^\"]*)\"$")
	public void set_currency_as_something(String currency) {
		try {
			app.TestData.put("currency", currency);
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	@And("^Navigate to \"([^\"]*)\" Page$")
	public void navigate_to_something_something(String page) {
		try {
			app.navigateToLinkPage(page);
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
	
	@Given("^login with below credentials$")
	public void login_with_below_credentials(DataTable LoginDetails) {
		try {
			List<List<String>> Credentials = LoginDetails.raw();
			app.login(Credentials.get(1).get(0), Credentials.get(1).get(1));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}

	}

	@SuppressWarnings("deprecation")
	@Then("^assert following analytics data$")
	public void assert_following_analytics_data(DataTable ExpectedAnalyticsInformations) throws InterruptedException {
		Thread.sleep(Constants.proxy_server_request_capture_wait_time);
		StringBuffer harlogs = new StringBuffer();
		List<HarEntry> entries = null;
		boolean allentriesfound = true;
		try {
			// boolean allentriesfound=true;
			List<List<String>> ExpectedAnalyticsInformation = ExpectedAnalyticsInformations.raw();
			entries = app.proxy.getHar().getLog().getEntries();
			for (int i = 1; i < ExpectedAnalyticsInformation.size(); i++) {

				if (i == 2)
					System.out.println("debug");
				List<String> AnalyticInformation = ExpectedAnalyticsInformation.get(i);
				String ExpectedEventLabel = app.replace(AnalyticInformation.get(2));
				System.out.println("ExpectedEventLabel = " + ExpectedEventLabel);
				// ExpectedEventLabel=ExpectedEventLabel.replace("$user",
				// app.TestData.get("userid")).replace("$currency",
				// app.TestData.get("currency"));
				boolean currentProcessingEntryFound = false;
				boolean EventCatagoryFound = false;
				boolean EventActionFound = false;
				boolean EventLabelFound = false;
				for (HarEntry entry : entries) {
					if (entry.getRequest().getUrl().toString().contains("https://www.google-analytics.com/")) {
						System.out.println(entry.getRequest().getUrl());
						List<HarNameValuePair> QueryParam = entry.getRequest().getQueryString();
						for (HarNameValuePair data : QueryParam) {
							try {
								if (data.getName().equals("ec") && data.getValue().equals(AnalyticInformation.get(0)))
									EventCatagoryFound = true;
								if (data.getName().equals("ea") && data.getValue().equals(AnalyticInformation.get(1)))
									EventActionFound = true;
								if (data.getName().equals("el")) {
									if (data.getValue().contains("Date") || data.getValue().contains("TimeStamp")
											|| data.getValue().contains("GMT")) {
										String updatedExpectedEventLabel = ExpectedEventLabel;
										String updatedActualEventLabel = data.getValue();
										String ActualTimeStamp = "";
										// String ExpectedTimeStamp="";
										String ActualDate = "";
										String ExpectedDate = "";
										if (data.getValue().contains("Date")) {
											updatedActualEventLabel = app.removeDateTimeStampField(data.getValue(),
													"Date");
											// updatedExpectedEventLabel=app.removeDateTimeStampField(ExpectedEventLabel,
											// "Date");
											ActualTimeStamp = app.extractDateTimeStampField(data.getValue(), "Date");
											// ExpectedTimeStamp=app.extractDateTimeStampField(ExpectedEventLabel,
											// "Date");
										} else if (data.getValue().contains("TimeStamp")) {
											updatedActualEventLabel = app.removeDateTimeStampField(data.getValue(),
													"TimeStamp");
											// updatedExpectedEventLabel=app.removeDateTimeStampField(ExpectedEventLabel,
											// "TimeStamp");
											ActualTimeStamp = app.extractDateTimeStampField(data.getValue(),
													"TimeStamp");
											// ExpectedTimeStamp=app.extractDateTimeStampField(ExpectedEventLabel,
											// "TimeStamp");
										} else if (data.getValue().contains("GMT")) {
											if (AnalyticInformation.get(0).equals("Search_v2")
													&& AnalyticInformation.get(1).equals("Homepage Search_Landings")) {
												updatedActualEventLabel = app
														.removeSearchTimeStampField(data.getValue(), ";", 1);
												// updatedExpectedEventLabel=app.removeSearchTimeStampField(ExpectedEventLabel,
												// ";",1);
												ActualTimeStamp = app.extractSearchTimeStampField(data.getValue(), ";",
														1);
												// ExpectedTimeStamp=app.extractSearchTimeStampField(ExpectedEventLabel,
												// ";",1);
											} else if (AnalyticInformation.get(0).equals("Search_v2")
													&& AnalyticInformation.get(1).equals("Homepage_Trending Search")) {
												updatedActualEventLabel = app
														.removeSearchTimeStampField(data.getValue(), ";", 2);
												// updatedExpectedEventLabel=app.removeSearchTimeStampField(ExpectedEventLabel,
												// ";",2);
												ActualTimeStamp = app.extractSearchTimeStampField(data.getValue(), ";",
														2);
												// ExpectedTimeStamp=app.extractSearchTimeStampField(ExpectedEventLabel,
												// ";",2);
											}
										}
										// ExpectedDate=Constants.dateformat.format(app.TestCase_StartTime);
										ExpectedDate = DateTimeConversion.convertTimeToSpecificTimeZone(
												app.TestCase_StartTime, Constants.server_instance_timezone);
										System.out.println("Actual Event label =" + updatedActualEventLabel);
										if (updatedActualEventLabel.equals(updatedExpectedEventLabel)
												&& app.checkTime(ActualTimeStamp, ExpectedDate))
											EventLabelFound = true;
									} else {
										if (data.getValue().equals(AnalyticInformation.get(2)))
											EventLabelFound = true;
									}
									if (EventCatagoryFound && EventActionFound && EventLabelFound) {
										currentProcessingEntryFound = true;
										break;
									}
								}
								if (currentProcessingEntryFound)
									break;

							} catch (Exception e) {
								e.printStackTrace();
							}
						}

						EventCatagoryFound = false;
						EventActionFound = false;
						EventLabelFound = false;
					}
					if (currentProcessingEntryFound)
						break;
				}
				if (!currentProcessingEntryFound) {
					allentriesfound = false;
					app.error_message.append("Row " + i + " Analytics Entries not found in HAR logs \n");
				}
			}
			// if(!allentriesfound)
			// Assert.fail(app.error_message.toString());
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
		for (HarEntry entry : entries) {
			harlogs.append(entry.getRequest().getUrl().toString() + " -> " + entry.getResponse().getStatus() + "\n");
		}
		app.attachment(harlogs.toString());
		if (!allentriesfound)
			Assert.fail(app.error_message.toString());
		// app.proxy.newHar();
	}
	
	@And("^Create a New session$")
	public void createNewSession() throws Exception {
		// TODO Auto-generated method stub
		try {
			app.closeBrowser();
		} catch (Exception e) {
			app.fail(e.getMessage());
			Assert.fail(e.getMessage());
		}
	}
}
