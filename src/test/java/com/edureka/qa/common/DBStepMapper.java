package com.edureka.qa.common;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import com.edureka.qa.common.ApplicationLibrary;
import com.mysql.cj.protocol.Resultset;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import io.qameta.allure.Step;

public class DBStepMapper {
	
ApplicationLibrary app;
	
	public DBStepMapper(ApplicationLibrary app) {
		this.app=app;
	}
	
	@Given("^extract the lead id$")
    public void extract_the_leadid() {
		
		try
		{
		if(app.TestData.get("userid")=="")
			Assert.fail("user id is empty in context");
       int LeadId=DBUtils.getLeadId(app.TestData.get("userid").toString(),app.TestCase_StartTime);
       if( LeadId!=-1)
    	   app.TestData.put("leadid", LeadId+"");
       else
    	   Assert.fail("Error while fetching LeadId through query= "+Constants.lead_id_query.replace("$USER_ID",app.TestData.get("userid").toString()));
		}
		catch(Exception e)
		{
			Assert.fail(e.getMessage());
		}
    }
	
	
	@Given("^extract the user id$")
    public void extract_the_userid() {
		try
		{
		if(app.TestData.get("email")=="")
			Assert.fail("email id is empty in context");
       int UserId=DBUtils.getUserId(app.TestData.get("email").toString(),app.TestCase_StartTime);
       if( UserId!=-1)
    	   app.TestData.put("userid", UserId+"");
       else
    	   Assert.fail("Error while fetching userid through query= "+Constants.user_id_query.replace("$EMAIL_ID",app.TestData.get("email").toString()));
		}
    	catch(Exception e)
    	{
    		Assert.fail(e.getMessage());
    	}
		}
	
	@And("^extract the course id for slug \"([^\"]*)\"$")
    public void extract_the_course_id_for_slug_something(String slug) {
		try
		{
	    	int CourseId=DBUtils.getCourseId(slug);
	    	 if(CourseId!=-1)
	      	   app.TestData.put("courseid", CourseId+"");
	    	 else
	      	   Assert.fail("Error while fetching could id through query= "+Constants.course_id_query.replace("$SLUG_URL",slug));
		}
    	catch(Exception e)
    	{
    		Assert.fail(e.getMessage());
    	}
		}
	
	
    @Given("^extract the course id$")
    public void extract_the_courseid() {
    	try
    	{
		if(app.TestData.get("slugurl")=="")
		Assert.fail("slugurl is null in context");		
    	int CourseId=DBUtils.getCourseId(app.TestData.get("slugurl").toString());
    	 if( CourseId!=-1)
      	   app.TestData.put("courseid", CourseId+"");
    	 else
      	   Assert.fail("Error while fetching course id through query= "+Constants.course_id_query.replace("$slugurl",app.TestData.get("slugurl").toString()));
    	}
    	catch(Exception e)
    	{
    		Assert.fail(e.getMessage());
    	}
    }
    
    @Step("DB Results")
	@And("^Assert following data in \"([^\"]*)\" table$")
	public void assert_following_data(String tablename, DataTable AssertionDetails) throws Exception {
		try {
			boolean flag = true;
			List<List<String>> ExpectedResponseDataArray = AssertionDetails.raw();
			String Query = "";
			String ExpectedResponse = "";
		

			for (int i = 1; i < ExpectedResponseDataArray.size(); i++) {
				List<String> ExpectedResponseData = ExpectedResponseDataArray.get(i);
				try {
					
					// Query=ExpectedResponseData.get(0); //Comment out after unit testing
					// Query=ExpectedResponseData.get(0).replace("$USER_ID",
					// app.TestData.get("userid")).replace("$COURSE_ID",app.TestData.get("courseid")).replace("$EMAIL_ID",app.TestData.get("email")).replace("$LEAD_ID",app.TestData.get("leadid")
					// ).replace("$TEST_CASE_START_TIME",
					// DateTimeConversion.convertTimeToSpecificTimeZone(app.TestCase_StartTime,
					// Constants.mysql_instance_timezone)).replace("$TEST_CASE_START_TIME",
					// DateTimeConversion.convertTimeToSpecificTimeZone(app.TestCase_StartTime,
					// Constants.mysql_instance_timezone)); //un comment after unit testing
					    Query = app.replace(ExpectedResponseData.get(0));
					// ExpectedResponse=ExpectedResponseData.get(1).toString().replace("$EMAIL_ID",
					// app.TestData.get("email")).replace("$PHONE_NO",
					// app.TestData.get("phoneno")).replace("$COURSE_ID",
					// app.TestData.get("courseid")).replace("$USER_ID",
					// app.TestData.get("userid")).replace("$LEAD_ID",app.TestData.get("leadid")
					// ).replace("$TEST_CASE_START_TIME",
					// DateTimeConversion.convertTimeToSpecificTimeZone(app.TestCase_StartTime,
					// Constants.mysql_instance_timezone)).replace("$TEST_CASE_START_TIME",
					// DateTimeConversion.convertTimeToSpecificTimeZone(app.TestCase_StartTime,
					// Constants.mysql_instance_timezone));
						
					ExpectedResponse = app.replace(ExpectedResponseData.get(1));
					if(ExpectedResponse.equals("COURSE_ID")) {
						ExpectedResponse=Constants.course_id;
					}
					app.pass(Query);
					System.out.println(Query);
					if (Query.contains("zoholead_id") || Query.contains("zoho_potential_id")) {
						for (int j = 0; j <= 6; j++) {
							Thread.sleep(4000);
							String ActualResponsezoho = DBUtils.getActualResult(Query);
							if (ActualResponsezoho == null || ActualResponsezoho == " ") {
								System.out.println("pot id is null ..going to try again");
								System.out.println("Query=" + Query + "Actual Response=" + ActualResponsezoho
										+ " Expected Response=" + ExpectedResponse);
								if(j==6)
								{
									Assert.fail(app.error_message.toString());
								}
							}
								else if (ActualResponsezoho != null) {
								System.out.println("Query=" + Query + "Actual Response=" + ActualResponsezoho
										+ " Expected Response=" + ExpectedResponse);
								break;
							}
							
						}

					}
					
					else {
					String ActualResponse = DBUtils.getActualResult(Query);
					System.out.println(ActualResponse);
					if (ActualResponse != "-1") {
						System.out.println("Query=" + Query + "Actual Response=" + ActualResponse
								+ " Expected Response=" + ExpectedResponse);	
						if (ActualResponse == null && ExpectedResponse.equals(""))
							continue;
						else if ((ActualResponse != null && !(ActualResponse.equals("")))
								&& ExpectedResponse.toLowerCase().equals("is_not_null_or_blank"))
							continue;
						if (ActualResponse.equals("") && ExpectedResponse.toLowerCase().equals("is_blank"))
							continue;
						else if (ActualResponse == null && !ExpectedResponse.equals("")) {
							app.error_message.append("Assertion Results are not equal for row =" + i + " Query=" + Query
									+ " Actual=" + ActualResponse + " , Expected Response=" + ExpectedResponse + "\n");
							flag = false;
						} else if (!ActualResponse.equals(ExpectedResponse)) {
							System.out.println(ActualResponse);
							System.out.println(ExpectedResponse);
							app.error_message.append("Assertion Results are not equal for row =" + i + " Query=" + Query
									+ " Actual=" + ActualResponse + " , Expected Response=" + ExpectedResponse + "\n");
							app.fail("Assertion Results are not equal for row =" + i + " Query=" + Query
									+ " Actual=" + ActualResponse + " , Expected Response=" + ExpectedResponse + "\n");
							flag = false;
						}
					} else {
						flag = false;
					}
					}
				} catch (Exception e) {
					app.error_message.append("Error During processing Expected Response Data Row = " + i + ", Query="
							+ Query + " , Expected Result=" + ExpectedResponse + "\n" + e);
					flag = false;
				}
			}
			if (!flag)
				Assert.fail(app.error_message.toString());
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
    
    
    @And("^extract discount from order summary page$")
    public void extract_discount_from_order_summary_page() {
    	try
    	{
    	}
    	catch(Exception e)
    	{
    		Assert.fail(e.getMessage());
    	}
    	
    }

    @And("^extract course price information for currency \"([^\"]*)\"$")
    public void extract_course_price_information_for_currency_something(String currency) {
    	try
    	{
    	if(app.TestData.get("courseid")=="")
    		Assert.fail("course id is null in context");
    	else
    	{
    	String	Query=Constants.course_price_query.replace("$COURSE_ID", app.TestData.get("courseid")).replace("$CURRENCY", currency);
    	String originalPrice=DBUtils.getActualResult(Query);
    	if(originalPrice.equals("-1"))
    		Assert.fail("Error during extracting course price Query="+Query);
    		else
    			app.TestData.put("originalprice", originalPrice);
    	}
    	}
    	catch(Exception e)
    	{
    		Assert.fail(e.getMessage());
    	}
    }

    @And("^extract course price in usd and inr$")
    public void extract_course_price_in_usd_and_inr() {
    	try
    	{
    	String priceinr="",priceusd="";
    	if(app.TestData.get("courseid")=="")
    		Assert.fail("course id is null in context");
    	else
    	{
    		String Query=Constants.price_usd_inr_query.replace("$COURSE_ID", app.TestData.get("courseid"));
    		ResultSet rs=DBUtils.executeQuery(Query);
    		if(rs!=null)
    		{
    			if(rs.next())
    			{
    				priceusd=rs.getString(1);
    				priceinr=rs.getString(2);
    			}
    		}
    		else
    		{
    			Assert.fail("Error during extraction of price usd,price inr information query="+Query);
    		}
    		app.TestData.put("priceusd",priceusd);
    		app.TestData.put("priceinr",priceinr);
    	}
    	}
    	catch(Exception e)
    	{
    		Assert.fail(e.getMessage());
    	}
    }

    @And("^calculate service tax with percentage \"([^\"]*)\"$")
    public void calculate_service_tax_with_percentage_something(String taxPercentage) {
    	try
    	{
    	if(app.TestData.get("originalprice").equals("-1"))
    		Assert.fail("course price information is null in context");
         int servicetax=Integer.parseInt(app.TestData.get("originalprice"))*Integer.parseInt(taxPercentage)/100;
         app.TestData.put("servicetax",servicetax+"");
    	}
    	catch(Exception e)
    	{
    		Assert.fail(e.getMessage());
    	}
    }

    @And("^calculate final price including taxation$")
    public void calculate_final_price_including_taxation() {
    	try
    	{
    	if(app.TestData.get("originalprice").equals("-1")||app.TestData.get("servicetax").equals("-1")||app.TestData.get("discount").equals("-1"))
    		Assert.fail("course price/Service tax/discount information is null in context");
    	int finalprice=Integer.parseInt(app.TestData.get("originalprice"))-Integer.parseInt(app.TestData.get("discount"))+Integer.parseInt(app.TestData.get("servicetax"));
    	app.TestData.put("finalprice",finalprice+"");
    	}
    	catch(Exception e)
    	{
    		Assert.fail(e.getMessage());
    	}
    }
    /**
     * @author sowmya
     */
    @And("^update password of admin$")
    public void updateadminpassword() {
    	try
    	{
    		DBUtils.setpassword();
    	}
    	catch(Exception e)
    	{
    		Assert.fail(e.getMessage());
    	}
    }
    /**
     * @author sowmya
     * @param value
     * @param type
     */
	@And("^extract the course id for \"([^\"]*)\" through \"([^\"]*)\"$")
    public void extract_the_course_id_from_something(String value,String type) {
		try
		{
	    	int CourseId=DBUtils.getcoursepidfrom(value,type);
	    	 if(CourseId!=-1)
	      	   app.TestData.put("courseid", CourseId+"");
	    	 else
	      	   Assert.fail("Error while fetching could id through query= ");
		}
    	catch(Exception e)
    	{
    		Assert.fail(e.getMessage());
    	}
		}
	
	/**
	 * @author sowmya
	 * @param tablename
	 */
	@And("^Check Child courses in \"([^\"]*)\" table")
    public void extract_the_course_id_from_something(String tablename) {
		try
		{
	    	DBUtils.checkcourseformastercourses(tablename,app.TestData.get("userid"),app.TestData.get("courseid"),app.TestData.get("email"));
	    	 
		}
    	catch(Exception e)
    	{
    		Assert.fail(e.getMessage());
    	}
		}
	
	/**
	 * @author sowmya
	 */
	@And("^extract original price and final price for Sales Alert form with currency conversion$")
    public void extract_course_price_in_usd_and_inr_salesalertform() {
    	try
    	{
    	String priceinr="",priceusd="";
    	float currencyrate=0;
    	int orgvalue=0;
    	int discount = 0;
//    	float servicetax 
    	if(app.TestData.get("courseid")=="")
    		Assert.fail("course id is null in context");
    	else
    	{
    		if(!app.TestData.get("currency").contains("INR"))
    		{
    		String Query="select currency_rate from sales_alert_currencies where currency_code=\""+app.TestData.get("currency")+"\" order by id desc;";
    		ResultSet rs=DBUtils.executeQuery(Query);
    		if(rs!=null)
    		{
    			if(rs.next())
    			{
    				currencyrate = rs.getFloat(1);
    			}
    		}
    		
    		String	Queryorg=Constants.course_price_query.replace("$COURSE_ID", app.TestData.get("courseid")).replace("$CURRENCY", "USD");

    		String originalPrice=DBUtils.getActualResult(Queryorg);

        	orgvalue = (int) (Float.valueOf(originalPrice)*currencyrate);

    		app.TestData.put("originalprice",String.valueOf(orgvalue));
    		}
    		if(app.TestData.get("currency").contains("INR"))
    		{
    			String discountquery = "Select discount_value from post_orders where userid=\'"+app.TestData.get("userid")+"\' and courseid=\'"+app.TestData.get("userid")+"\"';";
    			ResultSet rs1=DBUtils.executeQuery(discountquery);
        		if(rs1!=null)
        		{
        			if(rs1.next())
        			{
        				discount = rs1.getInt(1);
        			}
        		}
        		
        		
    		}
    		else {
    		}
    	}
    	}
    	catch(Exception e)
    	{
    		Assert.fail(e.getMessage());
    	}
    }
	/**
	 * @author sowmya
	 * @param email
	 * @param time
	 */
	@And("^Check updated validity value \"([^\"]*)\" for \"([^\"]*)\"$")
    public void check_updated_validity(String email,String time) {
		String discountvalidity = null;
		try
		{
			email = email.replace("$EMAIL_ID", app.TestData.get("email"));
			String Queryvalidity="select discount_validity from batch_discounts where user_email=\""+email+"\" order by id desc;";
    		ResultSet rs1=DBUtils.executeQuery(Queryvalidity);
    		if(rs1!=null)
    		{
    			if(rs1.next())
    			{
    				discountvalidity = rs1.getString(1);
    			}
    		}  
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                    Locale.ENGLISH);
    		Date parsedDatef = sdf.parse(discountvalidity);
    		Date parsedDates = sdf.parse(DBUtils.DbutilsTestData.get("validity"));

    		long diff = parsedDatef.getTime() - parsedDates.getTime();
    		long minutes = TimeUnit.MILLISECONDS.toMinutes(diff); 

    		Assert.assertEquals(time,String.valueOf(minutes) );

		}
    	catch(Exception e)
    	{
    		System.out.println(e);
    		Assert.fail(e.getMessage());
    	}
		}
	/**
	 * @author sowmya
	 */
	@And("^Update Lead ids and Potential ids$")
    public void updateleadids() {
		try
		{
			DBUtils.updateidstonull(app.TestData.get("email"),app.TestData.get("courseid"));

		}
    	catch(Exception e)
    	{
    		System.out.println(e);
    		Assert.fail(e.getMessage());
    	}
		}
}
