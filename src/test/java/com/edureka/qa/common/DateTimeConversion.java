package com.edureka.qa.common;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateTimeConversion {
	public static String convertTimeToSpecificTimeZone(Date SourceDate,String TargetTimeZone) throws ParseException
	{
		String SourceDateInString=Constants.dateformat.format(SourceDate);
		//System.out.println("Source Date="+SourceDateInString);
		Date Source=Constants.dateformat.parse(SourceDateInString);
		Constants.dateformat.setTimeZone(TimeZone.getTimeZone(TargetTimeZone));
		//System.out.println("Converted date in "+TargetTimeZone+" ="+Constants.dateformat.format(Source));
		return Constants.dateformat.format(Source);
	}
}
