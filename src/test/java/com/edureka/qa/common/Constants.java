package com.edureka.qa.common;

import java.sql.Connection;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Properties;

import javax.swing.text.DateFormatter;

public class Constants {

	public static String browser = null;

	public static String chrome_driver_path = null;

	public static String ie_driver_path = null;

	public static String firefox_driver_path = null;

	public static boolean is_initialized = false;

	public static boolean is_initialized_variable = false;

	public static Properties prop = new Properties();

	public static int explicit_wait_time = 0;

	public static int implicit_wait_time = 0;

	public static int page_load_time = 0;

	public static String base_url = null;

	public static String allcoursespage_url = null;

	public static String categorypage_url = null;

	public static String mysql_driver = "com.mysql.jdbc.Driver";

	public static Connection db_connection;

	public static Statement db_statement = null;

	public static String db_server = null;

	public static String db_username = null;

	public static String db_password = null;

	public static String db_port = null;

	public static String db_name = null;

	public static String course_id_query = null;

	public static String user_id_query = null;

	public static String prefix1 = null;

	public static String prefix2 = null;

	public static int general_wait_time = 0;

	public static long timestamp_threshold = 0;

	public static String format = "dd-MMM-yyyy HH:mm:ss";

	public static DateFormat dateformat = new SimpleDateFormat(format);

	public static String mysql_instance_timezone = null;

	public static String lead_id_query = null;

	public static String course_price_query = null;

	public static String price_usd_inr_query = null;

	public static int proxy_server_request_capture_wait_time = 0;

	public static String global_email_suffix = null;

	public static String global_email = null;

	public static String global_phonenumber = null;

	public static String global_password = null;

	public static String global_country_code = null;

	public static String server_instance_timezone = null;

	public static String paypal_email = null;

	public static String paypal_pwd = null;

	public static String course_name = null;

	public static String course_id = null;

	public static String ccenable_query = null;

	public static String ccdisable_query = null;

	public static String sql_safe_state = null;

	public static String ccavenue_default = null;

	public static String razorpay_default = null;
	
	public static String gtmetrix_url = null;

	public static String schema_url = null;

	public static String admin_email = null;

	public static String admin_pwd = null;
	
	public static String admin_pwdkey = null;

	public static String default_server = null;

	public static String fullload_time_clp = null;

	public static String fullload_time_mlp = null;

	public static String fullload_time_home = null;
	
	public static String course_type_query = null;

}
