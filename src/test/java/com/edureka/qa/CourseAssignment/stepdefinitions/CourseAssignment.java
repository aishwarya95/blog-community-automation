package com.edureka.qa.CourseAssignment.stepdefinitions;

import java.util.List;

import com.edureka.qa.common.ApplicationLibrary;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import junit.framework.Assert;

public class CourseAssignment {

ApplicationLibrary app;
	
	public CourseAssignment(ApplicationLibrary app)
	{
		this.app=app;
	}
	
	/***
     * @author sowmya
     * @param functionality
     */
	@And("^Check Course Assignment form$")
    public void checkCourseAssignmentForm(DataTable inputDetails) {
    	try {
    		List<List<String>> formDetails = inputDetails.raw();
    		String email = app.replace(formDetails.get(1).get(0));
    		String course  =   formDetails.get(1).get(1);
    		String batch = formDetails.get(1).get(2);
    		String reason = formDetails.get(1).get(3);
    		
    		app.courseassignmentform(email,course,batch,reason);
//    		app.coursecardHomepageinfo(pagename);
        	
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
    }
	
	
}
