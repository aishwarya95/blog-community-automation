package com.edureka.qa.leadforms.stepdefinitions;

import java.util.List;

import com.edureka.qa.common.ApplicationLibrary;
import com.edureka.qa.common.Constants;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class leadForms {

	ApplicationLibrary app;

	public leadForms(ApplicationLibrary app) {
		this.app = app;
	}

//	/**
//	* @author aishwarya
//	* throws exception
//	*/ 
//	@And("^Navigate to \"([^\"]*)\" Page$")
//	public void navigate_to_page(String page) throws Exception {
//		app.navigateToLinkPage(page);
//	}
//	
	/**
	* @author aishwarya
	* throws exception
	*/ 
	@Then("^verify sales lead form for email$")
	public void verify_sales_lead(DataTable inputDetails) throws Exception {
		List<List<String>> applyNowDetails = inputDetails.raw();
		String randomno = app.generatePhoneNumber();
		String email1 = randomno + applyNowDetails.get(1).get(0).toString();
		String course =applyNowDetails.get(1).get(1).toString();
		app.TestData.put("email", email1);
		app.saleslead(email1,course);
	}
	/**
	 * @author sowmya 
	 * @param inputDetails
	 * @throws Exception
	 */
	@Then("^verify Convert lead to Potential form$")
	public void verify_convert_lead_topotential(DataTable inputDetails) throws Exception {
		List<List<String>> applyNowDetails = inputDetails.raw();
		String course =applyNowDetails.get(1).get(0).toString();
		String alertmessage =applyNowDetails.get(1).get(1).toString();
		app.verifyconvertleadtopotentialform(course,alertmessage);
	}
	
	
}