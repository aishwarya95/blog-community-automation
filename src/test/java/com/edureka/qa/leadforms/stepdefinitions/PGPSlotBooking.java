package com.edureka.qa.leadforms.stepdefinitions;

import java.util.List;

import com.edureka.qa.common.ApplicationLibrary;
import com.edureka.qa.common.Constants;
import com.edureka.qa.common.DBUtils;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class PGPSlotBooking {

	ApplicationLibrary app;

	public PGPSlotBooking(ApplicationLibrary app) {
		this.app = app;
	}

	/**
	 * @author aishwarya throws exception
	 */
	@Then("^Add dummy faculty$")
	public void add_dummy_faculty() throws Exception {
		try {

			app.pgp_add_dummy_faculty();
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	/***
	 * @author aishwarya
	 * @param functionality
	 */
	@And("^Check Upload PGP slot functionality$")
	public void pgpupbulklead(DataTable formdetails) {
		try {
			List<List<String>> details = formdetails.raw();
			app.pgpuploadslot(details.get(1).get(0), details.get(1).get(1));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * @author aishwarya throws exception
	 */
	@Then("^verify pgp slot booking for user$")
	public void verify_pgp_slot_book(DataTable inputDetails) throws Exception {
		List<List<String>> applyNowDetails = inputDetails.raw();
		String email1 = app.replace(applyNowDetails.get(1).get(0).toString());
		String course = applyNowDetails.get(1).get(1).toString();
		app.pgp_book_slot(email1, course);
	}
	
	/**
	 * @author aishwarya throws exception
	 */
	@Then("^Inactivate existing slots for below course$")
	public void inactivate_existing_pgp_slots(DataTable inputDetails) throws Exception {
		List<List<String>> applyNowDetails = inputDetails.raw();
		String course = applyNowDetails.get(1).get(0).toString();
		DBUtils.inactivatePgpSlots(course);
	}

}