package com.edureka.qa.leadforms.stepdefinitions;

import java.util.List;

import com.edureka.qa.common.ApplicationLibrary;
import com.edureka.qa.common.Constants;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class modifybu {

	ApplicationLibrary app;

	public modifybu(ApplicationLibrary app) {
		this.app = app;
	}

	/**
	* @author aishwarya
	* throws exception
	*/ 
	@Then("^verify Add BU$")
	public void verify_add_bu(DataTable inputDetails) throws Exception {
		List<List<String>> formDetails = inputDetails.raw();
		String name = formDetails.get(1).get(0);
		String contactin = formDetails.get(1).get(1);
		String contactus = formDetails.get(1).get(2);
		String email = formDetails.get(1).get(3);
		app.addbu(name,contactin,contactus,email);
	}
	
	/**
	* @author aishwarya
	* throws exception
	*/ 
	@Then("^Verify Edit BU$")
	public void verify_edit_bu() throws Exception {
		app.editbu();
	}
		
}