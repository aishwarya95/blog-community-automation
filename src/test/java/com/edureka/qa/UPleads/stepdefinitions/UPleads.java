package com.edureka.qa.UPleads.stepdefinitions;

import java.util.List;

import com.edureka.qa.common.ApplicationLibrary;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import junit.framework.Assert;

public class UPleads {

ApplicationLibrary app;
	
	public UPleads(ApplicationLibrary app)
	{
		this.app=app;
	}
	
	/***
     * @author sowmya
     * @param functionality
     */
	@And("^Check Upload B2C lead functionality$")
    public void b2cupbulklead(DataTable formdetails) {
    	try {
	    	List<List<String>> details=formdetails.raw();
	    	app.uploadbulkleadfunc(details.get(1).get(0),details.get(1).get(1),details.get(1).get(2));

		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
    }
	
	/***
     * @author sowmya
     * @param functionality
     */
	@And("^Check Upload PGP lead functionality$")
    public void pgpupbulklead(DataTable formdetails) {
    	try {
	    	List<List<String>> details=formdetails.raw();
	    	app.pgpuploadbulkleadfunc(details.get(1).get(0),details.get(1).get(1));

		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
    }
	
	/***
     * @author sowmya
     * @param functionality
     */
	@And("^Verify User registration form$")
    public void verifyuserreg(DataTable formdetails) {
    	try {
	    	List<List<String>> details=formdetails.raw();
	    	app.filluserregform(details.get(1).get(0),details.get(1).get(1),details.get(1).get(2));

		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
    }
}
