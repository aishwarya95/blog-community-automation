package com.edureka.cucumber.runner;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumber;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;

@RunWith(Cucumber.class)
//@RunWith(ExtendedCucumber.class)

@CucumberOptions(plugin = { "io.qameta.allure.cucumberjvm.AllureCucumberJvm" }, 
features = {
//		"src/test/java/FeatureFiles/Leadforms", 
		"src/test/java/FeatureFiles/UserReg",
//		"src/test/java/FeatureFiles/Leadforms/SalesLead.feature" ,
//		"src/test/java/FeatureFiles/Referral_Credits",
//		"src/test/java/FeatureFiles/Customlink",
//		"src/test/java/FeatureFiles/Leadforms/B2CUPlead.feature",
//		"src/test/java/FeatureFiles/CourseAssignment/CourseAssignment.feature",
//		"src/test/java/FeatureFiles/CustomDiscount/CustomDiscount.feature",
//		"src/test/java/FeatureFiles/PgpPayment",
//		"src/test/java/FeatureFiles/SalesAlert",
//		"src/test/java/FeatureFiles/Extend_Custom_Discount_validity",
//		"src/test/java/FeatureFiles/Course_Management",
//		"src/test/java/FeatureFiles/ModifySalesData/ModifySalesData.feature"
		}, 
glue = { "com.edureka.qa.common",
				"com.edureka.qa.dropusquery.stepdefinitions", "com.edureka.qa.UPleads.stepdefinitions",
				"com.edureka.qa.leadforms.stepdefinitions",
				"com.edureka.qa.ReferralCredits.stepdefinitions",
				"com.edureka.qa.Customlink.stepdefinitions","com.edureka.qa.CourseAssignment.stepdefinitions",
				"com.edureka.qa.CustomDiscount.stepdefinitions","com.edureka.qa.pgppayment.stepdefinitions",
				"com.edureka.qa.SalesAlert.stepdefinitions",
				"com.edureka.qa.CourseManagement.stepdefinitions"}, 
monochrome = true)
//@ExtendedCucumberOptions(jsonReport="target/cucumber.json",retryCount = 1)
public class cucumberRunnerTest {

	/**
	 * @BeforeClass public static void before() { System.out.println("Inside Junit
	 *              Before class"); }
	 * 
	 * @AfterClass public static void after() { System.out.println("Inside Junit
	 *             after class"); }
	 **/

}
